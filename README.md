# local-identity

Tool for managing identities - such as git identities (the reason for this program's existence) - on a per-shell basis.

The idea - sometimes you want to keep multiple identities separate when developing or using programs. This tool is designed to manage that simply and easily.

Right now the tool is just designed to support git and provide a powerline module for indicating current identity.

### Configuration
The identities are designed to be written as simple toml files in the .local-identities folder.

### Means of hooking in
This program is designed to be run on every `.bashrc` or local shell equivalent with access to aliasing (any POSIX-compliant shell) to allow easy switching of identity (without needing to `source` things manually), 

### Supporting Git 
Supporting git means working on a repository *as* someone as well as providing basic checks to prevent accidental leakage of identity. 

Initially implemented checks and items:
* Per-Identity commit/tag/etc. gpg key signing
* Pre-Commit hook (via the git template directory feature) to check files for leaked identity.

### Means of switching identities on the fly
Autogenerated `source`-able scripts that change appropriate environment variables. Things like git repos will have associated identities and will use those to check identity leaks for any other identities present in your environment. 
