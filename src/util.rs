pub mod args;
pub mod conv;
pub mod err;
pub mod functions;
pub mod io;
pub mod macroutil;
pub mod multi_io;
pub mod path;
pub mod protocol;
pub mod serde;
pub mod shellenv;
pub mod subcmdlog;
pub mod tty_hacks;
pub mod unix_sockets;
pub mod xdgstuff;

/// Simply universally process a reference to an object and return it back.
pub trait UniversalInspect: Sized {
    #[inline]
    fn inspecting_process(self, function: impl Fn(&Self)) -> Self {
        function(&self);
        self
    }
}

impl<T: Sized> UniversalInspect for T {}

/// Simply universally modify an object with a function, mutably, then return the owned object
/// again ^.^
pub trait UniversalInspectMut: Sized {
    #[inline]
    fn inspecting_process_mut(mut self, function: impl Fn(&mut Self)) -> Self {
        function(&mut self);
        self
    }
}

impl<T: Sized> UniversalInspectMut for T {}

/// Return if the value is the same as the default for the type.
pub fn is_default<T: PartialEq + Default>(v: &T) -> bool { *v == T::default() }

/// Simple universal transform - like map but it does not have to be inside something like an
/// option ^.^
pub trait UniversalTransform: Sized {
    #[inline]
    /// Apply function directly to self.
    fn apply_fn<T>(self, function: impl FnOnce(Self) -> T) -> T { function(self) }
}

impl<T: Sized> UniversalTransform for T {}

#[inline]
pub const fn true_val() -> bool { true }

#[inline]
pub const fn false_val() -> bool { false }

/// Simple function for use in serde attrs that says if something is true
#[inline]
pub const fn is_true(v: &bool) -> bool { *v }

/// Simple function for use in serde attrs that says if something is false
#[inline]
pub const fn is_false(v: &bool) -> bool { !*v }

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
