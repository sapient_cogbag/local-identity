//! Tools for managing user-global, directory-local, and shell-local identity components
//!
//! This part of the program encodes how tagged directories, shell sessions, and global
//! configuration files are put together to check identity validity, and to change the current
//! identity for each of them.
//!
//! Modules are, well, *modular*. They are defined on a per-program basis and essentially amount to
//! a collection of two or three types of check and configuration.
//!
//! #### SHELL-LOCAL CONFIGURATION IDENTITY MODULES
//! Shell local configuration modules are designed to manage the identity a given program (e.g.
//! git) uses when publishing things to the internet or otherwise interacting with accounts.
//!
//! The primary mechanism of identity switching with these is setting up appropriate *environment
//! variables*. It is almost entirely independent of filesystem state at all - though a
//! per-identity-per-module temporary directory is optionally set up when managing an identity,
//! which can be used in environment variables for default initialisation if it is independent to
//! user-wide configuration or per-directory configuration.
//!
//! An example usage of this is using [GIT_TEMPLATE_DIR](https://git-scm.com/docs/git-init#_template_directory)
//! with a template directory to set up the metadata info for the purposes of `git init` or `git
//! clone` creating directories of the current local directory identity.
//!
//! #### DIRECTORY-LOCAL CONFIGURATION IDENTITY MODULES
//! Directory local identity modules are designed to ensure that one does not use the incorrect
//! shell-local identity when modifying a directory created under another shell-local identity, as
//! well as where possible providing hooks and checks to reduce the risk of leaks.
//!
//! For instance, in the case of git, it may involve - above just tagging the directory with the
//! shell identity used to create it - things like hooks to check for identity leaks before
//! committing, and automatic usage of identity-safe credentials in a local .gitconfig as provided
//! in a generated GIT_TEMPLATE_DIR (mentioned in the shell-local identity module section).
//!
//! These identity modules also encode methods of checking if the current directory is contained in
//! a directory with one or more local identities for various programs. If these local identities
//! are not coherent then warnings can also be provided.
//!
//! Most directories that exist, naturally, will not have a local identity associated with them -
//! but some, like git repositories, will have them.
//!
//! #### USER-GLOBAL CONFIGURATION IDENTITY MODULES
//! This component is more complex and unwieldy as compared to the former two. It essentially,
//! optionally, allows for the state of global configuration files and directories to be managed on
//! a per-identity basis with appropriately defined symlinks. Because of the complexity involved
//! here, it has to be explicitly enabled and configured for a possible module.
//!
//! Global identity modules in practice come in three modes:
//! * Unspecified - that is, nothing has been done to specify which identity is present in the
//! user file system at all
//! * Fixed - The current identity of the filesystem *is* specified, but it is not managed by the
//! program and so can't be easily changed by command.
//! * Managed - The current identity of the filesystem is both specified and most importantly
//! managed by this program, via symlinking of per-identity folders to known locations in $HOME or
//! somewhere else.
//!
//! Unspecified modules must be manually tagged with a current identity to start the process. This
//! will mark them as fixed - in particular, it says "these directories are currently for some
//! specific identity".
//!
//! Fixed and Managed directories can be interchanged. Managing a Fixed collection of directories
//! and files is done by moving the files and directories to a permanent, per-identity directory -
//! then symbolically linking that collection of files and directories to the original location.
//!
//! Switching managed directories and files is simple.... ish. In theory, it should be possible to
//! just unlink and then relink the items to a new per-identity set of locations. This is in
//! principle completely fine, but in practise it may cause some programs issues when constructing
//! a new identity because they depend on the nonexistence of a file to determine if it should be
//! created with defaults or not.
//!
//! Furthermore, it is essentially user-preference on if they should kill or otherwise run some
//! command or other to switch the identities of programs when modifying the symlinks.

use std::{
    borrow::Cow,
    collections::HashMap,
    error::Error,
    ffi::OsStr,
    fmt::Display,
    io,
    path::{Path, PathBuf},
    str::FromStr,
};

use anyhow::{anyhow, bail, Result as AnyResult};

use dirs::home_dir;
use log::{debug, error, info, warn};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::{
    dst_newtype,
    identity::{CascadingDefaults, Identity, IdentityFinalised, IdentityName, IdentityNameBuf},
    util::{
        functions::HashMapExt,
        io::{
            has_suffix_and_name, is_unicode_with_suffix_and_name,
            path_is_unicode_with_suffix_and_name,
        },
        xdgstuff::{NoHomeError, XdgDirsWithHome},
        UniversalInspectMut, UniversalTransform,
    },
};

use self::user_global::find_conflict_in_modules;

pub mod custom_env;
pub mod shell;
pub mod user_global;

dst_newtype! {
    /// Owned unique module identifier/name
    pub ModuleNameBuf(pub) from String;
    /// DST module name
    pub ModuleName(pub) from str;
    reffn pub from_raw_str;
    mutfn pub from_raw_str_mut;
    owned_only_derives Clone;
    serde serialize deserialize;
    json_schema yes;
}

impl<'a> std::fmt::Display for &'a ModuleName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { f.write_str(&self.0) }
}

/// Trait that enables emission of environment variable setting commands to something that can
/// consume them appropriately - presumably to produce one script or another.
pub trait EnvVarEmitter {
    /// Emit an environment variable to set to some given value.
    fn emit_env_var(&mut self, variable: &str, value: &str);
}

/// Primitive module data - for instance, consistent naming, etc.
pub trait BasicIdentityModule {
    /// Get the raw name of this identity module.
    ///
    /// This is used to generate environment variable components, directory components, etc.
    fn raw_name(&self) -> &ModuleName;

    /// Get the name component to be used in environment variables
    ///
    /// Default slugifies it and capitalises it
    ///
    /// TODO: Find some way to Cow up the slugify functions.
    fn envvar_name_component(&self) -> Cow<'_, str> {
        use slugify_rs::slugify;
        Cow::Owned(slugify!(
            self.raw_name().as_ref().to_uppercase().as_ref(),
            separator = "_"
        ))
    }

    /// Get the name component to be used in paths
    ///
    /// TODO: Find some way to Cow up the slugify functions
    fn directory_name_component(&self) -> Cow<'_, OsStr> {
        use slugify_rs::slugify;
        Cow::Owned(slugify!(self.raw_name().as_ref(), separator = "-").into())
    }
}

/// See library documentation for the principles behind this.
pub trait ShellIdentityModule {
    /// Whether or not this needs a nice local directory on a per-identity basis to work with, that
    /// should be generated on login.
    fn needs_scratch_directory(&self) -> bool;

    /// Generate the components of the scratch directory
    ///
    /// The directory provided ALWAYS exists - since this function is only ever called when
    /// [`ShellIdentityModule::needs_scratch_directory`] is `true`. Default implementation does
    /// nothing.
    ///
    /// This is called only once on program initialisation during login - the directory should not
    /// be assumed to exist between user sessions (or on refresh as a potential feature).
    fn generate_scratch_directory(
        &self,
        _raw_scratch_directory: &std::path::Path,
    ) -> io::Result<()> {
        Ok(())
    }

    /// Specify the environment variables to set when enabling this shell identity module.
    ///
    /// Note that by default there also is a variable set per-module
    fn specify_environment_variables(&self, environment_variable_feed: &mut dyn EnvVarEmitter);
}

// {{{ Local module directory stuff.

/// Represents the result of a directory-local identity module.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum LocalModuleDirectory {
    /// This directory is not tagged with an identity but does come under the domain of this
    /// module.
    ///
    /// The path provided is the base directory managed by the program or system associated with
    /// this module.
    Untagged { base_dir: PathBuf },
    /// This directory is both managed by the given module and has an associated identity.
    Tagged {
        base_dir: PathBuf,
        identity_name: IdentityNameBuf,
    },
}

impl LocalModuleDirectory {
    pub fn base_dir(&self) -> &Path {
        match self {
            LocalModuleDirectory::Untagged { base_dir } => base_dir,
            LocalModuleDirectory::Tagged {
                base_dir,
                identity_name: _,
            } => base_dir,
        }
    }
}

// }}}

/// See library documentation for the principles behind directory-local identity modules
///
/// These should be identifiable from the directory alone - tagging should preferrably be done by
/// standard creation processes controlled by [`ShellIdentityModule`] environment variables for the
/// appropriate program, if possible.
pub trait DirectoryLocalIdentityModule {
    /// This function detects if a path is in a directory managed by this module, and if so,
    /// whether it is tagged with an identity or not.
    ///
    /// It should find the first directory under this module that is a parent of the path or the
    /// path itself (that is, if the path is managed, it should be returned), and check if it is
    /// tagged with an identity or not.
    ///
    /// Returns:
    ///   None if we are not in a directory controlled by this module
    ///   Some([`LocalModuleDirectory::Untagged`]) if we are in a directory that could be tagged
    ///   with an identity for this module but isn't (e.g. a git repository without metadata
    ///   information)
    ///   Some([`LocalModuleDirectory::Tagged`]) if we are in a directory under the domain of this
    ///   module that is tagged with an identity
    fn local_directory_info(
        &self,
        path: &Path,
    ) -> Result<Option<LocalModuleDirectory>, Box<dyn Error + 'static>>;
}

/// See library documentation for the principles behind user-global identity modules.
///
/// These are more hacky than the rest and have to be managed with a proper metadata file.
///
/// This system must encode multiple functions. First, it needs to list out *which* files and
/// directories are relevant to the module, relative to `~`.
///
/// Then, it needs to be able to check the state of these.
pub trait UserGlobalIdentityModule {
    /// Get a map of *module-local names for the linked/managed items* to the relative-paths
    /// that they manage and their configurations.
    ///
    /// These names are used when configuring "unmanaged" identity information, that is, marking
    /// files/directories as being of a certain identity without managing them using the symlink
    /// mechanism.
    ///
    /// See the documentation of [`user_global::UserGlobalModuleManagedPath`] and friends for more
    /// info on the specifics of that configuration.
    fn named_links(
        &self,
    ) -> &HashMap<&'_ user_global::ManagedPathName, user_global::UserGlobalModuleManagedPath<'_>>;
}

/// Defines access to modules.
///
/// Note there are no convenient defaults - this is because we can't conditionally create defaults
/// when we are Sized (or it breaks vtable generation >.<)
pub trait IdentityModule {
    /// Obtain the basic identity module associated with this.
    fn basic(&self) -> &dyn BasicIdentityModule;

    /// Shell identity module, if present.
    fn shell(&self) -> Option<&dyn ShellIdentityModule>;

    /// Directory-local identity module, if present.
    fn directory_local(&self) -> Option<&dyn DirectoryLocalIdentityModule>;

    /// User-global identity module, if present
    fn user_global(&self) -> Option<&dyn UserGlobalIdentityModule> { None }
}

/// Locate all conflicting managed directories in a collection of identity modules
pub fn find_user_global_module_directory_conflicts<'m, Mod: IdentityModule + ?Sized + 'm>(
    modules: impl Iterator<Item = &'m Mod>,
    xdg_context: &XdgDirsWithHome,
) -> std::io::Result<impl Iterator<Item = user_global::Conflict<'m, 'm, 'm>>> {
    // Only modules that have user global will be allowed though ^.^
    let pair_iter =
        modules.filter_map(|module| module.user_global().map(|ug| (ug, module.basic())));
    find_conflict_in_modules(pair_iter, xdg_context)
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize, JsonSchema)]
#[repr(transparent)]
/// Directories to load identity modules from.
///
/// Modules are overridden on a per-module basis with the following priority order, by default:
/// * $XDG_DATA_HOME/modules with standard xdg defaults (highest priority)
/// * ~/.local-identity/modules
/// * All the $XDG_DATA_DIRS/modules, with standard xdg defaults
/// * /etc/local-identity/modules
pub struct ModuleDirectoryPaths {
    paths: Vec<PathBuf>,
}

impl ModuleDirectoryPaths {
    /// Create an iterator over the paths from lowest priority, to highest priority.
    pub fn iterate_from_lowest_to_highest(
        &self,
    ) -> impl Iterator<Item = &'_ Path> + DoubleEndedIterator {
        self.iterate().rev()
    }

    /// Iterate over the paths, in order from highest priority to lowest priority
    pub fn iterate(&self) -> impl Iterator<Item = &'_ Path> + DoubleEndedIterator {
        self.paths.iter().map(AsRef::as_ref)
    }
}

impl Display for ModuleDirectoryPaths {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for path in self.iterate() {
            writeln!(f, "{}", path.display())?
        }
        Ok(())
    }
}

// We use a custom impl to get proper defaults.
impl Default for ModuleDirectoryPaths {
    fn default() -> Self {
        let mut paths = Vec::new();
        // Try XDG
        let xdgs = xdg::BaseDirectories::with_prefix("local-identity");
        let home = dirs::home_dir();

        // While we continue
        if home.is_none() {
            warn!("No home directory detected!");
        }

        /// Function to just add the .local-identity in ~
        fn add_home_inner(paths: &mut Vec<PathBuf>, homedir: Option<PathBuf>) {
            match homedir {
                Some(mut hd) => {
                    hd.push(".local_identity");
                    hd.push("modules");
                    paths.push(hd);
                }
                None => {}
            }
        }

        match xdgs {
            Ok(xdg) => {
                paths.push(xdg.get_data_home());
                add_home_inner(&mut paths, home);
                paths.extend(xdg.get_data_dirs().drain(..).map(|mut data_dir| {
                    data_dir.push("modules");
                    data_dir
                }));
            }
            Err(xdg_err) => {
                warn!("XDG directory calculation error: {}", xdg_err);
                warn!("Proceeding without XDG");
                add_home_inner(&mut paths, home);
            }
        };
        // TODO: switch to into_ok() since the conversion is infallible, when it stabilises nya
        paths.push(PathBuf::from_str("/etc/local-identity/modules").expect("is infallible ^.^"));
        Self { paths }
    }
}

/// Represents the directory path to load identities from. If relative, it is relative to the home
/// directory of the user. Default is $XDG_CONFIG_HOME/local-identity/identities/ if xdg is
/// calculated without errors, else it is ~/.local-identity/identities/ if XDG can't be constructed.
#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
#[repr(transparent)]
pub struct IdentityDirectoryPathUnresolved {
    /// Path to the directory from which identities are obtained. If relative, it is relative to
    /// the user home.
    identity_path: PathBuf,
}

/// Represents the resolved identity directory path.
#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
#[repr(transparent)]
pub struct IdentityDirectoryPathResolved<'v> {
    /// Absolute path to the directory from which identities are obtained.
    resolved_identity_path: Cow<'v, Path>,
}

impl Display for IdentityDirectoryPathUnresolved {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.identity_path.display().fmt(f)
    }
}

impl<'v> Display for IdentityDirectoryPathResolved<'v> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.resolved_identity_path.display().fmt(f)
    }
}

impl IdentityDirectoryPathUnresolved {
    /// Attempt to resolve the identity path, if it is a relative path.
    pub fn resolved_identity_path(&self) -> AnyResult<IdentityDirectoryPathResolved<'_>> {
        if self.identity_path.is_absolute() {
            Ok(IdentityDirectoryPathResolved {
                resolved_identity_path: Cow::Borrowed(&self.identity_path),
            })
        } else {
            match home_dir() {
                Some(hd_path) => Ok(IdentityDirectoryPathResolved {
                    resolved_identity_path: Cow::Owned(hd_path.inspecting_process_mut(|path| {
                        path.push(&self.identity_path);
                    })),
                }),
                None => Err(NoHomeError.into()),
            }
        }
    }
}

impl Default for IdentityDirectoryPathUnresolved {
    fn default() -> Self {
        match xdg::BaseDirectories::with_prefix("local-identity") {
            Ok(xdg) => {
                // We have xdg, so fill that in! nya
                Self {
                    identity_path: {
                        let mut id_path = xdg.get_config_home();
                        id_path.push("identities");
                        id_path
                    },
                }
            }
            Err(no_xdg) => {
                warn!("Could not calculate $XDG_CONFIG_HOME - error: {}", no_xdg);
                warn!("Using ~/.local-identity/identities instead");
                Self {
                    identity_path: PathBuf::from(".local-identity/identities"),
                }
            }
        }
    }
}

/// Configuration for modules & identities *in general* - for instance the per-identity-per-module directory location (by
/// default, this is in /tmp), the available module directory itself, and similar such things.
///
/// This is loaded from the place specified in $LOCAL_IDENTITY_ROOT_CONFIG environment variable if
/// specified, then $XDG_CONFIG_HOME/local-identity/config.toml if XDG can be calculated, then
/// ~/.local-identity/config.toml if there is an error calculating XDG directories.
#[derive(Clone, Default, PartialEq, Eq, Debug, Serialize, Deserialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GlobalConfig {
    /// Directories to load modules & config from - in override order
    #[serde(default)]
    module_directories: ModuleDirectoryPaths,
    /// Directory where identity specifications should be found.
    #[serde(default)]
    identity_directory: IdentityDirectoryPathUnresolved,
}

impl Display for GlobalConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("MODULE-DIRECTORIES\n")?;
        self.module_directories.fmt(f)?;
        f.write_str("IDENTITY-DIRECTORIES\n")?;
        self.identity_directory.fmt(f)
    }
}

impl GlobalConfig {
    /// Attempt to resolve the identity directory path.
    pub fn resolved_identity_directory_path(&self) -> AnyResult<IdentityDirectoryPathResolved> {
        self.identity_directory.resolved_identity_path()
    }

    /// Get the unresolved identity path
    pub fn unresolved_identity_directory_path(&self) -> &IdentityDirectoryPathUnresolved {
        &self.identity_directory
    }

    pub fn module_paths(&self) -> &ModuleDirectoryPaths { &self.module_directories }

    /// Attempt to get the filepath from the root config. If the environment variable is not
    /// specified and there is also no home dir, it will error out.
    pub fn root_config_location() -> AnyResult<PathBuf> {
        Ok(match std::env::var("LOCAL_IDENTITY_ROOT_CONFIG") {
            Ok(root_config_location) => PathBuf::from(root_config_location),
            Err(env_var_error) => {
                debug!(
                    "could not get root config environment variable - error: {}",
                    env_var_error
                );
                debug!("attempting xdg calculation");
                match xdg::BaseDirectories::with_prefix("local-identity") {
                    Ok(xdgctx) => xdgctx.get_config_home().inspecting_process_mut(|path| {
                        path.push("config.toml");
                    }),
                    Err(xdg_err) => {
                        warn!(
                            "XDG calculation error when finding root config: {}",
                            xdg_err
                        );
                        warn!("Using ~/.local-identity/config.toml");
                        let homedir_cfg = dirs::home_dir().ok_or(NoHomeError)?;

                        homedir_cfg.inspecting_process_mut(|path| {
                            path.push(".local-identity");
                            path.push("config.toml");
                        })
                    }
                }
            }
        })
    }

    /// Attempt to load the global module configuration in general, spitting out Result when the provided
    /// file is invalid.
    ///
    /// If the file does not exist, then fill in from defaults.
    pub fn try_load() -> AnyResult<Self> {
        info!("Attempting to load the root configuration path.");
        let root_config_location = Self::root_config_location()?;
        info!(
            "Attempting to load the root configuration from {}",
            root_config_location.display()
        );
        Ok(match std::fs::read(&root_config_location) {
            Ok(cfg_bytes) => {
                debug!("Loaded bytes from {}", root_config_location.display());
                toml::de::from_slice(&cfg_bytes)?
            }
            Err(io_err) => {
                info!("Could not read root config: {}", io_err);
                debug!("Using default root config");
                Default::default()
            }
        })
    }
}

/// The name and location of a single module file, unresolved from whether or not there actually is
/// a path.
#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct UnresolvedModuleLocation {
    /// Path to the module definition file
    module_file_path: Option<PathBuf>,
    /// Path to the module data directory, if it exists.
    module_directory_path: Option<PathBuf>,
}

/// A fully resolved module location - module file and maybe module directory.
#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct ModuleLocation {
    /// Path to the module .toml file ^.^
    module_file_path: PathBuf,
    /// Path to module data directory.
    module_directory_path: Option<PathBuf>,
}

impl UnresolvedModuleLocation {
    /// Provide a location with only a directory path.
    ///
    /// Primarily used internally when merging with an existing entry in the map of module names to
    /// files and directories.
    fn provide_module_directory_path(p: PathBuf) -> Self {
        Self {
            module_file_path: None,
            module_directory_path: Some(p),
        }
    }

    /// Provide a location with only a config file path. This is primarily used internally when
    /// merging entries from all the directories ^.6
    fn provide_module_file_path(p: PathBuf) -> Self {
        Self {
            module_file_path: Some(p),
            module_directory_path: None,
        }
    }

    /// Override locations in self with those provided in other, if other has one.
    ///
    /// Used when merging directories ^.^
    pub fn override_with(&mut self, overriding_data: Self) {
        self.module_file_path = overriding_data
            .module_file_path
            .or_else(|| self.module_file_path.take());
        self.module_directory_path = overriding_data
            .module_directory_path
            .or_else(|| self.module_directory_path.take());
    }

    /// Load module data from a single module directory into a provided hashmap, overriding if
    /// necessary.
    pub fn load_module_overrides_from_single_directory(
        module_directory: &Path,
        current_data_and_path_maps: &mut HashMap<ModuleNameBuf, UnresolvedModuleLocation>,
    ) {
        let files_and_directories = match std::fs::read_dir(module_directory) {
            Ok(v) => v,
            Err(opening_module_dir_err) => {
                info!(
                    "Error opening module directory {}: {}",
                    module_directory.display(),
                    opening_module_dir_err
                );
                return;
            }
        };
        // Sorts out the files and directories in the directory, mapping from a .toml-stripped
        // filename, to the full path to the file if it exists and to the full path of the
        // directory if it exists nya.
        for maybe_dir_entry in files_and_directories {
            let dir_entry = match maybe_dir_entry {
                Ok(v) => v,
                Err(access_entry_error) => {
                    warn!(
                        "Error accessing entry in module directory, skipping entry: {}",
                        access_entry_error
                    );
                    break;
                }
            };
            // Note we use this rather than dir_entry.file_type() becuase that does not traverse
            // symlinks >.<
            let entry_path = dir_entry.path();
            let meta = match std::fs::metadata(&entry_path) {
                Ok(v) => v,
                Err(access_metadata_error) => {
                    info!(
                        "Error accessing metadata for module directory entry {}: {}",
                        entry_path.display(),
                        access_metadata_error
                    );
                    break;
                }
            };
            let osstring_filename = entry_path.file_name().expect(
                "only terminates when path endpoint is .., and direntries do not list that",
            );

            let raw_string_filename = match is_unicode_with_suffix_and_name(osstring_filename, "") {
                Ok(v) => v,
                Err(validation_err) => {
                    warn!(
                        "non-unicode file or directory in module directory - {}",
                        validation_err
                    );
                    break;
                }
            };
            if meta.is_dir() {
                current_data_and_path_maps.insert_or_modify(
                    // [modulename]/ and [modulename].toml are the relevant components nya
                    <&ModuleName>::from(raw_string_filename),
                    Self::provide_module_directory_path(entry_path.clone()),
                    |_, og, new| og.override_with(new),
                );
            } else if meta.is_file() {
                const FILE_EXTENSION: &str = ".toml";
                let module_name = match has_suffix_and_name(raw_string_filename, FILE_EXTENSION) {
                    Ok(v) => v,
                    Err(e) => {
                        warn!(
                            "invalid module definition filename in module folder {}: {}",
                            raw_string_filename, e
                        );
                        break;
                    }
                };

                info!(
                    "Found module file {} for module {}",
                    raw_string_filename, module_name
                );
                current_data_and_path_maps.insert_or_modify(
                    <&ModuleName>::from(module_name),
                    Self::provide_module_file_path(entry_path.clone()),
                    |_, og, new| og.override_with(new),
                );
            } else {
                unreachable!("We used a symlink-traversing method ^.^")
            }
        }
        // Filter out things without a filepath.
    }

    /// Load modules from the set of module directory paths, without resolving valid modules.
    pub fn load_module_overrides(
        module_paths: &ModuleDirectoryPaths,
    ) -> HashMap<ModuleNameBuf, UnresolvedModuleLocation> {
        let mut pathdata = HashMap::new();
        for path in module_paths.iterate_from_lowest_to_highest() {
            Self::load_module_overrides_from_single_directory(path, &mut pathdata);
        }
        pathdata
    }

    /// Attempt to resolve this into a real module location if it has a config file path.
    pub fn resolve(self) -> Option<ModuleLocation> {
        match (self.module_file_path, self.module_directory_path) {
            (Some(resolved_path), dirpath) => Some(ModuleLocation {
                module_file_path: resolved_path,
                module_directory_path: dirpath,
            }),
            (None, Some(dirpath)) => {
                warn!(
                    "Module directory path {} has no associated module configuration file, \
                     ignoring.",
                    dirpath.display()
                );
                None
            }
            (None, None) => None,
        }
    }

    /// Fully resolve all the modules in the module name map ^.^
    pub fn resolve_all(
        unresolved_collection: HashMap<ModuleNameBuf, UnresolvedModuleLocation>,
    ) -> HashMap<ModuleNameBuf, ModuleLocation> {
        unresolved_collection
            .into_iter()
            .filter_map(|(module_name, unresolved)| unresolved.resolve().map(|v| (module_name, v)))
            .collect()
    }
}

/// Fully resolved identity location.
#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, JsonSchema, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct IdentityLocation {
    location: PathBuf,
}

impl Display for IdentityLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.location.display().fmt(f)
    }
}

const IDENTITY_FILE_SUFFIX: &str = ".toml";

impl IdentityLocation {
    /// Attempt to load identities from the specified path.
    ///
    /// If the path doesn't exist, it will try and create the directory.
    pub fn try_load(
        directory_config: &IdentityDirectoryPathUnresolved,
    ) -> AnyResult<HashMap<IdentityNameBuf, Self>> {
        let identity_directory_path = directory_config.resolved_identity_path()?;
        let identity_directory_path = identity_directory_path.resolved_identity_path.as_ref();
        let mut id_map = HashMap::new();
        info!(
            "Attempting to read identities from {}",
            identity_directory_path.display()
        );
        let dir_entries = match std::fs::read_dir(identity_directory_path) {
            Ok(v) => v,
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
                warn!(
                    "Identity directory {} did not exist, attempting to create.",
                    identity_directory_path.display()
                );
                std::fs::create_dir_all(identity_directory_path)?;
                std::fs::read_dir(identity_directory_path)?
            }
            Err(v) => bail!(v),
        };

        for dir_entry in dir_entries {
            let dir_entry = dir_entry?;
            let entry_path = dir_entry.path();
            let symlink_traversed_metadata = std::fs::metadata(&entry_path)?;
            if symlink_traversed_metadata.is_dir() {
                warn!(
                    "Directory {} found in the identity definition directory",
                    entry_path.display()
                );
            } else if symlink_traversed_metadata.is_file() {
                let identity_name =
                    match path_is_unicode_with_suffix_and_name(&entry_path, IDENTITY_FILE_SUFFIX) {
                        Ok(v) => v,
                        Err(e) => {
                            warn!("Invalid file filename in identity directory: {}", e);
                            break;
                        }
                    };
                id_map.insert(IdentityNameBuf::from(identity_name.to_owned()), Self {
                    location: entry_path,
                });
            } else {
                unreachable!("We traversed all symlinks, in theory ^.^");
            }
        }
        Ok(id_map)
    }

    pub fn location_ref(&self) -> &Path { &self.location }

    /// Build an identity file location from a resolved directory and a name, regardless of if the
    /// location exists or not. Can be used as a target path for new identities.
    pub fn build_from_name(
        resolved_dir: &IdentityDirectoryPathResolved,
        name: &IdentityName,
    ) -> Self {
        let full_file_path = resolved_dir
            .resolved_identity_path
            .as_ref()
            .join(name.apply_fn(|inbuf| {
                AsRef::<str>::as_ref(inbuf)
                    .to_owned()
                    .inspecting_process_mut(|a| a.push_str(IDENTITY_FILE_SUFFIX))
            }));
        Self {
            location: full_file_path,
        }
    }
}

/// Holds the raw binary content of identity files, as well as where they were loaded from.
pub struct IdentityFileContents {
    data: HashMap<IdentityNameBuf, (IdentityLocation, Vec<u8>)>,
}

impl IdentityFileContents {
    /// Attempt to load the raw contents from all the locations listed in the provided hashmap meow
    pub fn load_from_locations(
        content: HashMap<IdentityNameBuf, IdentityLocation>,
    ) -> AnyResult<Self> {
        let mut res =
            HashMap::<IdentityNameBuf, (IdentityLocation, Vec<u8>)>::with_capacity(content.len());
        for (name, location) in content.into_iter() {
            info!(
                "Loading identity {} from file on disk at {}",
                name,
                location.location_ref().display()
            );
            let contents = std::fs::read(location.location_ref())?;
            res.insert(name, (location, contents));
        }
        Ok(Self { data: res })
    }
}

/// A fully successful parse of identity file contents from [IdentityFileContents]
pub struct LoadedIdentityFileContents<'content_map> {
    fully_loaded: HashMap<
        &'content_map IdentityName,
        (Identity<'content_map>, IdentityFinalised<'content_map>),
    >,
}

impl<'a> LoadedIdentityFileContents<'a> {
    /// Use the data stored in the raw contents object and attempt to load finalised identities
    /// from it ^.^
    pub fn try_load_from(contents: &'a IdentityFileContents) -> AnyResult<Self> {
        let mut accumulated = HashMap::<&'a IdentityName, _>::with_capacity(contents.data.len());
        for (name_buf, (location, raw_u8s)) in contents.data.iter() {
            let valid_utf8_str: &str = match std::str::from_utf8(raw_u8s) {
                Ok(v) => v,
                Err(e) => {
                    error!("invalid utf8 in identity file at {} - {}", &location, &e);
                    return Err(e.into());
                }
            };
            let valid_identity_parsed: Identity<'a> =
                match toml::from_str::<'a, Identity<'a>>(valid_utf8_str) {
                    Ok(v) => {
                        info!(
                            "Successfully parsed identity {} [from: '{}']",
                            name_buf, location
                        );
                        if v.core_identity.name != name_buf.as_ref() {
                            error!(
                                "name in identity file '{}' - {} - does not match name indicated \
                                 by the filepath ({})",
                                location, v.core_identity.name, name_buf
                            );
                            bail!("Non-matching identity name and identity file name.");
                        };
                        v
                    }
                    Err(inner) => {
                        error!("Error parsing identity file {} - {}", location, inner);
                        return Err(inner.into());
                    }
                };
            info!("Postprocessing identity {name_buf}");
            let finalised_identity = valid_identity_parsed.with_cascaded_prerequisites(());
            match accumulated.insert(
                name_buf.as_ref(),
                (valid_identity_parsed, finalised_identity),
            ) {
                None => {}
                Some(_already_existing) => bail!(
                    "Could not load identity {} from {} because an identity with that name \
                     already existed.",
                    name_buf,
                    location
                ),
            }
        }
        Ok(Self {
            fully_loaded: accumulated,
        })
    }

    pub fn inner_map(&self) -> &HashMap<&'a IdentityName, (Identity<'a>, IdentityFinalised<'a>)> {
        &self.fully_loaded
    }
}

#[cfg(test)]
pub mod test_utilities {
    use std::collections::HashMap;

    use super::EnvVarEmitter;

    /// Basic environment variable emitter that simply glues pairs of emitted environment variables
    /// onto a [`Vec`] and also into a hashmap.
    #[derive(Default, Clone, Debug)]
    pub struct EnvTrackingEmitter {
        pub emitted_vars: Vec<(String, String)>,
        pub emitted_vars_hashed: HashMap<String, String>,
    }

    impl EnvVarEmitter for EnvTrackingEmitter {
        fn emit_env_var(&mut self, variable: &str, value: &str) {
            self.emitted_vars
                .push((variable.to_owned(), value.to_owned()));
            self.emitted_vars_hashed
                .insert(variable.to_owned(), value.to_owned());
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
