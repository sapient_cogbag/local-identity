//! Module for commandlines using `argh`

pub mod daemon;
pub mod debug;
pub mod docgen;
pub mod identity;
pub mod modules;

pub trait CmdExec {
    /// Possible error output
    type Error: std::fmt::Debug;

    /// how to actually run this command ^.^
    fn exec(self) -> Result<(), Self::Error>;
}

/// Like [CmdExec], but also has passthrough data ^.^ (perhaps from options higher in the command
/// chain?)
pub trait CmdExecWithPassthrough {
    /// Possible error output
    type Error: std::fmt::Debug;

    /// Type that supercommands must pass through to the subcommands.
    type Passthrough;

    /// How to execute self
    fn exec_passthrough(self, passthrough: Self::Passthrough) -> Result<(), Self::Error>;
}

impl<T: CmdExec> CmdExecWithPassthrough for T {
    type Error = <T as CmdExec>::Error;
    type Passthrough = ();

    #[inline]
    fn exec_passthrough(self, _: Self::Passthrough) -> Result<(), Self::Error> {
        CmdExec::exec(self)
    }
}

// For logging see:
// https://docs.rs/stderrlog/0.5.1/stderrlog/#structs
//
// nya
/// Logging
#[derive(clap::Args)]
pub struct LoggingArgs {
    #[clap(short = 'v', long, default_value = "1")]
    /// How much to log.
    ///
    /// 0 => Error only
    ///
    /// 1 => Error + Warning
    ///
    /// 2 => Error + Warning + Info
    ///
    /// 3 => Error + Warning + Info + Debug
    ///
    /// 4+ => Error = Warning + Info + Debug + Trace
    pub verbosity: usize,

    #[clap(short = 'q', long)]
    /// Silence all stderr output
    pub quiet: bool,

    #[clap(long, default_value = "ms")]
    /// Indicate the precision of the timestamp to put in logs.
    ///
    /// If None, don't output any timestamp at all.
    pub timestamp_precision: stderrlog::Timestamp,
}

impl LoggingArgs {
    /// Apply the parameters to a stderrlog
    pub fn apply_to_stderrlog<'a>(
        &self,
        logger: &'a mut stderrlog::StdErrLog,
    ) -> &'a mut stderrlog::StdErrLog {
        logger
            .verbosity(self.verbosity)
            .quiet(self.quiet)
            .timestamp(self.timestamp_precision)
    }
}

#[derive(clap::Parser)]
/// Manage multiple identities for a single user, and reduce the risk of identity leaks when
/// using programs that connect to the internet.
///
/// At the moment, git is the primary focus for this program.
#[clap(author, version)]
pub struct LocalIdentityCmd {
    #[clap(flatten)]
    pub logargs: LoggingArgs,
    #[clap(subcommand)]
    pub subcommand: L1Commands,
}

impl CmdExecWithPassthrough for LocalIdentityCmd {
    type Error = <L1Commands as CmdExec>::Error;
    // this is the module name
    type Passthrough = String;

    /// Passthrough is the module path of root ^.^
    fn exec_passthrough(self, module_path_string: Self::Passthrough) -> Result<(), Self::Error> {
        let mut logger = stderrlog::new();
        logger
            // Note that we need to make this be the crate root, currently we're in [crate::cmd], which
            // means that this module path would exclude logging from everything else >.< nya, if
            // we didn't pass through the value from main
            .module(module_path_string)
            .color(stderrlog::ColorChoice::Auto);
        self.logargs.apply_to_stderrlog(&mut logger).init()?;

        match self.subcommand.exec() {
            Ok(_) => Ok(()),
            Err(e) => {
                error!("Command failed with error.");
                error!("{e}");
                Err(e)
            }
        }
    }
}

#[derive(clap::Subcommand)]
pub enum L1Commands {
    #[clap(name = "docgen")]
    DocGen(docgen::DocumentationGenerator),
    #[clap(name = "daemon")]
    Daemon(daemon::LocalIdentityDaemonCmd),
    #[clap(name = "debug")]
    Debug(debug::DebugCmd),
    #[clap(name = "identity", subcommand)]
    Identity(identity::Identity),
}

impl CmdExec for L1Commands {
    type Error = anyhow::Error;

    fn exec(self) -> Result<(), Self::Error> {
        match self {
            L1Commands::DocGen(v) => v.exec()?,
            L1Commands::Daemon(d) => d.exec()?,
            L1Commands::Debug(d) => d.exec()?,
            L1Commands::Identity(i) => i.exec()?,
        };
        Ok(())
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use log::error;
