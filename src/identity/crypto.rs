//! Part of the identity specification for cryptographic identity. This is oriented around Git,
//! which can use gpg keys but *also* ssh keys and x509 keys ^.^

use crate::identity::CascadingDefaults;

use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use super::PrimitiveIdentityFinalised;

/// GPG identity components ^.^
#[derive(Serialize, Deserialize, Default, Debug, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GpgIdentity<'a> {
    /// Some raw identifier for the key - may be name, email, GPG key hash, GPG keygrip, or
    /// anything else GPG can use to pull up a key ^.^
    #[serde(default = "Default::default")]
    // Note that schemars currently doesn't like non-standard (zero arg) serde defaults
    // nya
    #[schemars(default)]
    pub key_identifier: Option<&'a str>,
}

/// Like [GpgIdentity], but with all defaults filled in ^.^
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct GpgIdentityFinalised<'a> {
    pub key_identifier: &'a str,
}

/// GPG Identity, but with finalised default values.
///
/// Pulls key identifiers when not provided from:
/// * [`super::PrimitiveIdentityFinalised::email`]
/// * then [`super::PrimitiveIdentityFinalised::full_name`]
impl<'a> CascadingDefaults<'a> for GpgIdentity<'a> {
    type Finalised = GpgIdentityFinalised<'a>;
    type Prerequisites = PrimitiveIdentityFinalised<'a>;

    fn with_cascaded_prerequisites(&self, other_values: Self::Prerequisites) -> Self::Finalised {
        let key_identifier: Option<&'a str> = self.key_identifier;
        let key_identifier: Option<&'a str> = key_identifier.or(other_values.email);
        let key_identifier: &'a str = key_identifier.unwrap_or(other_values.full_name);

        GpgIdentityFinalised { key_identifier }
    }
}

/// SSH Identity
///
/// Note that if you use `gpg-agent` as your `ssh-agent`, this will not grab from that by default.
/// You need to manually figure out the ssh public key, probably by doing `gpg --export-ssh-key
/// <key>`. If your GPG *primary* key has the Authentication flag needed to
/// use SSH rather than one of it's listed subkeys, then the primary key ID might need an
/// exclamation mark "!" at the end ^.^ https://dev.gnupg.org/T2957
/// With a public key, generally the private keys can be worked with via the public keys ^.^
#[derive(Serialize, Deserialize, Debug, Clone, Copy, JsonSchema, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SshIdentity<'a> {
    /// Some raw identifier for the key - may be path, name, inline public key, etc. For an SSH
    /// key ^.^.
    pub key: &'a str,
}

/// Like SSH identity, but I know basically nothing about the programs involved to make this
/// function. However, git *does* allow signing with x509/PKI keys if you specify a proper program
/// for it (and iirc GPG might have some support for this but i don't *know*?), and it can
/// be a form of identity.
///
/// This also needs no finalisation.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct X509CertificateIdentity<'a> {
    /// Raw cert identifier for use with programs that accept them in whatever format.
    pub certificate_identifier: &'a str,
}

/// Utility struct for pooling together the cryptographic identities.
#[derive(Clone, Debug, Copy)]
pub struct CryptoIdentities<'a> {
    pub gpg_identity: Option<GpgIdentityFinalised<'a>>,
    pub ssh_identity: Option<SshIdentity<'a>>,
    pub x509_identity: Option<X509CertificateIdentity<'a>>,
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
