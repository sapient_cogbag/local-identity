//! Module containing the relevant git identity components. Mostly, gpg signing keys and author
//! names.
use crate::util::{is_default, UniversalInspect};
use log::warn;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use super::{crypto::CryptoIdentities, CascadingDefaults, PrimitiveIdentityFinalised};

/// Credentials for a specific url in git or defaults
///
/// See `credential.<url>.*` in `git-config` manpage ^.^
#[derive(Deserialize, Serialize, Default, Debug, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GitCredential<'a> {
    #[serde(borrow, default)]
    /// credential.username
    pub username: Option<&'a str>,

    #[serde(borrow, default)]
    /// credential.helper
    pub helper: Option<&'a str>,

    #[serde(default = "crate::util::false_val")]
    /// credential.useHttpPath
    pub use_http_path: bool,
}

/// `git-config` `format.from` configuration value, if present.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, JsonSchema)]
#[serde(tag = "default-from-mode", rename_all = "kebab-case")]
pub enum FormatFrom<'a> {
    /// Directly use commit authors (output -> boolean value false)
    RawCommitAuthors,
    /// Use commit identity (output -> boolean value true)
    CommitIdentity,
    /// Use a custom name and email (output -> "name <email>")
    ///
    /// Defaults are provided for each of these values when not present
    ///
    /// However, if there is no default email deduceable, AND none provided, there is a potential
    /// issue >.<. We delegate this to the finalised version.
    NameAndEmail {
        #[serde(default)]
        name: Option<&'a str>,
        #[serde(default)]
        email: Option<&'a str>,
    },
}

/// Version of [`FormatFrom`] with the defaults filled in ^.^
#[derive(Deserialize, Serialize, Debug, Clone, Copy, JsonSchema)]
#[serde(tag = "default-from-mode", rename_all = "kebab-case")]
pub enum FormatFromFinalised<'a> {
    RawCommitAuthors,
    CommitIdentity,
    NameAndEmail {
        name: &'a str,
        // There may be a rare case with *no* specified emails *at all* nya. This is very annoying,
        // however this will get handled.
        email: Option<&'a str>,
    },
}

impl<'a> CascadingDefaults<'a> for Option<FormatFrom<'a>> {
    type Finalised = FormatFromFinalised<'a>;
    type Prerequisites = PrimitiveIdentityFinalised<'a>;

    fn with_cascaded_prerequisites(&self, other_values: Self::Prerequisites) -> Self::Finalised {
        match (self, other_values) {
            // No email - use raw commit author ids in this case nya.
            (None, PrimitiveIdentityFinalised { email: None, .. }) => {
                FormatFromFinalised::RawCommitAuthors
            }
            (
                None,
                PrimitiveIdentityFinalised {
                    full_name,
                    email: Some(email),
                    ..
                },
            ) => FormatFromFinalised::NameAndEmail {
                name: full_name,
                email: Some(email),
            },
            (Some(FormatFrom::RawCommitAuthors), _) => FormatFromFinalised::RawCommitAuthors,
            (Some(FormatFrom::CommitIdentity), _) => FormatFromFinalised::CommitIdentity,
            // Override when unspecified.
            (
                Some(FormatFrom::NameAndEmail { name, email }),
                PrimitiveIdentityFinalised {
                    full_name,
                    email: default_email,
                    ..
                },
            ) => FormatFromFinalised::NameAndEmail {
                name: name.unwrap_or(full_name),
                email: email.or(default_email)
                    .inspecting_process(|v| if v.is_none() { warn!(target: "git-identity", "Setting format.from git property without an email!") }),
            },
        }
    }
}

/// Represents the signing key git should use.
#[derive(Deserialize, Serialize, Debug, Clone, JsonSchema)]
#[serde(tag = "key-type", rename_all = "kebab-case")]
pub enum GitSigningKey<'a> {
    Gpg {
        /// Key identifier. Default is the global gpg identity if present.
        ///
        /// Corresponds to git property `user.signingkey`
        gpg_key_identifier: Option<&'a str>,
    },
    Ssh {
        /// Path to ssh private key or inline ssh private key as specified by `man git-config`
        /// `user.signingkey` property
        ssh_key: Option<&'a str>,
    },
    X509 {
        /// I don't know how you're meant to specify these certs for `user.signingkey`, but this
        /// will let you do it!
        x509_certificate_identifier: Option<&'a str>,
    },
    NoSign,
}

/// Represents the signing key git should use, but finalised.
#[derive(Serialize, Debug, Clone, Copy)]
#[serde(tag = "key-type", rename_all = "kebab-case")]
pub enum GitSigningKeyFinalised<'a> {
    Gpg {
        /// Key identifier. Default is the global gpg identity if present.
        ///
        /// Corresponds to git property `user.signingkey`
        ///
        /// If unset, this will result in no user.signingkey being set and whatever the global
        /// default happens to be being used. Luckily as long as you have a proper gpg identity
        /// you'll get that with no further config ^.^
        gpg_key_identifier: Option<&'a str>,
    },
    Ssh {
        /// Path to ssh private key or inline ssh private key as specified by `man git-config`
        /// `user.signingkey` property
        ssh_key: Option<&'a str>,
    },
    X509 {
        /// I don't know how you're meant to specify these certs for `user.signingkey`, but this
        /// will let you do it!
        x509_certificate_identifier: Option<&'a str>,
    },
    NoSign,
}

impl<'a> CascadingDefaults<'a> for Option<GitSigningKey<'a>> {
    type Finalised = GitSigningKeyFinalised<'a>;
    type Prerequisites = CryptoIdentities<'a>;

    // If it *is* specified, then we just use the specified value, with defaults for the type
    // given by the passed pre-parsed identity structs. If *not* however, then the
    // priority order is GPG, SSH, x509, None depending on what the user has.
    fn with_cascaded_prerequisites(
        &self,
        all_crypto_identities: Self::Prerequisites,
    ) -> Self::Finalised {
        if let Some(gitspecification) = self {
            match gitspecification {
                GitSigningKey::Gpg { gpg_key_identifier: key_identifier } => GitSigningKeyFinalised::Gpg {
                    gpg_key_identifier: key_identifier
                        .or(all_crypto_identities.gpg_identity.map(|v| v.key_identifier))
                        .inspecting_process(|v| if v.is_none() { warn!(target: "git-identity", "GPG git identity specified without either git-specific or identity-wide GPG key specifier. This will use the default gpg key for your user profile.") } ),
                },
                GitSigningKey::Ssh { ssh_key } => GitSigningKeyFinalised::Ssh {
                    ssh_key: ssh_key
                        .or(all_crypto_identities.ssh_identity.map(|v| v.key))
                        .inspecting_process(|v| if v.is_none() { warn!(target: "git-identity", "SSH git identity specified without either git-specific or identity-wide SSH key. If you're using GPG-based SSH keys, take a look at `gpg --export-ssh-key`.") }),
                },
                GitSigningKey::X509 {
                    x509_certificate_identifier: x509_certificate_string,
                } => GitSigningKeyFinalised::X509 {
                    x509_certificate_identifier: x509_certificate_string
                        .or(all_crypto_identities.x509_identity.map(|v| v.certificate_identifier))
                        .inspecting_process(|v| if v.is_none() { warn!(target: "git-identity", "X509 git signing identity specified without either git-specific or identity wide X509 certificate identifier.") }),
                },
                GitSigningKey::NoSign => GitSigningKeyFinalised::NoSign,
            }
        } else {
            // GPG, then SSH, then x509 ^.^
            all_crypto_identities
                .gpg_identity
                .map(|gpg| GitSigningKeyFinalised::Gpg {
                    gpg_key_identifier: Some(gpg.key_identifier),
                })
                .or_else(|| {
                    all_crypto_identities
                        .ssh_identity
                        .map(|ssh| GitSigningKeyFinalised::Ssh {
                            ssh_key: Some(ssh.key),
                        })
                })
                .or_else(|| {
                    all_crypto_identities
                        .x509_identity
                        .map(|x509| GitSigningKeyFinalised::X509 {
                            x509_certificate_identifier: Some(x509.certificate_identifier),
                        })
                })
                .unwrap_or(GitSigningKeyFinalised::NoSign)
        }
    }
}

/// This enum uses inline/pretags i.e. the variant is *what the enum is labelled with* ^.^
#[derive(Deserialize, Serialize, Debug, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub enum GitIMAPLoginHostTunnel<'a> {
    Host(&'a str),
    TunnelCommand(&'a str),
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GitIMAPLoginUserHostPort<'a> {
    pub username: &'a str,
    #[serde(borrow, flatten)]
    pub host_or_tunnel: GitIMAPLoginHostTunnel<'a>,
    pub port: Option<u16>,
}

/// Represents imap login details. When email is present, it will try and split on `@` to obtain a
/// host (prefixed with `imaps://`) and username. If not, it will default to using the name
/// parameter as username with localhost as host.
///
/// Basically, if IMAP is enabled at all, then it will pull defaults from email, but if either of
/// the username or host is set then both must be set ^.^
///
/// Password must be specified.
#[derive(Deserialize, Serialize, Default, Debug, Clone, Copy, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GitIMAPLogin<'a> {
    #[serde(borrow, default, flatten)]
    pub username_at_host: Option<GitIMAPLoginUserHostPort<'a>>,
    #[serde(borrow, default, flatten)]
    pub password: Option<&'a str>,
}

#[derive(Serialize, Debug, Clone, Copy)]
pub struct GitIMAPLoginFinalised<'a> {
    pub password: Option<&'a str>,
    pub username_at_host: GitIMAPLoginUserHostPort<'a>,
}

impl<'a> CascadingDefaults<'a> for Option<GitIMAPLogin<'a>> {
    type Finalised = GitIMAPLoginFinalised<'a>;
    type Prerequisites = PrimitiveIdentityFinalised<'a>;

    fn with_cascaded_prerequisites(
        &self,
        primitive_identity: Self::Prerequisites,
    ) -> Self::Finalised {
        let email_default_user_host: (&'a str, &'a str) = primitive_identity
            .email
            .and_then(|raw_email: &'a str| raw_email.split_once('@'))
            .unwrap_or((primitive_identity.name.as_ref(), "localhost"));
        let email_default_user = email_default_user_host.0;
        let email_default_host_tun = GitIMAPLoginHostTunnel::Host(email_default_user_host.1);
        match self {
            Some(specified_imap_login) => GitIMAPLoginFinalised {
                username_at_host: GitIMAPLoginUserHostPort {
                    username: specified_imap_login
                        .username_at_host
                        .map(|a| a.username)
                        .unwrap_or(email_default_user),
                    host_or_tunnel: specified_imap_login
                        .username_at_host
                        .map(|a| a.host_or_tunnel)
                        .unwrap_or(email_default_host_tun),
                    port: specified_imap_login.username_at_host.and_then(|a| a.port),
                },
                password: specified_imap_login.password,
            },
            None => GitIMAPLoginFinalised {
                username_at_host: GitIMAPLoginUserHostPort {
                    username: email_default_user,
                    host_or_tunnel: email_default_host_tun,
                    port: None,
                },
                password: None,
            },
        }
    }
}

/// sendmail/SMTP - Otherwise Known As Pain
///
/// Git provides a whole credentials mechanism for getting usernames/passwords/etc for the
/// `sendemail` program and interaction for users via credentials.
///
/// Here, we just set any usernames and From-like fields, as well as allowing the use of the
/// smtp.identity.* feature ^.^
#[derive(Deserialize, Serialize, Default, Debug, Clone, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct SendemailIdentity<'a> {
    #[serde(borrow, default)]
    /// Name of the sendmail.identity.*
    ///
    /// Default to the most basic single name
    pub identity_name: Option<&'a str>,
    #[serde(borrow, default)]
    /// Default from address for emails sent by sendmail
    ///
    /// If email is set, this is set to that.
    pub from_addr: Option<&'a str>,
    /*
     * NO WAY TO SET THIS IN GIT CONFIG nyaaa
    /// The address others should reply to. Defaults to the value of [Self::from_addr], cascading
    /// back through defaults.
    #[serde(borrow, default)]
    pub reply_to_addr: Option<&'a str>,
    */
    #[serde(borrow, default)]
    /// Envelope sender address. Unspecified means that git will just use your from address.
    pub envelope_sender: Option<&'a str>,

    /// The user on the smtp server - which should pull from git credentials.
    ///
    /// Defaults to the most basic single name.
    #[serde(borrow, default)]
    pub smtp_user: Option<&'a str>,
    /// Other options to associate with the smtp identity. For proper identity separation, it's
    /// worth looking at `sendemail.aliasFile` to separate social graphs ^.^, but that's a more
    /// extreme threat model.
    #[serde(borrow, flatten, default = "HashMap::new")]
    pub extra_per_identity_keys: HashMap<&'a str, &'a str>,
}

#[derive(Serialize, Debug, Clone, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct SendemailIdentityFinalised<'a> {
    /// Name of the sendmail.identity.*
    ///
    /// Default to the most basic single name
    pub identity_name: &'a str,
    /// Default from address for emails sent by sendmail
    ///
    /// If email is set, this is set to that.
    pub from_addr: Option<&'a str>,

    /*
    /// The address others should reply to. Defaults to the value of [Self::from_addr], cascading
    /// back through defaults.
    /// NO WAY TO SET THIS IN GIT CONFIG
    pub reply_to_addr: Option<&'a str>,
    */
    /// Envelope sender address. Unspecified means that git will just use your from address.
    pub envelope_sender: Option<&'a str>,

    /// The user on the smtp server - which should pull from git credentials.
    ///
    /// Defaults to the most basic single name.
    pub smtp_user: &'a str,
    /// Other options to associate with the smtp identity. For proper identity separation, it's
    /// worth looking at `sendemail.aliasFile` to separate social graphs ^.^, but that's a more
    /// extreme threat model.
    pub extra_per_identity_keys: HashMap<&'a str, &'a str>,
}

/// For cases with no sendemail identity, use Default::default() then do this
impl<'a> CascadingDefaults<'a> for Option<SendemailIdentity<'a>> {
    type Finalised = SendemailIdentityFinalised<'a>;
    type Prerequisites = (PrimitiveIdentityFinalised<'a>, FormatFromFinalised<'a>);

    fn with_cascaded_prerequisites(
        &self,
        (basic_identity, format_from): Self::Prerequisites,
    ) -> Self::Finalised {
        let default_email = match format_from {
            FormatFromFinalised::RawCommitAuthors => basic_identity.email,
            FormatFromFinalised::CommitIdentity => basic_identity.email,
            FormatFromFinalised::NameAndEmail { email, .. } => email.or(basic_identity.email),
        };

        SendemailIdentityFinalised {
            identity_name: self.as_ref().and_then(|v| v.identity_name).unwrap_or(basic_identity.name.as_ref()) ,
            from_addr: self.as_ref().and_then(|v| v.from_addr).or(default_email)
                .inspecting_process(|v| if v.is_none() {
                    warn!(
                        target: "git-identity", 
                        "No from email available for `sendmail.<identity>.from`, either default or specific. Automatic values may induce identity leaks!") 
                }),
                // No defaults here - we use git's existing defaults ^.^ nya
            envelope_sender: self.as_ref().and_then(|v| v.envelope_sender),
            smtp_user: self.as_ref().and_then(|v| v.smtp_user).unwrap_or(basic_identity.name.as_ref()),
            extra_per_identity_keys: self.as_ref().map(|v| v.extra_per_identity_keys.clone())
                .unwrap_or_else(|| HashMap::new()),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, JsonSchema)]
#[serde(rename_all = "kebab-case", tag = "type")]
pub enum UserAuthorCommitNameAndEmail<'a> {
    Combined {
        #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
        // Default is main name nya
        name: Option<&'a str>,
        #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
        // Default is email ^.^
        email: Option<&'a str>,
    },
    Separate {
        #[serde(default, borrow)]
        committer_name: Option<&'a str>,
        #[serde(default, borrow)]
        committer_email: Option<&'a str>,

        #[serde(default, borrow)]
        author_name: Option<&'a str>,
        #[serde(default, borrow)]
        author_email: Option<&'a str>,
    },
}

impl<'a> UserAuthorCommitNameAndEmail<'a> {
    /// Return true essentially if this is combined with default name and value.
    pub fn is_default_value(&self) -> bool {
        match self {
            UserAuthorCommitNameAndEmail::Combined { name, email } => {
                name.is_none() && email.is_none()
            }
            UserAuthorCommitNameAndEmail::Separate { .. } => false,
        }
    }
}

impl<'a> Default for UserAuthorCommitNameAndEmail<'a> {
    fn default() -> Self {
        Self::Combined {
            name: None,
            email: None,
        }
    }
}

#[derive(Serialize, Debug, Clone)]
#[serde(rename_all = "kebab-case", tag = "type")]
pub enum UserAuthorCommitNameAndEmailFinalised<'a> {
    Combined {
        // Default is main name nya
        name: &'a str,
        // Default is email ^.^
        email: Option<&'a str>,
    },
    Separate {
        committer_name: &'a str,
        committer_email: Option<&'a str>,

        author_name: &'a str,
        author_email: Option<&'a str>,
    },
}

impl<'a> CascadingDefaults<'a> for UserAuthorCommitNameAndEmail<'a> {
    type Finalised = UserAuthorCommitNameAndEmailFinalised<'a>;
    type Prerequisites = PrimitiveIdentityFinalised<'a>;

    fn with_cascaded_prerequisites(&self, basic_identity: Self::Prerequisites) -> Self::Finalised {
        match self {
            UserAuthorCommitNameAndEmail::Combined { name, email } => UserAuthorCommitNameAndEmailFinalised::Combined {
                name: name.unwrap_or(basic_identity.name.as_ref()),
                email: email.or(basic_identity.email)
                    .inspecting_process(|v| if v.is_none() {
                        warn!(target: "git-identity", "user.email not set either explicitly or with an identity-wide email for this identity! Git may attempt to use defaults.")
                    })
            },
            UserAuthorCommitNameAndEmail::Separate { committer_name, committer_email, author_name, author_email } => UserAuthorCommitNameAndEmailFinalised::Separate {
                committer_name: committer_name.unwrap_or(basic_identity.name.as_ref()),
                committer_email: committer_email.or(basic_identity.email)
                    .inspecting_process(|v| if v.is_none() {
                        warn!(target: "git-identity", "committer.email not set either explicitly or with an identity-wide email for this identity! Git may attempt to use defaults.")
                    }),
                author_name: author_name.unwrap_or(basic_identity.name.as_ref()),
                author_email: author_email.or(basic_identity.email)
                    .inspecting_process(|v| if v.is_none() {
                        warn!(target: "git-identity", "author.email not set either explicitly or via an identity-wide email parameter for this identity. Git may attempt to use defaults.")
                    }),
            }
        }
    }
}

/// Components for git identity stuff.
///
/// A lot of this was pulled out of `man git-config`
///
/// NOTE: we don't #[derive(Default)] for this because disable_git_deduce should be `true` by
/// default, rather than false.
#[derive(Deserialize, Serialize, Debug, Clone, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct GitIdentity<'a> {
    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    /// Default credentials - see `man git-config` `credential.*` section.
    default_credentials: Option<GitCredential<'a>>,

    #[serde(
        default = "std::collections::HashMap::new",
        borrow,
        skip_serializing_if = "HashMap::is_empty"
    )]
    // Schemars does not agree with non-standard (zero argument) default nya
    #[schemars(default)]
    /// Per-URL credentials - keys are url matchers, vals are associated credentials.
    url_matching_credentials: HashMap<&'a str, GitCredential<'a>>,

    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    /// When submitting patches, this controls the `From:` field in the email. By default, it is
    /// set to [Full Name] <[email]>, if email is present, and `false` ([`FormatFrom::RawCommitAuthors`]), if it is not ^.^
    format_from: Option<FormatFrom<'a>>,

    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    /// gitweb owner value. Default is the full_name value
    gitweb_owner: Option<&'a str>,

    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    /// Signing key for use with git ^.^
    /// controls `gpg.format` (for openpgp vs ssh vs x509), `user.signingKey` (for the actual key
    /// value), and commit.gpgSign (whether or not to sign commits), and `tag.gpgSign` (for tags
    /// ^.^).
    signing: Option<GitSigningKey<'a>>,

    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    /// Git imap stuff. If defined at all, it needs to involve a password.
    imap: Option<GitIMAPLogin<'a>>,

    #[serde(default, borrow, skip_serializing_if = "Option::is_none")]
    sendemail: Option<SendemailIdentity<'a>>,

    #[serde(
        default,
        borrow,
        skip_serializing_if = "UserAuthorCommitNameAndEmail::is_default_value"
    )]
    id: UserAuthorCommitNameAndEmail<'a>,

    /// Attempt to stop git from deducing name and email with defaults. Default value is true.
    #[serde(
        default = "crate::util::true_val",
        skip_serializing_if = "crate::util::is_true"
    )]
    disable_git_deduce: bool,

    /// Extra git keys ^.^
    #[serde(default = "HashMap::new", skip_serializing_if = "HashMap::is_empty")]
    // Schemars does not agree with non-standard (zero argument) default nya
    #[schemars(default)]
    pub extra: HashMap<&'a str, &'a str>,
}

impl<'a> Default for GitIdentity<'a> {
    fn default() -> Self {
        Self {
            default_credentials: None,
            url_matching_credentials: Default::default(),
            format_from: None,
            gitweb_owner: None,
            signing: None,
            imap: None,
            sendemail: None,
            id: Default::default(),
            disable_git_deduce: true,
            extra: Default::default(),
        }
    }
}

#[derive(Serialize, Debug, Clone)]
#[serde(rename_all = "kebab-case")]
pub struct GitIdentityFinalised<'a> {
    /// Attempt to stop git from deducing name and email with defaults. Default value is true.
    disable_git_deduce: bool,

    /// gitweb owner value. Default is the full_name value
    gitweb_owner: &'a str,

    /// Default credentials - see `man git-config` `credential.*` section.
    #[serde(skip_serializing_if = "Option::is_none")]
    default_credentials: Option<GitCredential<'a>>,

    /// When submitting patches, this controls the `From:` field in the email. By default, it is
    /// set to [Full Name] <[email]>, if email is present, and `false` ([`FormatFrom::RawCommitAuthors`]), if it is not ^.^
    format_from: FormatFromFinalised<'a>,

    /// Signing key for use with git ^.^
    /// controls `gpg.format` (for openpgp vs ssh vs x509), `user.signingKey` (for the actual key
    /// value), and commit.gpgSign (whether or not to sign commits), and `tag.gpgSign` (for tags
    /// ^.^).
    signing: GitSigningKeyFinalised<'a>,

    /// Git imap stuff. If defined at all, it needs to involve a password.
    imap: GitIMAPLoginFinalised<'a>,

    sendemail: SendemailIdentityFinalised<'a>,

    id: UserAuthorCommitNameAndEmailFinalised<'a>,

    /// Per-URL credentials - keys are url matchers, vals are associated credentials.
    /// see `man git-config` `credential.*` section`
    url_matching_credentials: HashMap<&'a str, GitCredential<'a>>,

    /// Extra git keys ^.^
    pub extra: HashMap<&'a str, &'a str>,
}

impl<'a> CascadingDefaults<'a> for GitIdentity<'a> {
    type Finalised = GitIdentityFinalised<'a>;
    type Prerequisites = (PrimitiveIdentityFinalised<'a>, CryptoIdentities<'a>);

    fn with_cascaded_prerequisites(
        &self,
        (basic_identity, cryptographic_identities): Self::Prerequisites,
    ) -> Self::Finalised {
        let from_format_identity = self
            .format_from
            .with_cascaded_prerequisites(basic_identity.clone());

        GitIdentityFinalised {
            default_credentials: self.default_credentials,
            url_matching_credentials: self.url_matching_credentials.clone(),
            format_from: from_format_identity,
            gitweb_owner: self.gitweb_owner.unwrap_or(basic_identity.full_name),
            signing: self
                .signing
                .with_cascaded_prerequisites(cryptographic_identities),
            imap: self
                .imap
                .with_cascaded_prerequisites(basic_identity.clone()),
            sendemail: self
                .sendemail
                .with_cascaded_prerequisites((basic_identity.clone(), from_format_identity)),
            id: self.id.with_cascaded_prerequisites(basic_identity.clone()),
            disable_git_deduce: self.disable_git_deduce,
            extra: self.extra.clone(),
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
