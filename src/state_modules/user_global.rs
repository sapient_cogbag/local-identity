//! Useful data structures for user-global modules.

use std::{
    borrow::Cow,
    collections::HashMap,
    fmt::Display,
    path::{Path, PathBuf},
};

use log::*;

use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::{
    dst_newtype,
    util::{
        is_default,
        xdgstuff::{XdgDirsWithHome, XdgErrorFlat},
    },
    with_deserialization_validation,
};

use super::{BasicIdentityModule, ModuleName, UserGlobalIdentityModule};

// {{{ Relative Paths

/// Encodes what a path is relative *to*.
///
/// See https://wiki.archlinux.org/title/XDG_Base_Directory for information on what this mostly is
/// all about.
#[derive(
    Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, JsonSchema, Debug,
)]
#[serde(rename_all = "kebab-case")]
pub enum RelativePathMode {
    /// Path is relative to home directory `$HOME`. This is the default.
    Home,
    /// Path is relative to `$XDG_CONFIG_HOME` (default `$HOME/.config`)
    XdgConfigHome,
    /// Path is relative to `$XDG_CACHE_HOME` (default `$HOME/.cache`)
    XdgCacheHome,
    /// Path is relative to `$XDG_DATA_HOME` (default `$HOME/.local/share`)
    XdgDataHome,
    /// Path is relative to `$XDG_STATE_HOME` (default `$HOME/.local/state`)
    XdgStateHome,
}

impl Default for RelativePathMode {
    fn default() -> Self { RelativePathMode::Home }
}

impl RelativePathMode {
    /// Get the base path that paths relative to this are appended to
    pub fn base_path(&self, ctx: &XdgDirsWithHome) -> Result<PathBuf, XdgErrorFlat> {
        match self {
            RelativePathMode::Home => ctx.home().map(ToOwned::to_owned).map_err(From::from),
            RelativePathMode::XdgConfigHome => ctx.xdg_config_home(),
            RelativePathMode::XdgCacheHome => ctx.xdg_cache_home(),
            RelativePathMode::XdgDataHome => ctx.xdg_data_home(),
            RelativePathMode::XdgStateHome => ctx.xdg_state_home(),
        }
    }
}

// }}}

// {{{ Behaviour for new global identities when switching and creating links.

/// Control what happens when a user-global module is activated with a new identity (or even an
/// existing one where one of the managed paths has nothing in it when converting to full
/// management mode).
#[derive(
    Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Serialize, Deserialize, JsonSchema,
)]
#[serde(rename_all = "kebab-case")]
pub enum NewIdentityLinkBehaviour {
    /// Create a bare symbolic link to the identity- and module- local path, even if it does not
    /// exist. This is useful if your program will fill out defaults itself, as broken/dangling links (at
    /// least on linux) appear to work very well.
    BareLink,
    /// Create an empty file or directory at the target location if it does not exist.
    CreateEmpty,
}

impl Default for NewIdentityLinkBehaviour {
    fn default() -> Self { Self::BareLink }
}

// }}}

// {{{ Types of files

/// The type of the item at the location of the path. In most cases, you shouldn't care about this,
/// unless you want to create an empty
#[derive(
    Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Serialize, Deserialize, JsonSchema,
)]
#[serde(rename_all = "kebab-case")]
pub enum ManagedPathType {
    File,
    Directory,
    /// The type is unknown and irrelevant. This is the default and in most cases it is what you
    /// want unless you use the [`NewIdentityLinkBehaviour::CreateEmpty`] option.
    Irrelevant,
}

impl Default for ManagedPathType {
    fn default() -> Self { Self::Irrelevant }
}

// }}}

// {{{ Full path behaviour specification

/// Holds a path that is relevant - and can be managed - by a user-global module.
#[derive(
    Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Serialize, Deserialize, JsonSchema,
)]
#[serde(rename_all = "kebab-case", remote = "Self")]
pub struct UserGlobalModuleManagedPath<'path> {
    // TODO: Make RelativePath newtype with dst_newtype!
    /// A path, relative to [`Self::relative_to`]
    ///
    /// This has to be Cow, for [this reason](https://github.com/serde-rs/serde/issues/1413)
    #[serde(borrow)]
    path: Cow<'path, Path>,
    /// What the path is relative to in the first place.
    #[serde(skip_serializing_if = "is_default", default)]
    relative_to: RelativePathMode,
    /// The type of the object this path points to
    #[serde(skip_serializing_if = "is_default", default)]
    target_type: ManagedPathType,
    /// The behaviour to use when the described path is linked-to somewhere new (for instance, a
    /// new identity-local folder when switching out identities, which is currently empty) or that
    /// otherwise has nothing at the final location the symbolic link would point to.
    #[serde(skip_serializing_if = "is_default", default)]
    on_new_link: NewIdentityLinkBehaviour,
}

with_deserialization_validation! { UserGlobalModuleManagedPath:<'path>, |this|<D> {this.valid().map(D::Error::custom)} }

#[derive(Clone, Debug, thiserror::Error, PartialEq, Eq)]
/// Error when creating a user-global-module managed path
pub enum UserGlobalModuleManagedPathErr {
    #[error("{0} is not a relative path")]
    PathNotRelative(PathBuf),
    #[error(
        "cannot use empty file/dir creation without knowing whether the target location is a file \
         or directory, path is {0}"
    )]
    CreatingEmptyRequiresSpecification(PathBuf),
}

impl<'path> UserGlobalModuleManagedPath<'path> {
    /// Create a path relative to home
    pub fn home_relative(path: &'path Path) -> Result<Self, UserGlobalModuleManagedPathErr> {
        Self::relative(path, RelativePathMode::Home)
    }

    /// Create a path relative to the given location, with default behaviour otherwise
    pub fn relative(
        path: &'path Path,
        relative_to: RelativePathMode,
    ) -> Result<Self, UserGlobalModuleManagedPathErr> {
        Self::relative_with_behaviour(path, relative_to, Default::default())
    }

    /// Create a path with non-default behaviours for the given location
    pub fn relative_with_behaviour(
        path: &'path Path,
        relative_to: RelativePathMode,
        (target_type, on_new_link): (ManagedPathType, NewIdentityLinkBehaviour),
    ) -> Result<Self, UserGlobalModuleManagedPathErr> {
        let res = Self {
            path: path.into(),
            relative_to,
            target_type,
            on_new_link,
        };
        res.valid().map(Err).unwrap_or(Ok(res))
    }

    /// Function that finds any internal consistency error. Used by both the constructors and the
    /// serde validator.
    ///
    /// This should be unnecessary in client code, as self-inconsistency cannot be created with
    /// public functions.
    fn valid(&self) -> Option<UserGlobalModuleManagedPathErr> {
        if let (ManagedPathType::Irrelevant, NewIdentityLinkBehaviour::CreateEmpty) =
            (self.target_type, self.on_new_link)
        {
            return Some(
                UserGlobalModuleManagedPathErr::CreatingEmptyRequiresSpecification(
                    self.path.as_ref().to_owned(),
                ),
            );
        }

        if !self.path.is_relative() {
            Some(UserGlobalModuleManagedPathErr::PathNotRelative(
                self.path.as_ref().to_owned(),
            ))
        } else {
            None
        }
    }

    /// Extract the full path with the prefix expanded for this path specification, when provided
    /// with an XDG context
    pub fn full_path(
        &self,
        base_dirs: &crate::util::xdgstuff::XdgDirsWithHome,
    ) -> Result<PathBuf, XdgErrorFlat> {
        self.relative_to.base_path(base_dirs).map(|mut p| {
            p.push(self.path.as_ref());
            p
        })
    }
}

// }}}

// {{{ Global module name newtype

dst_newtype! {
    /// An owned name for a managed path for a user-global identity module
    pub ManagedPathNameBuf(pub) from String;
    /// An unowned name for a managed path for a user-global identity module
    pub ManagedPathName(pub) from str;
    reffn pub from_raw_str;
    mutfn pub from_raw_str_mut;
    owned_only_derives Clone, Default;
    serde serialize deserialize;
    json_schema yes;
}

impl std::fmt::Display for ManagedPathName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("[managed-path name:{}]", &self.0))
    }
}

// }}}

/// Represents a single module-nameofpath-path 3-combo (used to document conflicts amongst other
/// things)
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct ModulePathTriplet<'a, 'b, 'c> {
    /// The name of the module with which the name is associated
    #[serde(borrow)]
    pub module_name: &'a ModuleName,
    /// The name for the path referenced, module-locally.
    #[serde(borrow)]
    pub module_path_name: &'b ManagedPathName,
    /// The actual, resolved path - this is used to detect conflicts as best as is possible and
    /// is the place where linking will occur to.
    fully_resolved_path: PathBuf,
    /// The initial path that was passed to the constructor, uncanonicalised.
    #[serde(borrow)]
    pub raw_path: Cow<'c, Path>,
}

impl<'a, 'b, 'c> ModulePathTriplet<'a, 'b, 'c> {
    /// Create a module path triplet.
    ///
    /// This will normalise the path provided as well, for better equality comparisons ^.^.
    ///
    /// Note however that, due to use of [`std::path::Path::canonicalize`], it can return an IO
    /// error.
    pub fn new(
        module_name: &'a ModuleName,
        module_path_name: &'b ManagedPathName,
        raw_path: Cow<'c, Path>,
    ) -> std::io::Result<Self> {
        Ok(Self {
            module_name,
            module_path_name,
            // Note on symbolic links!
            //
            // canonicalize() resolves symlinks. Since there is a good chance the file we point to
            // will be a symlink, it will probably get resolved to wherever it points to nya.
            //
            // However that is FINE because the symlink can't point to different places unless
            // someone fucks with the filesystem under our noses, which is beyond our model anyhow.
            //
            // The only thing to test is if the existence of the target path of the symlink is
            // required for canonicalization. However, I think this is unlikely (unless obviously
            // you are trying to access subdirectories of the symlink), because you can construct
            // the canonicalized version of the path fine without validating that existence nya.
            fully_resolved_path: raw_path.as_ref().canonicalize()?,
            raw_path,
        })
    }

    /// Get the path that has been completely canonicalized ^.^
    pub fn fully_resolved_path(&self) -> &Path { self.raw_path.as_ref() }
}

impl<'a, 'b, 'c> std::fmt::Display for ModulePathTriplet<'a, 'b, 'c> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}/{} -> {} (from: {})",
            self.module_name,
            self.module_path_name,
            self.fully_resolved_path.display(),
            self.raw_path.display()
        )
    }
}

/// Represents a single, simple conflict between managed path locations in either >=2 modules or
/// the same module.
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct Conflict<'a, 'b, 'c> {
    /// One of the minimum two entries known to conflict.
    #[serde(borrow)]
    pub primary: ModulePathTriplet<'a, 'b, 'c>,
    /// Second of the minimum two entries known to conflict
    #[serde(borrow)]
    pub secondary: ModulePathTriplet<'a, 'b, 'c>,
    /// More conflicting sections.
    #[serde(borrow)]
    pub extra: Vec<ModulePathTriplet<'a, 'b, 'c>>,
}

impl<'a, 'b, 'c> Display for Conflict<'a, 'b, 'c> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use std::iter::once;
        writeln!(f, "User-Global Module Path Conflict [")?;
        for conflict_entry in once(&self.primary)
            .chain(once(&self.secondary))
            .chain(self.extra.iter())
        {
            writeln!(f, "  {}", conflict_entry)?;
        }
        writeln!(f, "]")
    }
}

/// Find all the conflicts in a series of modules, when provided with an [`XdgDirsWithHome`] to
/// resolve paths ^.^
///
/// Because names are DRY, you must pair user-global identity modules with their basic module
/// information in the iterator.
pub fn find_conflict_in_modules<
    'module,
    UGIM: UserGlobalIdentityModule + ?Sized + 'module,
    BIM: BasicIdentityModule + ?Sized + 'module,
>(
    mods: impl Iterator<Item = (&'module UGIM, &'module BIM)>,
    xdg_context: &XdgDirsWithHome,
) -> std::io::Result<impl Iterator<Item = Conflict<'module, 'module, 'module>>> {
    debug!("Checking for user-global module path conflicts");
    use crate::util::io::to_other_io_error;
    // Map from canonical path to vector of module/name pairs nya
    let mut canonical_path_modules_and_names =
        HashMap::<PathBuf, Vec<ModulePathTriplet<'module, 'module, 'module>>>::new();
    // Some efficiency here.
    let (est_min, est_max) = mods.size_hint();
    canonical_path_modules_and_names.reserve(est_max.unwrap_or(est_min));

    for (user_global_module, basic_module_info) in mods {
        let module_name = basic_module_info.raw_name();
        for (directory_name, named_directory) in user_global_module.named_links().iter() {
            // Resolve $HOME/$XDG_*/etc. prefixes
            trace!(
                "Resolving module/name pair {}/{}",
                &module_name,
                &directory_name
            );
            let prefix_resolved_path = named_directory
                .full_path(xdg_context)
                .map_err(to_other_io_error)?;
            let mod_triple = ModulePathTriplet::new(
                module_name,
                directory_name,
                Cow::Owned(prefix_resolved_path),
            )?;
            let fully_resolved_path = mod_triple.fully_resolved_path();
            let pair_get = canonical_path_modules_and_names.get_mut(fully_resolved_path);
            match pair_get {
                Some(inner_vec) => {
                    trace!("Found conflict for {}/{}", &module_name, &directory_name);
                    inner_vec.push(mod_triple);
                }
                None => canonical_path_modules_and_names
                    .insert(fully_resolved_path.to_owned(), vec![mod_triple])
                    .map(|v| {
                        unreachable!(
                            "We checked that there was not originally a value. Found {:#?}",
                            v
                        )
                    })
                    .unwrap_or(()),
            };
        }
    }

    debug!("Generating filtered conflict iterator");
    Ok(canonical_path_modules_and_names
        .into_iter()
        .filter_map(|(_canon_path, triplets)| match triplets {
            v if v.len() < 2 => None,
            mut v => Some({
                let mut first_two_iter = v.drain(..2);
                let primary = first_two_iter.next().expect("length checked");
                let secondary = first_two_iter.next().expect("length checked");
                drop(first_two_iter);
                Conflict {
                    primary,
                    secondary,
                    extra: v,
                }
            }),
        }))
}

#[cfg(test)]
mod test_relpaths;

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
