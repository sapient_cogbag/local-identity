//! Module for shell-local module config.
//!
//! Note that we have separate structures for the environment, and the overall changes to the
//! environment.
//!
//! This is important as it allows emitting only the collection of environment variables that have
//! changed without writing to basic environment variables like $HOME. As an extra benefit, this
//! allows useful detection of conflicts between different modules and the environment variables
//! they set, because only the ones that have been changed are emitted rather than all environment
//! variables present.

pub mod dir_generation;
pub mod shell_variables;
pub mod var_storage;

use core::fmt;
use std::{collections::HashMap, fmt::Display};

use crate::{
    util::{
        err::{ResultTransparentExt, TransparentError, TransparentErrorExt},
        functions::HashMapExt,
    },
    with_deserialization_validation,
};

use enum_map::{enum_map, EnumMap};

#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case", remote = "Self")]
/// A collection of variable assignments and value substitutions to perform.
///
/// The keys are the names of the variables to assign to - the values are strings in
/// [shellexpand][shellexpand] substitution format, with accessible variables being those as
/// defined either in the constants section, in previous substitution sections, in the
/// environment (including modifications in previous substitution sections), or variables defined
/// by the identity. Tilde-to-home substitution also occurs.
///
/// This can modify environment variables and local variables.
///
/// [shellexpand]: https://docs.rs/shellexpand-fork/latest/shellexpand/index.html
pub struct SubstitutionSection<'a> {
    #[serde(borrow, flatten)]
    substitutions: HashMap<&'a ShellVarName, &'a ShellSubstitutionRule>,
}

impl<'a> ShellVarSection<'a> for SubstitutionSection<'a> {
    #[inline]
    fn context_type(&self) -> ShellVarSectionType { ShellVarSectionType::SubstitutionSection }

    fn apply_as_update_to_environment(
        &self,
        environment: &mut ShellModuleEnvironment<'a>,
    ) -> Result<ShellModuleEnvironmentDelta<'a>, InvalidEnvironmentUpdate> {
        environment.feed_updates(
            self,
            self.substitutions
                .iter()
                .map(|(rname, rval)| (*rname, Assignment::Substitution(*rval))),
        )
    }
}

with_deserialization_validation! {
    SubstitutionSection: <'a>, |this|<D> {
        shell_variables::ShellVarSectionExt::can_assign_all_of(&this, this.substitutions.keys().copied())
            .map(D::Error::custom)
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Default, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case", remote = "Self")]
/// A collection of constants to set.
///
/// The keys are the names of the constants to assign to. The values are simply plain strings.
pub struct ConstantSection<'a> {
    #[serde(borrow, flatten)]
    assignments: HashMap<&'a ShellVarName, &'a str>,
}

impl<'a> ShellVarSection<'a> for ConstantSection<'a> {
    fn context_type(&self) -> ShellVarSectionType { ShellVarSectionType::ConstantSection }

    fn apply_as_update_to_environment(
        &self,
        environment: &mut ShellModuleEnvironment<'a>,
    ) -> Result<ShellModuleEnvironmentDelta<'a>, InvalidEnvironmentUpdate> {
        environment.feed_updates(
            self,
            self.assignments
                .iter()
                .map(|(rname, rval)| (*rname, Assignment::Plain(*rval))),
        )
    }
}

with_deserialization_validation! {
    ConstantSection: <'a>, |this|<D> {
        shell_variables::ShellVarSectionExt::can_assign_all_of(&this, this.assignments.keys().copied())
            .map(D::Error::custom)
    }
}

/// Represents the change of a single shell variable from one thing to another.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct ShellVariableDelta {
    #[serde(default, skip_serializing_if = "crate::util::is_default")]
    /// The value before the change, if it existed
    pub old: Option<String>,
    /// The value after the change ^.^
    pub new: String,
}

impl ShellVariableDelta {
    /// Attempt to apply the passed delta to this current delta, in place, returning an error in
    /// case of failure.
    ///
    /// In case of success, returns reference to self
    pub fn merge_in_place_with(
        &mut self,
        to_apply: ShellVariableDelta,
    ) -> Result<&mut Self, TransparentError<&mut Self, ConflictingVarDeltaMerge>> {
        match to_apply.old {
            Some(to_apply_start_point) if to_apply_start_point == self.new => {
                // update our end point nyaa.
                self.new = to_apply.new;
                Ok(self)
            }
            Some(to_apply_start_point) => {
                Err(ConflictingVarDeltaMerge::InconsistentEndAndStartValues {
                    // Reconstruct from the moved values nya
                    new_delta: ShellVariableDelta {
                        old: Some(to_apply_start_point.clone()),
                        new: to_apply.new,
                    },
                    older_delta_result_value: self.new.clone(),
                    newer_delta_start_value: to_apply_start_point,
                }
                .with_transparent_data(self))
            }
            None => Err(ConflictingVarDeltaMerge::CreationOfExistingValue {
                new_delta_endpoint: to_apply.new,
            }
            .with_transparent_data(self)),
        }
    }
}

impl Display for ShellVariableDelta {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(ov) = self.old.as_ref() {
            write!(f, "\"{}\" => \"{}\"", ov, &self.new)
        } else {
            write!(f, "[undefined] => \"{}\"", &self.new)
        }
    }
}

#[derive(
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    Deserialize,
    Serialize,
    JsonSchema,
    thiserror::Error,
)]
#[serde(rename_all = "kebab-case")]
pub enum ConflictingVarDeltaMerge {
    #[error(
        "end value \"{older_delta_result_value}\" of first delta does not match the starting \
         value \"{newer_delta_start_value}\" of the second delta"
    )]
    InconsistentEndAndStartValues {
        new_delta: ShellVariableDelta,
        older_delta_result_value: String,
        newer_delta_start_value: String,
    },
    #[error(
        "attempt to create a variable that already exists with new value \"{new_delta_endpoint}\""
    )]
    CreationOfExistingValue { new_delta_endpoint: String },
}

impl ConflictingVarDeltaMerge {
    /// Convert this to an error with an associated variable >.<
    pub fn for_variable_named(self, var_name: ShellVarNameBuf) -> NamedConflictingVarDeltaMerge {
        NamedConflictingVarDeltaMerge {
            var_name,
            source: self,
        }
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize, JsonSchema, thiserror::Error)]
#[error("Error merging deltas for variable named: {var_name}")]
pub struct NamedConflictingVarDeltaMerge {
    pub var_name: ShellVarNameBuf,
    pub source: ConflictingVarDeltaMerge,
}

/// Represents the change induced by a single [`SubstitutionSection`] when applied to an
/// environment ^.^
#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize, JsonSchema, Default)]
#[serde(rename_all = "kebab-case")]
pub struct ShellModuleEnvironmentDelta<'a> {
    #[serde(borrow, flatten)]
    assignments: HashMap<&'a ShellVarName, ShellVariableDelta>,
}

impl<'a> FromIterator<(&'a ShellVarName, ShellVariableDelta)> for ShellModuleEnvironmentDelta<'a> {
    #[inline]
    fn from_iter<T: IntoIterator<Item = (&'a ShellVarName, ShellVariableDelta)>>(iter: T) -> Self {
        Self {
            assignments: HashMap::from_iter(iter),
        }
    }
}

impl<'a> Display for ShellModuleEnvironmentDelta<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (variable_name, delta) in self.assignments.iter() {
            writeln!(f, "\"{}\": {}", variable_name, delta)?
        }
        Ok(())
    }
}

/// The change induced by a collection of [`SubstitutionSection`]s applied in series and any
/// connected [`ShellVariableDelta`] for the same variable merged together
#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, JsonSchema, Default)]
#[serde(rename_all = "kebab-case")]
pub struct ShellModuleEnvironmentChangeset<'a> {
    #[serde(flatten, borrow)]
    full_changes: HashMap<&'a ShellVarName, ShellVariableDelta>,
}

impl<'a> ShellModuleEnvironmentChangeset<'a> {
    /// Merges a [`ShellModuleEnvironmentDelta`] into the overall changeset, returning a reference to self again (so it can
    /// be chained ^.^), or an error.
    pub fn merge(
        &mut self,
        delta: ShellModuleEnvironmentDelta<'a>,
    ) -> Result<&mut Self, NamedConflictingVarDeltaMerge> {
        debug!("Merging section delta into the overall changeset.");
        for (variable_name, variable_delta_to_apply) in delta.assignments.into_iter() {
            self.full_changes.try_insert_or_modify(
                &variable_name,
                variable_delta_to_apply,
                |variable_name_with_conflict, original_delta, delta_to_apply| {
                    trace!(
                        "Merging delta pair for \"{}\": ({}) then ({})",
                        variable_name_with_conflict,
                        &original_delta,
                        &delta_to_apply
                    );
                    original_delta
                        .merge_in_place_with(delta_to_apply)
                        .discard_err_data()
                        .map_err(move |e| {
                            e.for_variable_named((*variable_name_with_conflict).to_owned())
                        })?;
                    Ok(())
                },
            )?;
        }
        Ok(self)
    }

    /// Merges all the [`ShellModuleEnvironmentDelta`] from the iterator, erroring out if there are
    /// any delta conflicts.
    pub fn merge_all_of(
        &mut self,
        deltas: impl Iterator<Item = ShellModuleEnvironmentDelta<'a>>,
    ) -> Result<&mut Self, NamedConflictingVarDeltaMerge> {
        for delta in deltas {
            self.merge(delta)?;
        }
        Ok(self)
    }
}

/// The environment that gets modified as it passes through a series of substitution sections
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct ShellModuleEnvironment<'a> {
    /// The collection of items in each variable type category
    #[serde(borrow)]
    variables: EnumMap<ShellVarType, HashMap<&'a ShellVarName, String>>,
}

/// Error for environment updates
#[derive(Clone, Debug, thiserror::Error)]
pub enum InvalidEnvironmentUpdate {
    #[error("cannot assign to variable in current section")]
    UnassignableInContext(
        #[from]
        #[source]
        shell_variables::InvalidShellVarSection,
    ),
    #[error("could not look up variable")]
    CouldNotLookupVariable(
        #[from]
        #[source]
        shellexpand::LookupError<shell_variables::InvalidShellVariableLookup>,
    ),
}

/// Plain assignment and full substitution, as an enum
pub enum Assignment<'a> {
    Plain(&'a str),
    Substitution(&'a ShellSubstitutionRule),
}

impl<'a> ShellModuleEnvironment<'a> {
    pub fn get_variable(&self, varname: &ShellVarName) -> Option<&str> {
        self.variables[varname.var_type()]
            .get(varname)
            .map(String::as_str)
    }

    /// Update the environment in a section, with the given substitution to perform (using
    /// [`ShellSubstiutionRule::try_substitute`]
    pub fn feed_update(
        &mut self,
        section: &dyn shell_variables::ShellVarSectionExtDyn,
        variable_name: &'a ShellVarName,
        assignment_to_perform: Assignment<'a>,
    ) -> Result<ShellVariableDelta, InvalidEnvironmentUpdate> {
        // This should be checked on shell variable section creation, but we also do it here ^.^
        // nya
        section
            .dyn_can_assign_all_of(&mut std::iter::once(variable_name))
            .map_or(Ok(()), Result::Err)?;
        let new_value = match assignment_to_perform {
            Assignment::Plain(v) => v.to_owned(),
            Assignment::Substitution(sub) => sub.try_substitute(self)?,
        };
        let old: Option<String> =
            self.variables[variable_name.var_type()].insert(variable_name, new_value.clone());
        Ok(ShellVariableDelta {
            old,
            new: new_value,
        })
    }

    /// Feed an update to a collection of variables with substitution rules, rather than just one
    /// as in [`Self::feed_update`]. Returns a full [`ShellModuleEnvironmentDelta`], if successful.
    pub fn feed_updates(
        &mut self,
        section: &dyn shell_variables::ShellVarSectionExtDyn,
        vars: impl Iterator<Item = (&'a ShellVarName, Assignment<'a>)>,
    ) -> Result<ShellModuleEnvironmentDelta<'a>, InvalidEnvironmentUpdate> {
        vars.map(|(vname, vsubstitution)| -> Result<(&'a ShellVarName, ShellVariableDelta), InvalidEnvironmentUpdate>{
            let delta = self.feed_update(section, vname, vsubstitution)?;
            Ok((vname, delta))
        }).collect::<Result<ShellModuleEnvironmentDelta<'a>, _>>()
    }

    /// Used to construct the basic environment from a collection of variables - you need
    /// [`var_storage::IdentityVariables`], [`var_storage::EnvironmentVariables`], and [`var_storage::UtilityVariables`] constructs stored somewhere to build this properly ^.^
    pub fn create_initial_environment(
        identity_vars: &'a var_storage::IdentityVariables,
        env_vars: &'a var_storage::EnvironmentVariables,
        utility_vars: &'a var_storage::UtilityVariables,
    ) -> Self {
        Self {
            variables: enum_map! {
                ShellVarType::Constant => HashMap::new(),
                ShellVarType::Local => HashMap::new(),
                ShellVarType::Environment => env_vars.with_reference_keys(),
                ShellVarType::Identity => identity_vars.with_reference_keys(),
                ShellVarType::Utility => utility_vars.with_reference_keys()
            },
        }
    }

    /// Get an iterator over the environment variables currently present in the shell module
    /// environment. Note that this strips the prefixes off the environment variables and provides
    /// them as plain &str
    pub fn environment_variable_iterator(&self) -> impl Iterator<Item = (&str, &str)> {
        let envvars = &self.variables[ShellVarType::Environment];
        envvars
            .iter()
            .map(|(shname, val)| (shname.prefix_free_name(), val.as_ref()))
    }
}

/// Extension trait for std::process::command that lets you match the environment variables of a
/// command to be executed with the environment variables present in a [ShellModuleEnvironment].
pub trait ShellEnvCommandExt {
    /// Update the command with any environment variables set in the provided
    /// ShellModuleEnvironment.
    fn make_environment_match<'a>(
        &mut self,
        shellenvironment: &ShellModuleEnvironment<'a>,
    ) -> &mut Self;
}

impl ShellEnvCommandExt for std::process::Command {
    fn make_environment_match<'a>(
        &mut self,
        shellenvironment: &ShellModuleEnvironment<'a>,
    ) -> &mut Self {
        self.envs(shellenvironment.environment_variable_iterator());
        self
    }
}

/// Shell modules essentially allow configuring environment variables (shell-local) and template directories on a
/// per-identity basis.
///
/// This defines a generalisable shell module that allows for:
/// * The use of an initial set of environment variables, identity variables, and constants
/// * The repeated substition of environment and local variables using [shellexpand][shellexpand]
///   syntax, with access to identity variables, environment variables, constants, and local
///   variables
///
/// [shellexpand]: https://docs.rs/shellexpand-fork/latest/shellexpand/index.html
#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize, JsonSchema)]
#[serde(rename_all = "kebab-case")]
pub struct ShellModule<'a> {
    #[serde(borrow, default)]
    /// C_ prefixed variables that cannot be changed later.
    pub constants: ConstantSection<'a>,

    #[serde(borrow, default)]
    /// Substitutions to perform, in order of application.
    pub substitutions: Vec<SubstitutionSection<'a>>,

    #[serde(default = "crate::util::false_val")]
    /// Whether or not this shell module wants a temporary directory to put files in, on a
    /// per-identity basis. Note that while it is a temporary directory in the sense that it is
    /// ephemeral between daemon sessions of local-identity, it is persistent over an entire
    /// session of an identity being loaded from disk and hence can comfortably be used in
    /// environment variables.
    ///
    /// This is only required if you need to create the temp directory even when no ephemeral
    /// directory instructions are specified.
    ///
    /// The temporary directory is put in a standard OS-provided tmpdir, that is probably in RAM
    /// (likely to be in somewhere like `/tmp`). It's location is put in the Utility Variable
    /// `U_PER_IDENTITY_PERSISTENT_TMPDIR`
    ///
    /// Note that the module data directory is stored in the variable `U_MODULE_DATA_DIRECTORY` as
    /// well.
    pub force_temporary_directory: bool,

    #[serde(default, borrow)]
    /// List of instructions for creating directories inside the ephemeral directory.
    pub directory_behaviour: Vec<EphemeralDirectoryInstruction<'a>>,
}

pub const TMPDIR_UTILITY_VARIABLE: &str = "PER_IDENTITY_PERSISTENT_TMPDIR";
pub const MODULE_DATA_DIRECTORY_VARIABLE: &str = "MODULE_DATA_DIRECTORY";

/// Error for when the generation of a full environment changeset failed.
#[derive(Clone, Debug, thiserror::Error)]
pub enum ChangesetGenerationFailed {
    #[error(
        "generated changes from one section to the next were in conflict - should only be able to \
         occur if the sections were applied to different environments and the generated deltas \
         then applied to each other"
    )]
    DeltaConflict(
        #[from]
        #[source]
        NamedConflictingVarDeltaMerge,
    ),
    #[error("update to environment was invalid")]
    InvalidEnvironmentUpdate(
        #[from]
        #[source]
        InvalidEnvironmentUpdate,
    ),
}

impl<'a> ShellModule<'a> {
    /// Get all the changes this module should apply to the provided initial environment.
    ///
    /// Returns the updated environment and the set of changes.
    pub fn get_changes_to_environment(
        &self,
        mut initial_environment: ShellModuleEnvironment<'a>,
    ) -> Result<
        (
            ShellModuleEnvironment<'a>,
            ShellModuleEnvironmentChangeset<'a>,
        ),
        ChangesetGenerationFailed,
    > {
        let mut cumulative_changeset = ShellModuleEnvironmentChangeset::default();
        cumulative_changeset.merge(
            self.constants
                .apply_as_update_to_environment(&mut initial_environment)?,
        )?;
        for section in self.substitutions.iter() {
            cumulative_changeset
                .merge(section.apply_as_update_to_environment(&mut initial_environment)?)?;
        }
        Ok((initial_environment, cumulative_changeset))
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use log::{debug, trace};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use self::{
    dir_generation::EphemeralDirectoryInstruction,
    shell_variables::{
        ShellSubstitutionRule, ShellVarName, ShellVarNameBuf, ShellVarSection, ShellVarSectionType,
        ShellVarType,
    },
};
