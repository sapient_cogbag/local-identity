//! Tests.

use std::path::PathBuf;

use serde::{de::Error, Deserialize, Serialize};

use crate::{state_modules::user_global::RelativePathMode, util::path::StrPathExt};

use super::{UserGlobalModuleManagedPath, UserGlobalModuleManagedPathErr};

#[test]
fn serde_flat_err_validation() {
    let json_in_broken = serde_json::json! {{
        "path": "/absolute/path"
    }};

    let parsed_res: Result<UserGlobalModuleManagedPath, _> =
        crate::util::serde::deserialize(json_in_broken);

    let err_left = parsed_res.map_err(|v| v.to_string());
    let err_right_raw = Err(serde_json::Error::custom(
        UserGlobalModuleManagedPathErr::PathNotRelative(
            "/absolute/path".directly_as_path().to_owned(),
        ),
    ));
    let err_right = err_right_raw.map_err(|v| v.to_string());
    assert_eq!(err_left, err_right);

    let json_in_simple = serde_json::json!({
        "path": "relative/path/to/file.rs"
    });

    let parsed_res: Result<UserGlobalModuleManagedPath, _> =
        crate::util::serde::deserialize(json_in_simple);
    assert_eq!(
        parsed_res.map_err(|v| v.to_string()),
        Ok(UserGlobalModuleManagedPath::home_relative(
            "relative/path/to/file.rs".directly_as_path()
        )
        .unwrap())
    );

    let json_in_complex = serde_json::json!({
        "path": "lovely/relative",
        "relative-to": "xdg-config-home"
    });

    let parsed_res: Result<UserGlobalModuleManagedPath, _> =
        crate::util::serde::deserialize(json_in_complex);
    assert_eq!(
        parsed_res.map_err(|v| v.to_string()),
        Ok(UserGlobalModuleManagedPath::relative(
            "lovely/relative".directly_as_path(),
            RelativePathMode::XdgConfigHome
        )
        .unwrap())
    );
}

#[test]
/// Ensure we don't have extra problems with our [`remote = Self`] method described in
/// [`crate::with_deserialization_validation`] - in particular, ensuring validation errors
/// still occur
fn serde_embedded_capability() {
    #[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
    #[serde(rename_all = "kebab-case")]
    struct InnerS<'a> {
        #[serde(borrow)]
        directory: UserGlobalModuleManagedPath<'a>,
    }

    let json_in_error = serde_json::json! {{
        "directory": {
            "path": "/some/absolute/path"
        }
    }};
    let res: Result<InnerS, _> = crate::util::serde::deserialize(json_in_error);
    assert!(res.is_err());
    assert_eq!(
        res.map_err(|v| v.to_string()),
        Err(serde_json::Error::custom(
            UserGlobalModuleManagedPathErr::PathNotRelative(PathBuf::from("/some/absolute/path"))
        ))
        .map_err(|v| v.to_string())
    )
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
