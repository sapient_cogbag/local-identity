//! Utility types for shell variables, their types, values, substitutions, names, etc.

/// An indicator of what type a variable is.
#[derive(
    Enum,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Debug,
    Serialize,
    Deserialize,
    JsonSchema,
)]
pub enum ShellVarType {
    Constant,
    Local,
    Environment,
    Identity,
    Utility,
}

impl std::fmt::Display for ShellVarType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let w = match self {
            ShellVarType::Constant => "constant",
            ShellVarType::Local => "local variable",
            ShellVarType::Environment => "environment variable",
            ShellVarType::Identity => "identity variable",
            ShellVarType::Utility => "utility variable",
        };
        f.write_str(w)
    }
}

impl ShellVarType {
    /// Attempt to convert the pair of the first and second characters into a shell variable type.
    ///
    /// Returns [`None`] if they are invalid.
    ///
    /// This function is the authority on valid prefixes of all kinds.
    pub const fn from_character_pair(chars: [char; 2]) -> Option<Self> {
        match chars {
            ['C', '_'] => Some(Self::Constant),
            ['L', '_'] => Some(Self::Local),
            ['E', '_'] => Some(Self::Environment),
            ['I', '_'] => Some(Self::Identity),
            ['U', '_'] => Some(Self::Utility),
            _ => None,
        }
    }

    /// Get the prefix as a static string for this type
    ///
    /// TODO: Once [const for](https://github.com/rust-lang/rust/issues/87575) is implemented, we
    /// should be able to do this with a raw const [`EnumMap`] to avoid having to do matching (the
    /// operation should be reduced to an index ^.^)
    pub const fn prefix_str(self) -> &'static str {
        match self {
            ShellVarType::Constant => "C_",
            ShellVarType::Local => "L_",
            ShellVarType::Environment => "E_",
            ShellVarType::Identity => "I_",
            ShellVarType::Utility => "U_",
        }
    }
}

/// Whether a given type of variable can be assigned to in the given context.
pub const fn assignable_to(context: ShellVarSectionType, var_type: ShellVarType) -> bool {
    match context {
        ShellVarSectionType::ConstantSection => match var_type {
            ShellVarType::Constant => true,
            ShellVarType::Local => false,
            ShellVarType::Environment => false,
            ShellVarType::Identity => false,
            ShellVarType::Utility => false,
        },
        ShellVarSectionType::SubstitutionSection => match var_type {
            ShellVarType::Constant => false,
            ShellVarType::Local => true,
            ShellVarType::Environment => true,
            ShellVarType::Identity => false,
            ShellVarType::Utility => false,
        },
    }
}

dst_newtype! {
    /// Owned shell variable name
    pub ShellVarNameBuf from String;
    /// DST shell variable name
    ///
    /// Prefix:
    ///  C_ for setting constants in the constant section - these are also local.
    ///  L_ for local variables (accessing or setting) - these cannot affect the external
    ///    environment unless they
    ///  E_ for environment variables (accessing or setting). Environment variables are
    ///    updated in an atomic manner for an identity - that is, the initial environment variables
    ///    provided to all shell identity modules is the same, and only at the end are they merged
    ///    together.
    ///  I_ for identity variables. There are a few of these.
    ///  U_ for utilitiy variables. These are not identity-specific but are more general utility
    ///    functions instead.
    pub ShellVarName from str;
    reffn
        /// Safety:
        /// * Must be > 2 characters long
        /// * Must start with a valid prefix.
        pub from_raw_str;
    mutfn
        /// Safety:
        /// * Must be > 2 characters long
        /// * Must start with a valid prefix.
        pub from_raw_str_mut;
    owned_only_derives Clone;
    serde serialize deserialize;
    json_schema yes;
    display enable;
    validator
        /// Check if a string is a valid shell variable name. This does not check if the variable
        /// exists, but it does check that it has a valid prefix and such.
        pub is_valid_shellvar_name = |this: &'i DST| -> Option<InvalidShellVarName> {
            let l = this.len();
            match l {
                _ if l <= 2 => Some(InvalidShellVarName::TooShort { full_name: this.to_owned() }),
                _ => {
                    let mut chrs_iter = this.chars();
                    let first = chrs_iter.next().expect("Length checked");
                    let second = chrs_iter.next().expect("Length checked");
                    match ShellVarType::from_character_pair([first, second]) {
                        Some(_) => None,
                        None => Some(InvalidShellVarName::InvalidPrefix {
                            full_name: this.to_owned(),
                            prefix: String::from_iter([first, second])
                        })
                    }
                }
            }
        };
}

impl ShellVarName {
    /// Get the prefix as a static string
    pub fn prefix(&self) -> &'static str {
        // TODO: remake as const once var_type can be const nya
        self.var_type().prefix_str()
    }

    pub fn var_type(&self) -> ShellVarType {
        // TODO: remake to be const when issues #5739 is resolved.
        let mut chriter = self.0.chars();
        let first_char = chriter.next().expect("Length checked by validator on init");
        let second_char = chriter.next().expect("Length checked by validator on init");
        ShellVarType::from_character_pair([first_char, second_char])
            .expect("prefix validity checked on construction")
    }

    /// Get the variable name with the prefix removed.
    pub fn prefix_free_name(&self) -> &str { &self.0[self.prefix().len()..] }
}

#[derive(
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Debug,
    thiserror::Error,
    Serialize,
    Deserialize,
    JsonSchema,
)]
pub enum InvalidShellVarName {
    #[error(
        "got an invalid prefix for a shell module variable name (expected one of C_, L_, I_, E_, \
         got {prefix} (from \"{full_name}\"))"
    )]
    InvalidPrefix { full_name: String, prefix: String },
    #[error(
        "the shell module variable name was too short - must have at least one character after \
         the prefix (value was {full_name})"
    )]
    TooShort { full_name: String },
}

impl ShellVarNameBuf {
    /// Generate a name for this variable with added prefix when provided with the type it should
    /// be
    pub fn from_name_and_type(var_type: ShellVarType, name: impl AsRef<str>) -> Self {
        let length = name.as_ref().len() + var_type.prefix_str().len();
        let mut inner_string = String::with_capacity(length);
        inner_string += var_type.prefix_str();
        inner_string += name.as_ref();
        Self::try_from(inner_string)
            .expect("We constructed the string from scratch using the prefix ^.^")
    }
}

// Newtype for a substitution nyaa
dst_newtype! {
    /// Owned [`shellexpand`] substitution.
    pub ShellSubstitutionRuleBuf from String;
    /// Shell substitution rule.
    ///
    /// Defines what to set a variable to in a substitution rule section, similar to how you would
    /// use `$<VARIABLE NAME>` in a bash script. This uses [shellexpand][shellexpand]
    /// syntax for substitutions.
    ///
    /// [shellexpand]: https://docs.rs/shellexpand-fork/latest/shellexpand/index.html
    pub ShellSubstitutionRule from str;
    reffn pub from_raw_str;
    mutfn pub from_raw_mut_str;
    owned_only_derives Clone;
    serde serialize deserialize;
    json_schema yes;
}

impl ShellSubstitutionRule {
    /// Attempt to perform appropriate substitution, using [`shellexpand`] syntax
    pub fn try_substitute(
        &self,
        environment: &ShellModuleEnvironment,
    ) -> Result<String, shellexpand::LookupError<InvalidShellVariableLookup>> {
        shellexpand::env_with_context(&self.0, |raw_name: &str| {
            Ok(environment.get_variable(
                <&ShellVarName>::try_from(raw_name)
                    .map_err(InvalidShellVariableLookup::InvalidVariableName)?,
            ))
        })
        .map(Cow::into_owned)
    }
}

#[derive(
    Enum,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Debug,
    Serialize,
    Deserialize,
    JsonSchema,
)]
/// The context in which a variable exists.
pub enum ShellVarSectionType {
    /// Context where constants are being set.
    ConstantSection,
    /// Context where substitutions are occurring
    SubstitutionSection,
}

impl std::fmt::Display for ShellVarSectionType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let w = match self {
            ShellVarSectionType::ConstantSection => "constant section",
            ShellVarSectionType::SubstitutionSection => "substitution section",
        };
        f.write_str(w)
    }
}

/// Validate all variables assigned in a section of the given type.
///
/// Returns [`Option::None`] if they are all ok, or
/// [`Option::<ShellVarSectionValidationError>::Some`] if there is an attempt to assign to an
/// invalid variable type for that section ^.^
///
/// Note that if the iterator is mutable, it's [`Iterator::next`] value will be the first value
/// after the error-causing one.
pub fn validate_assigned_variables<'names>(
    section_type: ShellVarSectionType,
    variables: impl Iterator<Item = &'names ShellVarName>,
) -> Option<InvalidShellVarSection> {
    for name in variables {
        if !assignable_to(section_type, name.var_type()) {
            return Some(InvalidShellVarSection::IllegalAssignment {
                section_type,
                variable_type: name.var_type(),
                variable_name: name.to_owned(),
            });
        }
    }
    None
}

/// Simple trait encoding the type of variable context a section is.
pub trait ShellVarSection<'a> {
    /// The context type of this object
    fn context_type(&self) -> ShellVarSectionType;

    /// Apply the collection of variable substitutions stored in here as an update to the provided
    /// environment, returning a delta.
    fn apply_as_update_to_environment(
        &self,
        environment: &mut ShellModuleEnvironment<'a>,
    ) -> Result<ShellModuleEnvironmentDelta<'a>, InvalidEnvironmentUpdate>;
}

/// Simple extension trait ^.^
pub trait ShellVarSectionExt<'names>: ShellVarSection<'names> {
    /// Get the type of the section
    fn section_type(&self) -> ShellVarSectionType;

    /// Check if all the variables in the provided collection can be assigned to in this section
    ///
    /// Returns [`Option<InvalidShellVarSection>::Some`] if there is an invalidity.
    fn can_assign_all_of(
        &self,
        variables: impl Iterator<Item = &'names ShellVarName>,
    ) -> Option<InvalidShellVarSection>;
}

impl<'names, T: ShellVarSection<'names>> ShellVarSectionExt<'names> for T {
    #[inline]
    fn section_type(&self) -> ShellVarSectionType { self.context_type() }

    #[inline]
    fn can_assign_all_of(
        &self,
        variables: impl Iterator<Item = &'names ShellVarName>,
    ) -> Option<InvalidShellVarSection> {
        validate_assigned_variables(self.section_type(), variables)
    }
}

/// Object-safe version of [`ShellVarSectionExt`]
pub trait ShellVarSectionExtDyn<'a>: ShellVarSection<'a> {
    /// Get the type of the section
    fn dyn_section_type(&self) -> ShellVarSectionType { self.context_type() }

    /// Check if all the variable names in the given dynamic iterator can be assigned to in this
    /// section, returning an error if not.
    ///
    /// The iterator should be
    fn dyn_can_assign_all_of<'names>(
        &self,
        variables: &mut dyn Iterator<Item = &'names ShellVarName>,
    ) -> Option<InvalidShellVarSection> {
        validate_assigned_variables(self.dyn_section_type(), variables)
    }
}

impl<'a, T: ShellVarSection<'a>> ShellVarSectionExtDyn<'a> for T {}

/// Error when validating variable sections (e.g. invalid assignments for a section type)
#[derive(
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Debug,
    Serialize,
    Deserialize,
    JsonSchema,
    thiserror::Error,
)]
pub enum InvalidShellVarSection {
    #[error("cannot assign to {variable_type} {variable_name} in {section_type}")]
    IllegalAssignment {
        section_type: ShellVarSectionType,
        variable_type: ShellVarType,
        variable_name: ShellVarNameBuf,
    },
}

#[derive(Clone, Debug, Deserialize, Serialize, JsonSchema, thiserror::Error)]
pub enum InvalidShellVariableLookup {
    #[error("attempted to look up a variable with an invalid name")]
    InvalidVariableName(
        #[from]
        #[source]
        InvalidShellVarName,
    ),
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::borrow::Cow;

use enum_map::Enum;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::dst_newtype;

use super::{InvalidEnvironmentUpdate, ShellModuleEnvironment, ShellModuleEnvironmentDelta};
