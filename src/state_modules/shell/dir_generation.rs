//! Module for generating shell module directories.

use std::{
    io::{LineWriter, Write},
    path::{Path, PathBuf},
    process::Stdio,
};

use log::{info, warn};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::{
    state_modules::{shell::ShellEnvCommandExt, ModuleNameBuf},
    util::{path::RelativePath, subcmdlog::CommandLogPoolExt},
};

use super::{
    shell_variables::{InvalidShellVariableLookup, ShellSubstitutionRule},
    ShellModuleEnvironment,
};

use anyhow::{bail, Result as AnyResult};

/// Information for the module that is per-identity. In particular, this encodes things like the
/// path to extra data for a module (beyond just the module definition itself), and data on the
/// target directory.
///
/// Note that hot-reloading needs to preserve this stuff across hot reloads.
#[derive(Clone, PartialEq, Eq, Serialize, JsonSchema)]
pub struct ShellModuleFileInfo {
    module_name: ModuleNameBuf,
    /// Path to a directory containing extra module data.
    ///
    /// This is at <modules directory>/<module name> (if the module is at <base
    /// directory>/<module_name>.toml). Only present if this directory existed at load time ^.^
    #[serde(skip_serializing_if = "crate::util::is_default")]
    extra_module_data: Option<PathBuf>,
    /// Path to the per-identity module directory to generate
    ///
    /// Only present if the module requested a temporary directory, or if a series of ephemeral
    /// directory creation instructions were issued.
    #[serde(skip_serializing_if = "crate::util::is_default")]
    per_identity_data_directory: Option<PathBuf>,
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Deserialize, Serialize, JsonSchema)]
/// Instruction to perform when creating ephemeral directories for use in identity separation.
///
/// Uses [inner tagged enum representation](https://serde.rs/enum-representations.html),
#[serde(rename_all = "kebab-case", tag = "action")]
pub enum EphemeralDirectoryInstruction<'a> {
    /// Run a script from the module data directory
    ///
    /// This requires the actual presence of a data directory and will *error* if it does not
    /// exist.
    RunScript {
        /// The path of the script relative to the module directory
        #[serde(borrow)]
        script_path: &'a RelativePath,
        /// The arguments to pass to the script, which can contain substitutions of variables
        /// defined by the rest of the module.
        #[serde(borrow, default)]
        command_arguments: Option<Vec<&'a ShellSubstitutionRule>>,
    },
    /// Run a command with various sets of arguments, substituted from the environment defined by
    /// this module.
    ///
    /// This is useful e.g. if you want to set a bunch of config parameters with the same base
    /// command but different names
    RunMultiCommand {
        /// The base command executable to run (e.g. `git`, `cargo`, etc.)
        command: &'a str,
        #[serde(borrow, default, skip_serializing_if = "crate::util::is_default")]
        /// The base set of arguments to pass to the executed command. If multi-arguments are
        /// specified, this is attached to the start of the arguments for each command.
        ///
        /// Can have variables substituted from the environment generated for the module.
        arguments: Option<Vec<&'a ShellSubstitutionRule>>,
        #[serde(borrow, default, skip_serializing_if = "crate::util::is_default")]
        /// List of sets of arguments to append after the initial collection.
        ///
        /// Note that if this is unspecified (as is default), the command and arguments will just
        /// be executed as-is.
        ///
        /// These arguments can contain substitutions from the generated module environment.
        multi_arguments: Option<Vec<Vec<&'a ShellSubstitutionRule>>>,
    },
    /// Create a file with the given lines as shell-substituted templates.
    ///
    /// Note that the directories needed will be created if not present.
    CreateFile {
        /// Path to the file relative to the temporary directory
        file_output_path: &'a RelativePath,
        #[serde(borrow)]
        /// Lines to put in the file, substituted from the environment derived from the rest of the
        /// module.
        lines: Vec<&'a ShellSubstitutionRule>,
    },
    /// Create a directory with all parents needed, relative to the temporary directory
    CreateDirectory {
        /// Path to output to, relative to the temporary per-module-per-identity directory.
        directory_output_path: &'a RelativePath,
    },
}

/// Try to build substituted CLI args from a bunch of command arguments - None = presumed empty,
/// this is a convenience function :)
pub fn build_cli_args<'a, T: IntoIterator<Item = &'a ShellSubstitutionRule>>(
    argsubs: Option<T>,
    environ: &ShellModuleEnvironment<'a>,
) -> Result<Vec<String>, shellexpand::LookupError<InvalidShellVariableLookup>> {
    match argsubs {
        Some(intoiterator) => {
            let iterator = intoiterator.into_iter();
            Ok(iterator
                .map(|v| v.try_substitute(environ))
                .collect::<Result<Vec<_>, _>>()?)
        }
        None => Ok(vec![]),
    }
}

impl<'a> EphemeralDirectoryInstruction<'a> {
    /// If this needs a module data directory to even be run.
    pub fn needs_module_data_directory(&self) -> bool {
        match self {
            EphemeralDirectoryInstruction::RunScript { .. } => true,
            EphemeralDirectoryInstruction::RunMultiCommand { .. } => false,
            EphemeralDirectoryInstruction::CreateFile { .. } => false,
            EphemeralDirectoryInstruction::CreateDirectory { .. } => false,
        }
    }

    /// Attempt to run this individual directory instruction, with the given temporary directory
    /// location and the given module data directory location.
    ///
    /// Passes a channel that can take in stderrs and stdouts for logging purposes - note that this
    /// function will still wait on command output, but another thread handles logging.
    pub fn try_run(
        &self,
        absolute_per_module_and_identity_directory_path: &Path,
        absolute_module_data_directory: Option<&Path>,
        environment: &ShellModuleEnvironment<'a>,
        mut logpoolchannel: crate::util::subcmdlog::LogPoolSend,
    ) -> AnyResult<()> {
        if self.needs_module_data_directory() && absolute_module_data_directory.is_none() {
            bail!(
                "No module data directory, when it was needed to execute this ephemeral data \
                 directory instruction."
            );
        }
        let absolute_module_data_directory =
            absolute_module_data_directory.expect("We just checked.");

        match self {
            EphemeralDirectoryInstruction::RunScript {
                script_path,
                command_arguments,
            } => {
                let full_script_path = absolute_module_data_directory.join(&script_path.0);
                let command_arguments: Vec<String> = build_cli_args(
                    command_arguments.as_ref().map(|v| v.iter().copied()),
                    environment,
                )?;

                info!(
                    "Running module data script at {}, arguments {:#?}",
                    full_script_path.display(),
                    &command_arguments
                );

                let fs2 = full_script_path.clone();
                let fs3 = full_script_path.clone();

                let exit_status = std::process::Command::new(&full_script_path)
                    .stdin(Stdio::null())
                    .args(command_arguments)
                    .make_environment_match(environment)
                    .spawn_and_wait(
                        &mut logpoolchannel,
                        move |line| info!("[module script: {}][stdout] {}", fs3.display(), line),
                        move |line| warn!("[module script: {}][stderr] {}", fs2.display(), line),
                    )?;

                if exit_status.success() {
                    info!(
                        "[module script: {}] Exited with {}",
                        full_script_path.display(),
                        exit_status
                    );
                } else {
                    warn!(
                        "[module script: {}] Exited with {}",
                        full_script_path.display(),
                        exit_status
                    );
                }
            }
            EphemeralDirectoryInstruction::RunMultiCommand {
                command,
                arguments,
                multi_arguments,
            } => {
                let vv = vec![vec![]];
                let v = vec![];
                // Normalise multi_arguments with a single, empty default, so that we dont need two
                // sets of code for the case of no defined multi_arguments, and defined
                // multi_arguments nya
                //
                let multi_arguments = multi_arguments.as_ref().unwrap_or(&vv);
                let base_args = arguments.as_ref().unwrap_or(&v);
                // Resolve the arguments nyaaa
                let base_args = build_cli_args(Some(base_args.iter().copied()), environment)?;
                let mut multi_arguments_new: Vec<Vec<String>> =
                    Vec::with_capacity(multi_arguments.len());
                for inner_argument_list in multi_arguments.iter() {
                    multi_arguments_new.push(build_cli_args(
                        Some(inner_argument_list.iter().copied()),
                        environment,
                    )?);
                }

                // Now to run the commands!
                for per_command_args in &multi_arguments_new {
                    let all_args_iter = base_args
                        .iter()
                        .chain(per_command_args.iter())
                        .map(|a| a.as_str());
                    info!("Running module multi-command {}", command);
                    let exit_status = std::process::Command::new(command)
                        .stdin(Stdio::null())
                        .args(all_args_iter)
                        .make_environment_match(environment)
                        .spawn_and_wait(
                            &mut logpoolchannel,
                            |line| info!("[module multicommand][stdout] {}", line),
                            |line| warn!("[moudle multicommand][stderr] {}", line),
                        )?;

                    if exit_status.success() {
                        info!("[module multicommand] exited with {}", exit_status);
                    } else {
                        warn!("[module multicommand] exited with {}", exit_status)
                    }
                }
            }
            EphemeralDirectoryInstruction::CreateFile {
                file_output_path,
                lines,
            } => {
                let full_file_path =
                    absolute_per_module_and_identity_directory_path.join(file_output_path);
                info!(
                    "Ensuring existence of parent directories for {}",
                    full_file_path.display()
                );
                full_file_path
                    .parent()
                    .map(std::fs::create_dir_all)
                    .unwrap_or(Ok(()))?;
                info!(
                    "Generating file {} from multiline substitution template",
                    full_file_path.display()
                );
                let mut file_out = LineWriter::new(std::fs::File::create(full_file_path)?);
                for unchecked_line in lines.iter() {
                    let checked_line = unchecked_line.try_substitute(environment)?;
                    file_out.write_all(checked_line.as_ref())?;
                    file_out.write_all("\n".as_ref())?;
                }
                file_out.flush()?;
            }
            EphemeralDirectoryInstruction::CreateDirectory {
                directory_output_path,
            } => {
                info!(
                    "Ensuring directory {} exists.",
                    directory_output_path.as_ref().display()
                );
                std::fs::create_dir_all(directory_output_path)?;
            }
        }
        Ok(())
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
