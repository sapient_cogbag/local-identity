//! Objects for storing shell module environment variables (like e.g. identity variables) which can
//! be converted into hashmaps of shell variable names of the given type to string values.

/// Core environment variable object storage.
pub struct EnvironmentVariables {
    raw_env_vars: Vec<(ShellVarNameBuf, String)>,
}

impl EnvironmentVariables {
    pub fn new(env_vars: std::env::Vars) -> Self {
        Self {
            raw_env_vars: env_vars
                .map(|(vname, vvalue)| {
                    (
                        ShellVarNameBuf::from_name_and_type(ShellVarType::Environment, vname),
                        vvalue,
                    )
                })
                .collect(),
        }
    }

    /// Access a hashmap that uses references as keys. Note that the resulting item is entirely
    /// mutable and changeable.
    pub fn with_reference_keys(&self) -> HashMap<&'_ ShellVarName, String> {
        self.raw_env_vars
            .iter()
            .map(|(k, v)| (k.as_ref(), v.clone()))
            .collect()
    }
}

/// Identity variable storage, generated from identity local variables.
pub struct IdentityVariables<'id> {
    raw_identity_vars: Vec<(ShellVarNameBuf, &'id str)>,
}

impl<'id> IdentityVariables<'id> {
    /// Generate a hashmap referencing this object to hold the identity-local shell module
    /// variables.
    pub fn with_reference_keys(&self) -> HashMap<&'_ ShellVarName, String> {
        self.raw_identity_vars
            .iter()
            .map(|(k, v)| (k.as_ref(), (*v).to_owned()))
            .collect()
    }

    /// Generate identity variables from an identity
    pub fn make_from_identity(id: IdentityFinalised<'id>) -> Self {
        let id_var_name =
            |name: &str| ShellVarNameBuf::from_name_and_type(ShellVarType::Identity, name);
        let mut list = vec![];
        let mut push = |name: &str, value: &'id str| list.push((id_var_name(name), value));

        // Basic identity variables nya
        push("NAME", id.core_identity.name.as_ref());
        push("FULL_NAME", id.core_identity.full_name);

        let mut maybe_push =
            |name: &str, value: Option<&'id str>| value.map(|v| list.push((id_var_name(name), v)));
        maybe_push("EMAIL", id.core_identity.email);

        // Crypto key identifiers
        maybe_push("GPG_IDENTIFIER", id.gpg.map(|v| v.key_identifier));
        maybe_push("SSH_IDENTIFIER", id.ssh.map(|v| v.key));
        maybe_push(
            "X509_CERTIFICATE_IDENTIFIER",
            id.x509.map(|v| v.certificate_identifier),
        );

        Self {
            raw_identity_vars: list,
        }
    }
}

/// Utility variable storage, generated from xdg and similar such things ^.^
pub struct UtilityVariables {
    raw_utility_vars: Vec<(ShellVarNameBuf, String)>,
}

impl UtilityVariables {
    /// Generate a hashmap referencing this object to hold utility variables.
    pub fn with_reference_keys(&self) -> HashMap<&'_ ShellVarName, String> {
        self.raw_utility_vars
            .iter()
            .map(|(k, v)| (k.as_ref(), (*v).to_owned()))
            .collect()
    }

    /// Generate the actual utility variables
    ///
    /// Takes 3 things - XDG context, module data directory directory, and identity+module-local temporary
    /// directory.
    pub fn make_from_xdgcontext(
        xdg: &XdgDirsWithHome,
        module_data_directory: Option<&Path>,
        module_identity_temp_directory: Option<&Path>,
    ) -> Self {
        let utility_var_name =
            |name: &str| ShellVarNameBuf::from_name_and_type(ShellVarType::Utility, name);
        let mut list = vec![];
        let mut maybe_push = |name: &str, value: Result<Cow<'_, str>, XdgErrorFlat>| match value {
            Ok(v) => list.push((utility_var_name(name), v.into_owned())),
            Err(e) => warn!(
                "Cannot set utility variable {}, error constructing value: {}",
                name, e
            ),
        };

        let make_pathbuf_unicode_or_crash = |v: Result<PathBuf, XdgErrorFlat>| {
            v.map(|v| {
                v.to_str()
                    .expect("Non-unicode path for an XDG variable - unsupported")
                    .to_owned()
            })
        };

        maybe_push(
            "HOME",
            xdg.home()
                .map(|v| v.to_str().expect("$HOME is not utf8-valid!"))
                .map_err(Into::into)
                .map(Cow::Borrowed),
        );
        maybe_push(
            "XDG_CONFIG_HOME",
            make_pathbuf_unicode_or_crash(xdg.xdg_config_home()).map(Cow::Owned),
        );
        maybe_push(
            "XDG_CACHE_HOME",
            make_pathbuf_unicode_or_crash(xdg.xdg_cache_home()).map(Cow::Owned),
        );
        maybe_push(
            "XDG_DATA_HOME",
            make_pathbuf_unicode_or_crash(xdg.xdg_data_home()).map(Cow::Owned),
        );
        maybe_push(
            "XDG_STATE_HOME",
            make_pathbuf_unicode_or_crash(xdg.xdg_state_home()).map(Cow::Owned),
        );

        if let Some(data_directory) = module_data_directory {
            list.push((
                utility_var_name(MODULE_DATA_DIRECTORY_VARIABLE),
                make_pathbuf_unicode_or_crash(Ok(data_directory.to_owned())).expect("input was ok"),
            ));
        }

        if let Some(module_identity_tmp_data_directory) = module_identity_temp_directory {
            list.push((
                utility_var_name(TMPDIR_UTILITY_VARIABLE),
                make_pathbuf_unicode_or_crash(Ok(module_identity_tmp_data_directory.to_owned()))
                    .expect("input was ok"),
            ));
        }

        Self {
            raw_utility_vars: list,
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    borrow::Cow,
    collections::HashMap,
    path::{Path, PathBuf},
};

use log::warn;

use crate::{
    identity::IdentityFinalised,
    util::xdgstuff::{XdgDirsWithHome, XdgErrorFlat},
};

use super::{
    ShellVarName, ShellVarNameBuf, ShellVarType, MODULE_DATA_DIRECTORY_VARIABLE,
    TMPDIR_UTILITY_VARIABLE,
};
