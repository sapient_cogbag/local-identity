//! Extra customisation for the environment associated with an identity.

impl<'a> BasicIdentityModule for CustomEnvironment<'a> {
    fn raw_name(&self) -> &ModuleName { "custom-env".into() }
}

impl<'a> ShellIdentityModule for CustomEnvironment<'a> {
    fn specify_environment_variables(&self, environment_variable_feed: &mut dyn EnvVarEmitter) {
        for (env_var, env_value) in self.vars.iter() {
            environment_variable_feed.emit_env_var(env_var, env_value);
        }
    }

    fn needs_scratch_directory(&self) -> bool { false }
}

#[cfg(test)]
mod test {
    use super::super::test_utilities::EnvTrackingEmitter;
    use crate::{identity::CustomEnvironment, state_modules::ShellIdentityModule};

    #[test]
    pub fn basic_custom_env_test() {
        let pairs = vec![
            ("FIRST_ENV_VAR", "testval1"),
            ("SECOND_ENV_VAR", "testval2"),
        ];
        let mut emitter = EnvTrackingEmitter::default();
        let customenv = CustomEnvironment {
            vars: pairs.iter().cloned().collect(),
        };

        customenv.specify_environment_variables(&mut emitter);

        assert_eq!(
            emitter
                .emitted_vars_hashed
                .get("FIRST_ENV_VAR")
                .map(AsRef::as_ref),
            Some("testval1")
        );
        assert_eq!(
            emitter
                .emitted_vars_hashed
                .get("SECOND_ENV_VAR")
                .map(AsRef::as_ref),
            Some("testval2")
        );
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::identity::CustomEnvironment;

use super::{BasicIdentityModule, EnvVarEmitter, ModuleName, ShellIdentityModule};
