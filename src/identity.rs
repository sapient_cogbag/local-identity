//! Represents core identity with modules for subidentity components.

pub mod crypto;
pub mod git;

use crate::util::is_default;
use log::error;
use schemars::JsonSchema;

use serde::{Deserialize, Serialize};
use std::{borrow::Cow, collections::HashMap, convert::Infallible, fmt::Display, str::FromStr};

use crate::dst_newtype;

use self::{
    crypto::{
        CryptoIdentities, GpgIdentity, GpgIdentityFinalised, SshIdentity, X509CertificateIdentity,
    },
    git::{GitIdentity, GitIdentityFinalised},
};

dst_newtype! {
    /// Owned name of an identity.
    pub IdentityNameBuf(pub) from String;
    /// DST of an identity name (like [`str`])
    pub IdentityName(pub) from str;
    reffn pub from_ref_str;
    mutfn pub from_ref_str_mut;
    owned_only_derives Clone, Default;
    serde serialize deserialize;
    json_schema yes;
    display enable;
}

/// This is an infallible conversion - all identity names are valid <3
impl FromStr for IdentityNameBuf {
    type Err = Infallible;

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> { Ok(IdentityName::from_ref_str(s).to_owned()) }
}

/// Trait indicating that a type can have various defaults filled in, when provided with other
/// types.
pub trait CascadingDefaults<'a> {
    /// What types (as tuple) are needed to fill in all the defaults.
    ///
    /// # Good Usage
    /// Make this contain types that themselves are a [FillableDefaults::Finalised]. That way you
    /// can ensure that you never pull values from types that have not already had their defaults
    /// filled in.
    ///
    /// If we had real variadic generics I could implement this as a bound but sadly, no :(
    type Prerequisites: 'a;
    /// The finalised type that has all defaults filled in ^.^
    type Finalised;

    /// Function to finalise all the defaults of this type.
    fn with_cascaded_prerequisites(&self, prerequisites: Self::Prerequisites) -> Self::Finalised;
}

/// Most basic components of identity, used to supply defaults.
#[derive(Deserialize, Debug, Clone, JsonSchema, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct PrimitiveIdentity<'a> {
    /// Actual name of the identity in question ^.^
    #[serde(borrow)]
    pub name: &'a IdentityName,
    /// Full name of the person being identified - used to locate leaks of all kinds of
    /// combinatorics of this by splitting into little space chunks :)
    ///
    /// Default is [Self::name]
    #[serde(default, skip_serializing_if = "crate::util::is_default")]
    pub full_name: Option<&'a str>,
    /// Email associated with this identity
    #[serde(default)]
    pub email: Option<&'a str>,

    /// Values considered leaks when not prefixed by a bunch of other alphabetic characters with no
    /// whitespace or separator (some names overlap with common words so without this we'd end up
    /// with aggressive false positives >.<)
    ///
    /// This value always gets [full name components][Self::full_name] clunked onto it. as a
    /// heuristic ^.^, as well as the email address.
    #[serde(borrow, default = "Vec::new")]
    // Schemars does not agree with non-standard (zero argument) default nya
    #[schemars(default)]
    pub leakmatchers: Vec<Cow<'a, str>>,

    /// Explicitly force no assumption of a default GPG identifier ^.^
    ///
    /// By default, even if a GPG key is not set, this will generate a GPG key identifier (to look
    /// for an associated key) using your full name. This disables even that and prevents the
    /// assumption of a gpg identity.
    #[serde(
        default = "crate::util::false_val",
        skip_serializing_if = "crate::util::is_default"
    )]
    pub no_default_gpg_identity: bool,
}

/// Like [PrimitiveIdentity] but the defaults are all finalised.
#[derive(Serialize, Debug, Clone)]
pub struct PrimitiveIdentityFinalised<'a> {
    pub name: &'a IdentityName,
    /// Default is [Self::name]
    pub full_name: &'a str,
    /// Email has no sensible default >.<
    pub email: Option<&'a str>,

    /// Leak matching values. At this point they are very much finalised
    pub leakmatchers: Vec<Cow<'a, str>>,

    /// Explicitly force no assumption of a default GPG identifier ^.^
    pub no_default_gpg_identity: bool,
}

/// Turn a full name string into single names by splitting on separator heuristics, stripping, and
/// removing empty elements
pub(crate) fn splice_apart_full_name(fullname: &str) -> impl Iterator<Item = &'_ str> + '_ {
    fullname
        .split(|c: char| {
            // https://unicode.org/reports/tr44/#General_Category_Values nyaaa
            // We care about letters and nonspacing marks (I think)
            !(unic_ucd_category::GeneralCategory::of(c).is_letter()
                || unic_ucd_category::GeneralCategory::of(c).is_mark())
        })
        .map(|s| s.trim())
        // Short names too common in english text (notably, *code* nyaaa)
        //
        // Note that ascii-uncommon characters both take more than 2 bytes and typically are rare in
        // the kinds of text (compatible with random poor-unicode-impl systems), so even if they are
        // single graphemes it's still good to match on them anyway.
        //
        // i18n note: May be useful to come up with a different system using graphemes in all cases for
        // global compatibility but this poses a risk of graphemes that are unique because they *are*
        // someone's name getting deleted simply because of being less than two graphemes nya.
        //
        // This is a hard problem that would probably require a grapheme frequency graph to solve
        // properly :/ nya.
        .filter(|name_chunk| name_chunk.len() > 2)
}

impl<'a> CascadingDefaults<'a> for PrimitiveIdentity<'a> {
    type Finalised = PrimitiveIdentityFinalised<'a>;
    type Prerequisites = ();

    /// Calculate local default values for the ones not present ^.^
    fn with_cascaded_prerequisites(
        &self,
        _: Self::Prerequisites,
    ) -> PrimitiveIdentityFinalised<'a> {
        // Cow - Make everything a reference to the old Cows nya
        let full_name: &'a str = self.full_name.unwrap_or_else(|| self.name.as_ref());
        let leakmatchers = self
            .leakmatchers
            .iter()
            .cloned()
            .chain(splice_apart_full_name(full_name).map(|a| a.into()))
            .chain(self.email.as_ref().copied().map(Cow::Borrowed))
            .collect::<_>();
        PrimitiveIdentityFinalised {
            name: self.name,
            full_name,
            email: self.email,
            leakmatchers,
            no_default_gpg_identity: self.no_default_gpg_identity,
        }
    }
}

#[derive(Clone, Deserialize, Serialize, Default, Debug, JsonSchema)]
#[serde(rename_all = "kebab-case")]
/// Holds a custom environment for a given identity.
pub struct CustomEnvironment<'a> {
    #[serde(borrow, default)]
    /// Custom environment variables
    pub vars: HashMap<&'a str, &'a str>,
}

#[derive(Deserialize, Debug, JsonSchema, Serialize)]
#[serde(rename_all = "kebab-case")]
/// Core identity configuration modules
pub struct Identity<'a> {
    #[serde(flatten, borrow)]
    pub core_identity: PrimitiveIdentity<'a>,

    #[serde(borrow, default)]
    pub gpg: Option<GpgIdentity<'a>>,
    #[serde(borrow, default, skip_serializing_if = "crate::util::is_default")]
    pub ssh: Option<SshIdentity<'a>>,
    #[serde(borrow, default, skip_serializing_if = "crate::util::is_default")]
    pub x509: Option<X509CertificateIdentity<'a>>,

    #[serde(borrow, default)]
    pub git: GitIdentity<'a>,

    #[serde(borrow, default)]
    /// Custom environment variables for this identity, in addition to that provided by various identity
    /// modules.
    pub custom_env: CustomEnvironment<'a>,
}

/// Note that the sheer complexity of this task, means we reduce to toml rendering >.<
impl<'a> Display for Identity<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let intermediate_string = toml::ser::to_string_pretty(self).map_err(|e| {
            error!(
                "Error displaying identity {} as toml because of serialization error {:?}",
                self.core_identity.name, e
            );
            std::fmt::Error
        })?;
        writeln!(f, "##### IDENTITY {} #####", self.core_identity.name)?;
        f.write_str(&intermediate_string)
    }
}

/// Default-finalised version of the [Identity] struct - with all it's defaults and component
/// defaults filled in ^.^
#[derive(Serialize, Debug, Clone)]
#[serde(rename_all = "kebab-case")]
pub struct IdentityFinalised<'a> {
    #[serde(flatten)]
    pub core_identity: PrimitiveIdentityFinalised<'a>,

    #[serde(skip_serializing_if = "is_default")]
    pub gpg: Option<GpgIdentityFinalised<'a>>,

    #[serde(skip_serializing_if = "is_default")]
    pub ssh: Option<SshIdentity<'a>>,

    #[serde(skip_serializing_if = "is_default")]
    pub x509: Option<X509CertificateIdentity<'a>>,

    #[serde(borrow, default)]
    pub git: GitIdentityFinalised<'a>,

    pub custom_env: CustomEnvironment<'a>,
}

/// Note that the sheer complexity of this task, means we reduce to toml rendering >.<
impl<'a> Display for IdentityFinalised<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let intermediate_string = toml::ser::to_string_pretty(self).map_err(|e| {
            error!(
                "Error displaying finalised identity {} as toml because of serialization error {}",
                self.core_identity.name, e
            );
            std::fmt::Error
        })?;
        writeln!(
            f,
            "##### IDENTITY(FINALISED) {} #####",
            self.core_identity.name
        )?;
        f.write_str(&intermediate_string)
    }
}
impl<'a> CascadingDefaults<'a> for Identity<'a> {
    type Finalised = IdentityFinalised<'a>;
    type Prerequisites = ();

    fn with_cascaded_prerequisites(&self, _: Self::Prerequisites) -> Self::Finalised {
        let core_identity = self.core_identity.with_cascaded_prerequisites(());

        let gpg = self
            .gpg
            .as_ref()
            .map(|gpg_id| gpg_id.with_cascaded_prerequisites(core_identity.clone()));

        // construct defaults even when no gpg identity specified nya
        let gpg = gpg.or_else(|| {
            if !self.core_identity.no_default_gpg_identity {
                // ::default() is not static so doing this with that makes the borrow checker treat the
                // value as a temporary (which it is nya). But in this case, the lifetime is really
                // static ^.^
                Some(
                    GpgIdentity {
                        key_identifier: None,
                    }
                    .with_cascaded_prerequisites(core_identity.clone()),
                )
            } else {
                None
            }
        });

        let ssh = self.ssh;
        let x509 = self.x509;

        IdentityFinalised {
            core_identity: core_identity.clone(),
            gpg,
            ssh,
            x509,
            git: self
                .git
                .with_cascaded_prerequisites((core_identity, CryptoIdentities {
                    gpg_identity: gpg,
                    ssh_identity: ssh,
                    x509_identity: x509,
                })),
            custom_env: self.custom_env.clone(),
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
