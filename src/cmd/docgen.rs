//! Command for generating documentation for this tool
mod jsfh;
use log::*;

#[derive(clap::ArgEnum, Clone, Copy)]
pub enum DocumentationFormat {
    JSONSchema,
    /// JSFH with template_name=md_nested
    JsfhMarkdownNested,
    /// JSFH with template_name=md
    JsfhMarkdown,
    /// JSFH with template_name=js
    JsfhHtmlJs,
    /// JSFH with template_name=flat
    JsfhHtmlFlat,
}

impl DocumentationFormat {
    /// The structure of the documentation is written in JSON schema
    ///
    /// This dictates how to actually print it to a given output.
    pub fn print_raw_json_schema(
        &self,
        raw_json_schema: &schemars::schema::RootSchema,
        target: StdoutOrPath,
    ) -> Result<(), anyhow::Error> {
        match self {
            DocumentationFormat::JSONSchema => {
                trace!("Opening filepath for output if present, and locking stdout if present.");
                let mut locked_tgt = target.open_filepath_writeable()?.with_locked_stdout();
                debug!("Writing out raw JSON schema.");
                locked_tgt.bulk_write_operation(|w: &mut dyn Write| {
                    serde_json::to_writer_pretty(w, &raw_json_schema)
                })?;
                trace!("Flushing raw JSON schema");
                locked_tgt.flush()?;
            }
            DocumentationFormat::JsfhMarkdown => {
                jsfh_generation(jsfh::RawJSFHTemplateName::Markdown, raw_json_schema, target)?
            }
            DocumentationFormat::JsfhMarkdownNested => jsfh_generation(
                jsfh::RawJSFHTemplateName::MarkdownNested,
                raw_json_schema,
                target,
            )?,
            DocumentationFormat::JsfhHtmlJs => {
                jsfh_generation(jsfh::RawJSFHTemplateName::HtmlJs, raw_json_schema, target)?
            }
            DocumentationFormat::JsfhHtmlFlat => {
                jsfh_generation(jsfh::RawJSFHTemplateName::HtmlFlat, raw_json_schema, target)?
            }
        };
        Ok(())
    }
}

impl Default for DocumentationFormat {
    fn default() -> Self { Self::JSONSchema }
}

#[derive(clap::ArgEnum, Clone, Copy)]
/// What to produce the documentation of.
pub enum DocumentationCollection {
    /// Document identity files ^.^
    IdentityFiles,
}

impl Default for DocumentationCollection {
    fn default() -> Self { Self::IdentityFiles }
}

impl DocumentationCollection {
    pub fn json_schema(&self) -> schemars::schema::RootSchema {
        match self {
            DocumentationCollection::IdentityFiles => {
                info!("Generating raw JSON schema for Identity files");
                schemars::schema_for!(crate::identity::Identity)
            }
        }
    }
}

#[derive(clap::Args)]
/// Generate documentation for various formats used by this tool, based on JSON schema.
#[clap(name = "docgen")]
pub struct DocumentationGenerator {
    #[clap(arg_enum, default_value_t, long)]
    /// Output format for the documentation.
    ///
    /// json-schema: output raw JSON schema
    ///
    /// jsfh-*:
    ///   Use json-schema-for-humans (jsfh) to output markdown or html files with
    ///   several different templates. This only works if JSFH is actually installed and this
    ///   program can find it on the pythonpath.
    ///
    ///   When using templates with JS and CSS, you may need to place the JS and CSS files into the
    ///   same folder as wherever your .html output is placed.
    ///   Obtaining this is a bit janky - in the future a utility command might be added to
    ///   make it easier.
    ///
    /// [jsfh]: https://coveooss.github.io/json-schema-for-humans/#/
    pub output_format: DocumentationFormat,

    /// Which documentation to actually output
    ///
    /// identity-files: Output documentation for identity file configuration.
    #[clap(arg_enum, default_value_t, long)]
    pub documentation: DocumentationCollection,

    #[clap(short='o', long, parse(from_os_str), value_hint = clap::ValueHint::FilePath)]
    /// output file path, if any.
    ///
    /// When unsupplied, output to stdout.
    pub output_path: Option<PathBuf>,
}

impl super::CmdExec for DocumentationGenerator {
    type Error = anyhow::Error;

    fn exec(self) -> Result<(), Self::Error> {
        let json_schema = self.documentation.json_schema();
        self.output_format
            .print_raw_json_schema(&json_schema, self.output_path.clone().into())?;

        Ok(())
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{io::Write, path::PathBuf};

use crate::util::args::StdoutOrPath;

use self::jsfh::jsfh_generation;
