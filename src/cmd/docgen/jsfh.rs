//! Little module for Json Schema For Humans stuff ^.^
//!
//! See [here](https://coveooss.github.io/json-schema-for-humans/#/) for docs stuff ^.^

use std::{
    fs::File,
    io::{stderr, stdout, Read, Write},
    path::Path,
    process::{ExitStatus, Stdio},
};

use crate::util::{args::StdoutOrPath, err::ioerr, io::ReadExt};
use schemars::schema::RootSchema;
use tempfile::{NamedTempFile, TempPath};

/// Generates a basic "python_executable <path to json_schema_for_humans>" partially intialised
/// [std::process::Command]. You need to provide extra arguments and process the output.
///
/// If the provided argument is true, then redirect stdout info messages to a stored variable [std]. Else, redirect
/// them to /dev/null
fn raw_json_schema_for_humans_command(
    redirect_stdout_to_mem: bool,
) -> std::io::Result<std::process::Command> {
    use crate::util::shellenv::py;
    let pypath = py::python_path()?;
    // Found by experimentation nya
    let jsfh_python_script = pypath
        .find_in_pythonpath(std::path::Path::new("json_schema_for_humans/cli.py"))
        .ok_or_else(|| {
            ioerr::other(
                "Could not find json_schema_for_humans/cli.py in python's search path (see \
                 python's `sys.path`)",
            )
        })?;
    let mut res = std::process::Command::new(py::PYEXEC);
    if redirect_stdout_to_mem {
        res.stdout(Stdio::piped())
    } else {
        res.stdout(Stdio::null())
    };
    res.arg(&jsfh_python_script);
    Ok(res)
}

/// Efficiently write a [RootSchema] to a file
/// - more efficient than [std::fs::write] as it does not use intermediary [Vec<u8>] or [String]
pub fn write_schema_to_file_raw(mut file: &mut File, schema: &RootSchema) -> std::io::Result<()> {
    // we can serialize directly into the file nya, rather than
    // going through an intermediate Vec<u8> or String as would be required with
    // [std::fs::write], despite that being less lines meow
    serde_json::to_writer_pretty(&mut file, schema)?;
    file.sync_data()
}

/// Write a schema to a newly created temporary file. Returns the temporary file path.
pub fn write_schema_to_file(schema: &RootSchema) -> std::io::Result<TempPath> {
    let mut scratch_file = NamedTempFile::new()?;
    write_schema_to_file_raw(scratch_file.as_file_mut(), schema)?;
    Ok(scratch_file.into_temp_path())
}

/// Enum of possible json_schema_for_humans template names
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum RawJSFHTemplateName {
    HtmlJs,
    HtmlFlat,
    Markdown,
    MarkdownNested,
}

// {{{ Implementations for getting specific information meow.
impl RawJSFHTemplateName {
    /// Args to add to a JSFH call to make it use this template
    const fn template_name_config_params(&self) -> &'static [&'static str; 2] {
        const CFG: &str = "--config";
        match self {
            RawJSFHTemplateName::HtmlJs => &[CFG, "template_name=js"],
            RawJSFHTemplateName::HtmlFlat => &[CFG, "template_name=flat"],
            RawJSFHTemplateName::Markdown => &[CFG, "template_name=md"],
            RawJSFHTemplateName::MarkdownNested => &[CFG, "template_name=md_nested"],
        }
    }
}
// }}}

/// Run json_schema_for_humans command with the given template_name (js, flat, md, md_nested),
/// with the given input file and output file.
///
/// Args:
///  * template: simply the template to use.
///  * full path to input
///  * full path to output
///  * whether to redirect JSFH output to stderr or /dev/null
fn raw_jsfh_command_with_template(
    template: RawJSFHTemplateName,
    in_path: &Path,
    out_path: &Path,
    redirect_stdout_to_stderr: bool,
) -> std::io::Result<()> {
    let mut child_proc = raw_json_schema_for_humans_command(redirect_stdout_to_stderr)?
        .args(template.template_name_config_params())
        .arg(in_path)
        .arg(out_path)
        .spawn()?;
    let child_proc_stdout = child_proc.stdout.take();
    let stderr_forward_thread = child_proc_stdout.map(|mut child_proc_stdout| {
        std::thread::spawn(move || -> std::io::Result<()> {
            const BUF_SIZE: usize = 4096;
            let mut buf = [0u8; BUF_SIZE];
            loop {
                let num_bytes_read = child_proc_stdout.read_interrupt_retry(&mut buf)?;
                match num_bytes_read {
                    0 => break,
                    n => {
                        let actual_buf = &buf[..n];
                        let mut stderr_lck = stderr().lock();
                        stderr_lck.write_all(actual_buf)?;
                    }
                }
            }
            Ok(())
        })
    });
    // Convert exit errors into Other io error nya
    let res = child_proc.wait().and_then(|exit_status: ExitStatus| {
        exit_status
            .exit_ok()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    });
    let stderr_thread_res_maybe = stderr_forward_thread.map(|handle| {
        handle
            .join()
            .expect("The thread shouldnt panic, it passes off errors")
    });
    if let Some(stderr_thread_res) = stderr_thread_res_maybe {
        stderr_thread_res?;
    };
    res
}

/// Run json_schema_for_humans command with the given template name, and root schema,
/// and final output target.
///
/// Returns a standard error thing.
pub fn jsfh_generation(
    template: RawJSFHTemplateName,
    root_schema: &RootSchema,
    final_output_target: StdoutOrPath,
) -> std::io::Result<()> {
    let in_temp_filepath = write_schema_to_file(root_schema)?;
    let maybe_temppath = match &final_output_target {
        StdoutOrPath::Stdout => Some(NamedTempFile::new()?.into_temp_path()),
        StdoutOrPath::FilePath(_) => None,
    };

    let target_path: &Path = maybe_temppath
        .as_ref()
        .map(AsRef::<Path>::as_ref)
        .unwrap_or_else(|| match &final_output_target {
            StdoutOrPath::Stdout => {
                unreachable!("None option only set when FilePath is the enum option.")
            }
            StdoutOrPath::FilePath(fp) => fp,
        });

    raw_jsfh_command_with_template(template, &in_temp_filepath, target_path, true)?;

    // Print out temporary file contents, if necessary ^.^
    if let Some(temporary_file_output) = maybe_temppath {
        const BUF_SIZE: usize = 4096;
        let mut buf = [0u8; BUF_SIZE];
        let mut ofile = File::open(temporary_file_output)?;
        let mut locked_stdout = stdout().lock();
        loop {
            let n = ofile.read(&mut buf)?;
            if n == 0 {
                break;
            } else {
                let used_buf = &buf[..n];
                locked_stdout.write_all(used_buf)?;
            }
        }
        locked_stdout.flush()?;
    }

    Ok(())
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
