//! Module containing commands intended to aid in identifying problems with the daemon, module
//! definitions, etc..

/// Encodes the output formats
#[derive(
    Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Deserialize, Serialize, JsonSchema, clap::ArgEnum,
)]
pub enum OutputFormat {
    /// Output the information in JSON
    JSON,
    /// Output the information as simple display.
    Human,
}

impl Default for OutputFormat {
    fn default() -> Self { Self::Human }
}

impl OutputFormat {
    /// Write out a single piece of data
    ///
    /// Note this flushes the output.
    pub fn print_out_single<T: Display + serde::Serialize>(
        &self,
        data: &T,
        mut output_target: &mut dyn Write,
    ) -> std::io::Result<()> {
        match self {
            OutputFormat::JSON => {
                serde_json::to_writer_pretty(&mut output_target, data)?;
            }
            OutputFormat::Human => {
                writeln!(&mut output_target, "{}", data)?;
            }
        }
        output_target.flush()
    }
}

/// Module for attempting to find and/or load various files.
pub mod file_loading {}

/// Literally just a unified "try load global command config" so we can process it on a per-command
/// basis rather than worrying about if it fails before a command is executed ^.^
macro_rules! glcfg {
    {} => {GlobalConfig::try_load()?}
}

/// Show various properties.
pub mod show_commands {
    use std::{
        fmt::Display,
        io::stdout,
        path::{Path, PathBuf},
    };

    use schemars::JsonSchema;
    use serde::{Deserialize, Serialize};

    use crate::{
        cmd::CmdExecWithPassthrough,
        identity::IdentityName,
        state_modules::{GlobalConfig, IdentityLocation},
        util::xdgstuff::XdgDirsWithHome,
    };

    use super::OutputFormat;

    #[derive(
        Clone,
        Copy,
        PartialEq,
        Eq,
        PartialOrd,
        Ord,
        Deserialize,
        Serialize,
        JsonSchema,
        clap::Subcommand,
    )]
    /// Command to show various debug items for diagnosing errors with things like loading
    /// identities or modules, or finding where the hell they get put in the first place.
    pub enum ShowCmd {
        /// Show the set of paths from which modules are loaded, in highest to lowest priority.
        ModuleLoadPath,
        /// Show some basic XDG stuff.
        XdgStuff,
        /// Attempt to load just the global configuration paths.
        GlobalConfigurationPaths,
        /// Get the locations of identities on the filesystem.
        IdentityLocations,
        /// Get the directory from which identities are loaded.
        IdentityDirectory,
        /// Print out the location of the root configuration file that will be loaded if it exists
        /// to override other loading paths.
        RootConfigPath,
    }

    impl CmdExecWithPassthrough for ShowCmd {
        type Error = anyhow::Error;
        type Passthrough = OutputFormat;

        fn exec_passthrough(self, oformat: Self::Passthrough) -> Result<(), Self::Error> {
            let mut stdout = stdout();
            match self {
                ShowCmd::ModuleLoadPath => {
                    let global_cfg = glcfg! {};
                    let paths = global_cfg.module_paths();
                    oformat
                        .print_out_single(paths, &mut stdout)
                        .map_err(Into::into)
                }
                ShowCmd::XdgStuff => {
                    let xdg_stuff =
                        crate::util::xdgstuff::XdgPrintableInfo::new(&XdgDirsWithHome::new());
                    oformat
                        .print_out_single(&xdg_stuff, &mut stdout)
                        .map_err(Into::into)
                }
                ShowCmd::GlobalConfigurationPaths => {
                    let global_cfg = glcfg! {};
                    oformat
                        .print_out_single(&global_cfg, &mut stdout)
                        .map_err(Into::into)
                }
                ShowCmd::IdentityLocations => {
                    let global_cfg = glcfg! {};
                    let id_unresolved_directory = global_cfg.unresolved_identity_directory_path();
                    for (identity_name, identity_location) in
                        IdentityLocation::try_load(id_unresolved_directory)?.iter()
                    {
                        #[derive(Clone, Debug, PartialEq, Eq, Serialize, JsonSchema)]
                        struct Pair<'v> {
                            identity_name: &'v IdentityName,
                            identity_location: &'v Path,
                        }
                        impl<'v> Display for Pair<'v> {
                            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                                f.write_str(&self.identity_name.0)?;
                                writeln!(f, "@{}", self.identity_location.display())
                            }
                        }

                        oformat.print_out_single(
                            &Pair {
                                identity_name: identity_name.as_ref(),
                                identity_location: identity_location.location_ref(),
                            },
                            &mut stdout,
                        )?;
                    }
                    Ok(())
                }
                ShowCmd::IdentityDirectory => {
                    let global_cfg = glcfg! {};
                    let resolved_directory = global_cfg.resolved_identity_directory_path()?;
                    oformat
                        .print_out_single(&resolved_directory, &mut stdout)
                        .map_err(Into::into)
                }
                ShowCmd::RootConfigPath => {
                    let root_cfg_path = GlobalConfig::root_config_location()?;
                    #[derive(serde::Serialize)]
                    #[serde(rename_all = "kebab-case")]
                    struct GlobalCfgPath {
                        pub global_config_root_path: PathBuf,
                    }

                    impl std::fmt::Display for GlobalCfgPath {
                        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                            self.global_config_root_path.display().fmt(f)
                        }
                    }

                    oformat
                        .print_out_single(
                            &GlobalCfgPath {
                                global_config_root_path: root_cfg_path,
                            },
                            &mut stdout,
                        )
                        .map_err(Into::into)
                }
            }
        }
    }
}

#[derive(clap::Subcommand)]
pub enum DebugInnerCommands {
    #[clap(name = "show", subcommand)]
    Show(show_commands::ShowCmd),
}

#[derive(clap::Args)]
#[clap(name = "debug")]
pub struct DebugCmd {
    #[clap(long, default_value_t, arg_enum)]
    /// How to provide the output.
    pub output_format: OutputFormat,
    #[clap(subcommand)]
    pub subcommand: DebugInnerCommands,
}

impl super::CmdExec for DebugCmd {
    type Error = anyhow::Error;

    fn exec(self) -> Result<(), Self::Error> {
        match self.subcommand {
            DebugInnerCommands::Show(show_cmd) => show_cmd.exec_passthrough(self.output_format),
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{fmt::Display, io::Write};

use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use super::CmdExecWithPassthrough;
