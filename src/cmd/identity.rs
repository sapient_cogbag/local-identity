//! Commands for managing the current list of identities
use std::{
    borrow::Cow,
    io::{stdout, Write},
};

use anyhow::bail;
use log::{error, info, warn};

use crate::{
    identity::{
        crypto::{GpgIdentity, SshIdentity},
        IdentityNameBuf, PrimitiveIdentity,
    },
    state_modules::{
        GlobalConfig, IdentityDirectoryPathUnresolved, IdentityFileContents, IdentityLocation,
        LoadedIdentityFileContents,
    },
    util::shellenv::{
        self,
        editor::{editor_cmd, simply_edit},
    },
};

use super::CmdExec;

use super::debug::OutputFormat;

#[derive(clap::Args)]
/// Arguments for the creation of an identity file.
pub struct IdentityFileModificationArgs {
    /// Full name to create or modify the identity with
    #[clap(long)]
    pub with_full_name: Option<String>,
    /// Email to create or modify the identity with
    #[clap(long)]
    pub with_email: Option<String>,
    /// GPG key identifier to create or modify the identity with.
    #[clap(long)]
    pub with_gpg_identifier: Option<String>,
    #[clap(long)]
    /// SSH key identifier to create or modify the identity with - some programs like git can use
    /// identifiers for ssh keys instead of gpg keys for e.g. signing files or commits, usually
    /// provided with either a raw public or private key, or a filepath. This can be specified
    /// here.
    pub with_ssh_identifier: Option<String>,
}

#[derive(clap::Subcommand)]
/// Commands related to user-local identities.
///
/// User local identities define a set of primitive per-identity information (such as name, full
/// name, email), used to derive global configuration directories, environment variables, directory-tagging mechanisms,
/// and more abstract variables for numerous programs or general utilities, that can also be
/// specifically overridden, via mechanisms and directives specified in files (referred to as Identity Modules in the
/// terminology of the `local-identity` program) that can be supplied by the user or by the
/// distribution and enabled per-user.
pub enum Identity {
    #[clap(name = "list")]
    /// List the identities as a series of JSON values or as "human readable toml with titles".
    List {
        #[clap(arg_enum, default_value = "human", long)]
        /// The output format of that specific identity
        output_format: OutputFormat,
        #[clap(short, long)]
        /// Only output specific information for the provided identity as opposed to all identities
        /// found. Will error if the identity does not exist.
        identity_name: Option<IdentityNameBuf>,
        #[clap(long)]
        /// Output the identity information fully finalised - i.e. with all defaults filled in.
        ///
        /// WARNING - this is extremely verbose.
        output_finalised: bool,
    },
    #[clap(name = "new")]
    /// Create an identity, optionally filling in fields with values beforehand.
    NewIdentity {
        /// Name of the identity to create in the identity module directory.
        name: IdentityNameBuf,
        #[clap(short, long)]
        /// If the file exists, force it to be overwritten, instead of cancelling the operation.
        force: bool,
        #[clap(short, long)]
        /// Do not edit the file after creation
        no_edit: bool,
        #[clap(flatten)]
        special_creation_args: IdentityFileModificationArgs,
    },
    #[clap(name = "edit")]
    /// Simply edit an existing identity with your configured editor
    ///
    /// If it does not exist, error out.
    EditIdentity {
        /// Name of the identity to edit, if it exists.
        name: IdentityNameBuf,
    },
}

impl CmdExec for Identity {
    type Error = anyhow::Error;

    fn exec(self) -> Result<(), Self::Error> {
        let bare_config = GlobalConfig::try_load()?;
        let full_identity_locations =
            IdentityLocation::try_load(bare_config.unresolved_identity_directory_path())?;
        match self {
            Identity::List {
                output_format,
                identity_name,
                output_finalised,
            } => {
                let mut stdout = stdout();
                let loaded_identities =
                    IdentityFileContents::load_from_locations(full_identity_locations)?;
                let parsed_identities =
                    LoadedIdentityFileContents::try_load_from(&loaded_identities)?;
                match identity_name {
                    Some(filter_name) => {
                        info!("Filtering for identity named {filter_name}");
                        match parsed_identities.inner_map().get(filter_name.as_ref()) {
                            Some(found_pair) => {
                                if output_finalised {
                                    let to_produce = &found_pair.1;
                                    output_format.print_out_single(to_produce, &mut stdout)?;
                                } else {
                                    let to_produce = &found_pair.0;
                                    output_format.print_out_single(to_produce, &mut stdout)?;
                                }
                                stdout.flush()?;
                            }
                            None => {
                                error!("No identity of name {filter_name}");
                                bail!("No identity of name {filter_name}")
                            }
                        }
                    }
                    None => {
                        info!("Producing unfiltered list of identities!");
                        for (_id_name, id_parsed) in parsed_identities.inner_map().iter() {
                            if output_finalised {
                                let to_produce = &id_parsed.1;
                                output_format.print_out_single(to_produce, &mut stdout)?;
                            } else {
                                let to_produce = &id_parsed.0;
                                output_format.print_out_single(to_produce, &mut stdout)?;
                            }
                            stdout.flush()?;
                        }
                    }
                }
                Ok(())
            }
            Identity::NewIdentity {
                name,
                force,
                no_edit,
                special_creation_args:
                    IdentityFileModificationArgs {
                        with_full_name,
                        with_email,
                        with_gpg_identifier,
                        with_ssh_identifier,
                    },
            } => {
                if let Some(loc) = full_identity_locations.get(&name) {
                    if !force {
                        error!(
                            "Cannot create identity {name}@{} because it already exists. If you \
                             want to do it anyway, use --force",
                            loc
                        );
                        bail!("Identity {name} already exists on filesystem at {}", loc)
                    } else {
                        warn!(
                            "Identity {name}@{} already exists, overwriting because of --force",
                            loc
                        );
                    }
                };
                let target_output_path = IdentityLocation::build_from_name(
                    &bare_config.resolved_identity_directory_path()?,
                    name.as_ref(),
                );
                info!("Generating identity {name}@{target_output_path}");
                let identity_obj = crate::identity::Identity {
                    core_identity: PrimitiveIdentity {
                        name: name.as_ref(),
                        full_name: Some(
                            with_full_name.as_deref().unwrap_or(
                                "<Full Name Here - Default if deleted is just the name>",
                            ),
                        ),
                        email: Some(with_email.as_deref().unwrap_or(
                            "<Email Here - while you can have an identity without an email, it \
                             will severely limit it's capabilities (in particular around git)>",
                        )),
                        leakmatchers: vec![Cow::Borrowed(
                            "Extra phrases that may cause identity leakage can go here for any \
                             automatic checks. Stuff like name, full name, email, and email \
                             components are automatically chunked out and added here",
                        )],
                        no_default_gpg_identity: false,
                    },
                    gpg: Some(GpgIdentity {
                        key_identifier: Some(with_gpg_identifier.as_deref().unwrap_or(
                            "<GPG key identifier here - by default this will be your email, but \
                             if you have an actual GPG key hash or keygrip that is always better \
                             and more precise>",
                        )),
                    }),
                    ssh: with_ssh_identifier
                        .as_ref()
                        .map(|inner_ssh_id| SshIdentity { key: inner_ssh_id }),
                    x509: None,
                    git: Default::default(),
                    custom_env: Default::default(),
                };
                let resulting_file_out = toml::to_string_pretty(&identity_obj)?;
                info!("Generated new file contents for identity {name}@{target_output_path}");
                if no_edit {
                    info!(
                        "Editing identity {name}@{target_output_path} - terminating the editor \
                         with an error will prevent generation of the new identity"
                    );
                };
                shellenv::editor::post_write_edited_file(
                    |w| w.write_all(resulting_file_out.as_str().as_ref()),
                    target_output_path.location_ref(),
                    !no_edit,
                )?;
                Ok(())
            }
            Identity::EditIdentity { name } => {
                if let Some(location) = full_identity_locations.get(&name) {
                    info!("Editing identity {name}@{location} with {}", editor_cmd());
                    simply_edit(location.location_ref()).map_err(Into::into)
                } else {
                    error!(
                        "Identity {name} is not defined - consider the `identity new` command \
                         instead."
                    );
                    bail!("Tried to edit identity {name}, which does not exist")
                }
            }
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
