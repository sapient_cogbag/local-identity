//! Core module for the daemon command.

use crate::state_modules;

#[derive(clap::Args)]
#[clap(name = "daemon")]
/// The `local-identity` daemon should run one instance per unix user.
///
/// It maintains and manages the following:
///  * Generating environment variable definition files in temporary paths, to be executed by users
///    changing the identity used
///  * Detecting
///
///
/// Fundamentally this runs the part of the program that maintains coherence between usages, holds
/// temporary directories for automatic destruction, etc.
pub struct LocalIdentityDaemonCmd {}

pub struct RunningLocalIdentityDaemon {}

impl super::CmdExec for LocalIdentityDaemonCmd {
    type Error = anyhow::Error;

    fn exec(self) -> Result<(), Self::Error> {
        todo!();
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
