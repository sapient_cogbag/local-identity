//! Path-oriented utilities.

/// Extension trait that converts an &str directly into an &Path
pub trait StrPathExt {
    /// Convert an [`&str`] directly into a [`std::path::Path`]
    fn directly_as_path(&self) -> &Path;
}

impl StrPathExt for str {
    #[inline]
    fn directly_as_path(&self) -> &Path { Path::new(self) }
}

use crate::dst_newtype;

#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize, JsonSchema, thiserror::Error)]
#[error("Path not relative")]
pub struct NonRelativePath;

dst_newtype! {
    /// A path that is relative.
    pub RelativePathBuf(pub) from std::path::PathBuf;
    /// A path that is relative (unowned) ^.^
    pub RelativePath(pub) from std::path::Path;
    reffn pub from_raw_path;
    //mutfn pub from_raw_path_mut;
    owned_only_derives Clone, Default;
    serde serialize deserialize;
    json_schema yes;
    validator pub is_relative_path = |inner: &'v DST| -> Option<NonRelativePath> {
        if inner.is_relative() { None } else { Some(NonRelativePath)}
    };
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::path::Path;
