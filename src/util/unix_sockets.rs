//! Utilities for working with unix stream sockets, creating servers and clients for them, and
//! creating a nice packet message system on top of them via https://docs.serde.rs/serde_json/struct.StreamDeserializer.html

use std::{
    net::Shutdown,
    os::unix::net::{UnixListener, UnixStream},
};

use serde::{Deserialize, Serialize};

use super::functions::non_at_fns::NatFnMut;

/// Simple error in the outer packet protocol
#[derive(serde::Serialize, serde::Deserialize, thiserror::Error, Debug, Clone)]
pub enum ProtocolError<'a> {
    #[error(
        "Conflicting version - sending instance has version: {sender_version}, receiving instance \
         has version: {receiver_version}"
    )]
    VersionConflict {
        sender_version: &'a str,
        receiver_version: &'a str,
    },
}

/// Full protocol message, embedding another message within it :), from the client to the
/// server.
///
/// This uses some hacks, see https://serde.rs/attr-bound.html
///
/// Note that the embedded string in most of the enums is the version string for the sender.
#[derive(serde::Serialize, serde::Deserialize)]
pub enum FullProtocolMessageC2S<'a, ClientMessage, ClientError> {
    /// No more messages will come from the client - after this is sendt, the connection can be
    /// assumed to be fully shutdown and any waiting operations forcefully terminated on the server
    /// end via its own Shutdown or otherwise dropping of socket, if necessary.
    Done(&'a str),
    /// Error on the outer protocol level was discovered in the opposite end of the connection.
    ///
    /// After one end of the connection sends this, it should close the connection. If this has not
    /// happened, the other end can forcefully close it too.
    /// See [`PacketError`]
    ProtocolError(#[serde(borrow)] ProtocolError<'a>),
    /// Inner message
    /// Bound ensures that for all 'de ('de: 'a), we implement deserialize
    #[serde(bound(deserialize = "ClientMessage: serde::Deserialize<'de>"))]
    ClientMessage(&'a str, ClientMessage),
    /// Inner error message ^.^
    #[serde(bound(deserialize = "ClientError: serde::Deserialize<'de>"))]
    ClientError(&'a str, ClientError),
}

/// Full protocol message, embedding another message within it :), from the server to the
/// client.
///
/// This uses some hacks, see https://serde.rs/attr-bound.html
///
/// Note that the embedded string in most of the variants is the version.
#[derive(serde::Serialize, serde::Deserialize)]
pub enum FullProtocolMessageS2C<'a, ServerMessage, ServerError> {
    /// No more messages will come from the server - after this is sent, the connection should be
    /// presumed shutdown and can be forcefully shutdown if not..
    Done(&'a str),
    /// Error on the outer protocol level was discovered in the opposite end of the connection.
    ///
    /// After one end of the connection sends this, it should close the connection. If this has not
    /// happened, the other end can forcefully close it too.
    /// See [`PacketError`]
    ProtocolError(#[serde(borrow)] ProtocolError<'a>),
    /// Inner message
    /// Bound ensures that for all 'de ('de: 'a), we implement deserialize
    #[serde(bound(deserialize = "ServerMessage: serde::Deserialize<'de>"))]
    ServerMessage(&'a str, ServerMessage),
    /// Inner error message ^.^
    #[serde(bound(deserialize = "ServerError: serde::Deserialize<'de>"))]
    ServerError(&'a str, ServerError),
}

/// We end up needing GATs because the innards are nasty and have lifetime dependence :/
/// Wish we had real streaming iterators :(
pub trait Ctr {
    type Ty<'a>: ?Sized;
}

/*
pub trait SizedCtr : Ctr where for<'a> Self::Ty<'a>: Sized {}

// This don't work booooooo nya

impl <T: Ctr> SizedCtr for T where for<'a> T::Ty<'a>: Sized {}
*/

/// Module for combining constructors.
pub mod ctr_combinators {
    use crate::util::protocol::WithLt;

    use super::Ctr;

    impl<T: Ctr, E: Ctr> Ctr for Result<T, E>
    where
        for<'a> T::Ty<'a>: Sized,
        for<'a> E::Ty<'a>: Sized,
    {
        type Ty<'a> = Result<T::Ty<'a>, E::Ty<'a>>;
    }

    impl<T: Ctr> Ctr for Option<T>
    where
        for<'a> T::Ty<'a>: Sized,
    {
        type Ty<'a> = Option<T::Ty<'a>>;
    }

    impl<T: Ctr> Ctr for [T]
    where
        for<'a> WithLt<'a, T>: Sized,
    {
        type Ty<'a> = [T::Ty<'a>];
    }
}

/// Indicates the action to perform by a client or server, as returned by a reactor.
pub enum NextMessage<Message, Error, FinalValue> {
    /// Terminate the connection, returning the final value from the function - this sends a
    /// connection termination signal to the other end of the connection, as well as shutting down
    /// the write and read part of the socket.
    Done(FinalValue),
    /// Pass a message along to the other end of the connection.
    Message(Message),
    /// Indicate an error of some kind to the other end of the connection
    Error(Error),
}

/// Check that versions match between a sender and receiver packet, where the sender is the
/// client.
///
/// If they do not, produce Some final value, and try and send a protocol error packet on the
/// stream. First argument is the version of the packet sender (client), second argument is the version of
/// ourselves.
fn version_check_on_server<S2CMC: Ctr, S2CEC: Ctr, FinalVal>(
    sender_version_string: &str,
    our_version_string: &str,
    socket_to_write: &UnixStream,
    mut final_value_generator: impl FnMut(&str, &str) -> FinalVal,
) -> std::io::Result<Option<FinalVal>>
where
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullS2CMessage<'a, S2CMC, S2CEC> =
        FullProtocolMessageS2C<'a, <S2CMC as Ctr>::Ty<'a>, <S2CEC as Ctr>::Ty<'a>>;

    if sender_version_string != our_version_string {
        let res = Some(final_value_generator(
            sender_version_string,
            our_version_string,
        ));
        serde_json::ser::to_writer(
            socket_to_write,
            &FullS2CMessage::<'_, S2CMC, S2CEC>::ProtocolError(ProtocolError::VersionConflict {
                sender_version: sender_version_string,
                receiver_version: our_version_string,
            }),
        )?;
        socket_to_write.shutdown(Shutdown::Both)?;
        Ok(res)
    } else {
        Ok(None)
    }
}
/// Check that versions match between a sender and receiver packet, where the sender is the
/// server.
///
/// If they do not, produce Some final value, and try and send a protocol error packet on the
/// stream. First argument is the version of the packet sender (server), second argument is the
/// version of ourselves
fn version_check_on_client<C2SMC: Ctr, C2SEC: Ctr, FinalVal>(
    sender_version_string: &str,
    our_version_string: &str,
    socket_to_write: &UnixStream,
    mut final_value_generator: impl FnMut(&str, &str) -> FinalVal,
) -> std::io::Result<Option<FinalVal>>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullC2SMessage<'a, C2SMC, C2SEC> =
        FullProtocolMessageC2S<'a, <C2SMC as Ctr>::Ty<'a>, <C2SEC as Ctr>::Ty<'a>>;

    if sender_version_string != our_version_string {
        let res = Some(final_value_generator(
            sender_version_string,
            our_version_string,
        ));
        serde_json::ser::to_writer(
            socket_to_write,
            &FullC2SMessage::<'_, C2SMC, C2SEC>::ProtocolError(ProtocolError::VersionConflict {
                sender_version: sender_version_string,
                receiver_version: our_version_string,
            }),
        )?;
        socket_to_write.shutdown(Shutdown::Both)?;
        Ok(res)
    } else {
        Ok(None)
    }
}

/// Attempt to perform the given next message command on the server, sending to the appropriate
/// unix stream, and returning either an io error (as it may be), or Some(FinalVal) for terminating
/// the server session, or None, to keep listening for more packets.
fn maybe_send_next_message_on_server<'v, S2CMC: Ctr, S2CEC: Ctr, FinalVal>(
    next_message_to_send: NextMessage<S2CMC::Ty<'v>, S2CEC::Ty<'v>, FinalVal>,
    on_socket: &UnixStream,
    our_version_string: &'v str,
) -> std::io::Result<Option<FinalVal>>
where
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullS2CMessage<'a, SMC, SEC> =
        FullProtocolMessageS2C<'a, <SMC as Ctr>::Ty<'a>, <SEC as Ctr>::Ty<'a>>;
    match next_message_to_send {
        NextMessage::Done(v) => {
            // Send done message, shutdown, then return.
            serde_json::ser::to_writer(
                on_socket,
                &FullS2CMessage::<'_, S2CMC, S2CEC>::Done(our_version_string),
            )?;
            on_socket.shutdown(Shutdown::Both)?;
            Ok(Some(v))
        }
        NextMessage::Message(out_msg) => {
            serde_json::ser::to_writer(
                on_socket,
                &FullS2CMessage::<'_, S2CMC, S2CEC>::ServerMessage(our_version_string, out_msg),
            )?;
            Ok(None)
        }
        NextMessage::Error(out_err) => {
            serde_json::ser::to_writer(
                on_socket,
                &FullS2CMessage::<'_, S2CMC, S2CEC>::ServerError(our_version_string, out_err),
            )?;
            Ok(None)
        }
    }
}

/// Attempt to perform the given next message command on the client, sending to the
/// unix stream, and returning either an io error (as it may be), or Some(FinalVal) for terminating
/// the client session, or None, to keep listening for more packets.
fn maybe_send_next_message_on_client<'v, C2SMC: Ctr, C2SEC: Ctr, FinalVal>(
    next_message_to_send: NextMessage<C2SMC::Ty<'v>, C2SEC::Ty<'v>, FinalVal>,
    on_socket: &UnixStream,
    our_version_string: &'v str,
) -> std::io::Result<Option<FinalVal>>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullC2SMessage<'a, SMC, SEC> =
        FullProtocolMessageC2S<'a, <SMC as Ctr>::Ty<'a>, <SEC as Ctr>::Ty<'a>>;
    match next_message_to_send {
        NextMessage::Done(v) => {
            // Send done message, shutdown, then return.
            serde_json::ser::to_writer(
                on_socket,
                &FullC2SMessage::<'_, C2SMC, C2SEC>::Done(our_version_string),
            )?;
            on_socket.shutdown(Shutdown::Both)?;
            Ok(Some(v))
        }
        NextMessage::Message(out_msg) => {
            serde_json::ser::to_writer(
                on_socket,
                &FullC2SMessage::<'_, C2SMC, C2SEC>::ClientMessage(our_version_string, out_msg),
            )?;
            Ok(None)
        }
        NextMessage::Error(out_err) => {
            serde_json::ser::to_writer(
                on_socket,
                &FullC2SMessage::<'_, C2SMC, C2SEC>::ClientError(our_version_string, out_err),
            )?;
            Ok(None)
        }
    }
}

enum Either<A, B> {
    A(A),
    B(B),
}

/// Take a raw, parsed input message from a client and turn it into one of two things, in the
/// process acting to early-terminate the socket connection if necessary.
///
/// In the case without an early return, this also re-provides the input single-use functions.
fn process_raw_protocol_message_input_on_server<
    'v,
    S2CMC: Ctr,
    S2CEC: Ctr,
    C2SMC: Ctr,
    C2SEC: Ctr,
    FinalVal,
    OEDF: for<'a> FnOnce() -> FinalVal,
    OEPEF: for<'a> FnOnce(ProtocolError<'a>) -> FinalVal,
>(
    raw_received_c2s_message: FullProtocolMessageC2S<'v, C2SMC::Ty<'v>, C2SEC::Ty<'v>>,
    socket: &UnixStream,
    on_received_message: &mut impl for<'a> NatFnMut<
        (C2SMC::Ty<'a>,),
        NextMessage<S2CMC::Ty<'a>, S2CEC::Ty<'a>, FinalVal>,
    >,
    on_received_error: &mut impl for<'a> NatFnMut<
        (C2SEC::Ty<'a>,),
        NextMessage<S2CMC::Ty<'a>, S2CEC::Ty<'a>, FinalVal>,
    >,
    on_other_end_done: OEDF,
    on_other_end_claimed_protocol_error: OEPEF,
    mut on_version_mismatch: impl FnMut(&str, &str) -> FinalVal,
    our_version: &str,
) -> std::io::Result<
    Either<
        FinalVal,
        (
            NextMessage<S2CMC::Ty<'v>, S2CEC::Ty<'v>, FinalVal>,
            OEDF,
            OEPEF,
        ),
    >,
>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    match raw_received_c2s_message {
        FullProtocolMessageC2S::Done(_v) => Ok(Either::A(on_other_end_done())),
        // As per specification, upon receiving this, both ends of the connection should be
        // shut down :)
        FullProtocolMessageC2S::ProtocolError(e) => {
            Ok(Either::A(on_other_end_claimed_protocol_error(e)))
        }
        FullProtocolMessageC2S::ClientMessage(client_version, inner_msg) => {
            match version_check_on_server::<S2CMC, S2CEC, _>(
                client_version,
                our_version,
                socket,
                &mut on_version_mismatch,
            )? {
                Some(version_mismatch_final_val) => Ok(Either::A(version_mismatch_final_val)),
                None => Ok(Either::B((
                    on_received_message.call_mut((inner_msg,)),
                    on_other_end_done,
                    on_other_end_claimed_protocol_error,
                ))),
            }
        }
        FullProtocolMessageC2S::ClientError(client_version, inner_msg) => {
            match version_check_on_server::<S2CMC, S2CEC, _>(
                client_version,
                our_version,
                socket,
                &mut on_version_mismatch,
            )? {
                Some(version_mismatch_final_val) => Ok(Either::A(version_mismatch_final_val)),
                None => Ok(Either::B((
                    on_received_error.call_mut((inner_msg,)),
                    on_other_end_done,
                    on_other_end_claimed_protocol_error,
                ))),
            }
        }
    }
}

/// Take a raw, parsed input message from a server and turn it into one of two things, in the
/// process acting to early-terminate the socket connection if necessary.
///
/// In the case without an early return, this also re-provides the input single-use functions.
fn process_raw_protocol_message_input_on_client<
    'v,
    S2CMC: Ctr,
    S2CEC: Ctr,
    C2SMC: Ctr,
    C2SEC: Ctr,
    FinalVal,
    OEDF: for<'a> FnOnce() -> FinalVal,
    OEPEF: for<'a> FnOnce(ProtocolError<'a>) -> FinalVal,
>(
    raw_received_s2c_message: FullProtocolMessageS2C<'v, S2CMC::Ty<'v>, S2CEC::Ty<'v>>,
    socket: &UnixStream,
    on_received_message: &mut impl for<'a> NatFnMut<
        (S2CMC::Ty<'a>,),
        NextMessage<C2SMC::Ty<'a>, C2SEC::Ty<'a>, FinalVal>,
    >,
    on_received_error: &mut impl for<'a> NatFnMut<
        (S2CEC::Ty<'a>,),
        NextMessage<C2SMC::Ty<'a>, C2SEC::Ty<'a>, FinalVal>,
    >,
    on_other_end_done: OEDF,
    on_other_end_claimed_protocol_error: OEPEF,
    mut on_version_mismatch: impl FnMut(&str, &str) -> FinalVal,
    our_version: &str,
) -> std::io::Result<
    Either<
        FinalVal,
        (
            NextMessage<C2SMC::Ty<'v>, C2SEC::Ty<'v>, FinalVal>,
            OEDF,
            OEPEF,
        ),
    >,
>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    match raw_received_s2c_message {
        FullProtocolMessageS2C::Done(_v) => Ok(Either::A(on_other_end_done())),
        // As per specification, upon receiving this, both ends of the connection should be
        // shut down :)
        FullProtocolMessageS2C::ProtocolError(e) => {
            Ok(Either::A(on_other_end_claimed_protocol_error(e)))
        }
        FullProtocolMessageS2C::ServerMessage(server_version, inner_msg) => {
            match version_check_on_client::<C2SMC, C2SEC, _>(
                server_version,
                our_version,
                socket,
                &mut on_version_mismatch,
            )? {
                Some(version_mismatch_final_val) => Ok(Either::A(version_mismatch_final_val)),
                None => Ok(Either::B((
                    on_received_message.call_mut((inner_msg,)),
                    on_other_end_done,
                    on_other_end_claimed_protocol_error,
                ))),
            }
        }
        FullProtocolMessageS2C::ServerError(server_version, inner_msg) => {
            match version_check_on_client::<C2SMC, C2SEC, _>(
                server_version,
                our_version,
                socket,
                &mut on_version_mismatch,
            )? {
                Some(version_mismatch_final_val) => Ok(Either::A(version_mismatch_final_val)),
                None => Ok(Either::B((
                    on_received_error.call_mut((inner_msg,)),
                    on_other_end_done,
                    on_other_end_claimed_protocol_error,
                ))),
            }
        }
    }
}

pub fn client_sync_session<C2SMC: Ctr, C2SEC: Ctr, S2CMC: Ctr, S2CEC: Ctr, FinalVal>(
    version: &str,
    mut on_server_message: impl for<'a> NatFnMut<
        (S2CMC::Ty<'a>,),
        NextMessage<C2SMC::Ty<'a>, C2SEC::Ty<'a>, FinalVal>,
    >,
    mut on_server_error: impl for<'a> NatFnMut<
        (S2CEC::Ty<'a>,),
        NextMessage<C2SMC::Ty<'a>, C2SEC::Ty<'a>, FinalVal>,
    >,
    mut on_server_terminates_connection: impl for<'a> FnOnce() -> FinalVal,
    mut on_server_protocol_error: impl for<'a> FnOnce(ProtocolError<'a>) -> FinalVal,
    mut on_version_mismatch: impl FnMut(&str, &str) -> FinalVal,
    unix_str: UnixStream,
) -> std::io::Result<FinalVal>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullS2CMessage<'a, SMC, SEC> =
        FullProtocolMessageS2C<'a, <SMC as Ctr>::Ty<'a>, <SEC as Ctr>::Ty<'a>>;

    let reading_ref = &unix_str;
    let writing_ref = &unix_str;

    let streaming_deser =
        serde_json::StreamDeserializer::<_, FullS2CMessage<'_, S2CMC, S2CEC>>::new(
            serde_json::de::IoRead::new(reading_ref),
        );

    for inner_message in streaming_deser {
        let inner_message = inner_message?;
        let raw_to_send: NextMessage<_, _, _> = match process_raw_protocol_message_input_on_client::<
            S2CMC,
            S2CEC,
            C2SMC,
            C2SEC,
            FinalVal,
            _,
            _,
        >(
            inner_message,
            writing_ref,
            &mut on_server_message,
            &mut on_server_error,
            on_server_terminates_connection,
            on_server_protocol_error,
            &mut on_version_mismatch,
            version,
        )? {
            Either::A(early_ret) => return Ok(early_ret),
            Either::B((v, oedf, oepef)) => {
                on_server_terminates_connection = oedf;
                on_server_protocol_error = oepef;
                v
            }
        };
        match maybe_send_next_message_on_client::<C2SMC, C2SEC, FinalVal>(
            raw_to_send,
            writing_ref,
            version,
        )? {
            Some(v) => return Ok(v),
            None => continue,
        }
    }
    unreachable!("The loop either explicitly continues or it returns an Ok() or Err() ")
}

/// Function that can run on a series of messages from a [UnixStream] and reply to them.
///
/// on_client_protocol_error is called when the client sends a message to us saying our version
/// does not match its version.
///
/// on_version_mismatch is called when we ourselves detect a mismatch of versions, and returns the
/// appropriate value for this function, just before we go to send a protocol error notification
/// across the connection and shutdown. First argument is client version, second argument is server
/// version.
pub fn server_reactor_sync_session<C2SMC: Ctr, C2SEC: Ctr, S2CMC: Ctr, S2CEC: Ctr, FinalVal>(
    version: &str,
    mut on_client_message: impl for<'a> NatFnMut<
        (C2SMC::Ty<'a>,),
        NextMessage<S2CMC::Ty<'a>, S2CEC::Ty<'a>, FinalVal>,
    >,
    mut on_client_error: impl for<'a> NatFnMut<
        (C2SEC::Ty<'a>,),
        NextMessage<S2CMC::Ty<'a>, S2CEC::Ty<'a>, FinalVal>,
    >,
    mut on_client_terminates_connection: impl for<'a> FnOnce() -> FinalVal,
    mut on_client_protocol_error: impl for<'a> FnOnce(ProtocolError<'a>) -> FinalVal,
    mut on_version_mismatch: impl FnMut(&str, &str) -> FinalVal,
    unix_str: UnixStream,
) -> std::io::Result<FinalVal>
where
    for<'de> C2SMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> C2SEC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CMC::Ty<'de>: Deserialize<'de> + Serialize,
    for<'de> S2CEC::Ty<'de>: Deserialize<'de> + Serialize,
{
    type FullC2SMessage<'a, CMC, CEC> =
        FullProtocolMessageC2S<'a, <CMC as Ctr>::Ty<'a>, <CEC as Ctr>::Ty<'a>>;

    let reading_ref = &unix_str;
    let writing_ref = &unix_str;

    let streaming_deser =
        serde_json::StreamDeserializer::<_, FullC2SMessage<'_, C2SMC, C2SEC>>::new(
            serde_json::de::IoRead::new(reading_ref),
        );

    for inner_message in streaming_deser {
        let inner_message = inner_message?;
        let raw_to_send: NextMessage<_, _, _> = match process_raw_protocol_message_input_on_server(
            inner_message,
            writing_ref,
            &mut on_client_message,
            &mut on_client_error,
            on_client_terminates_connection,
            on_client_protocol_error,
            &mut on_version_mismatch,
            version,
        )? {
            Either::A(early_ret) => return Ok(early_ret),
            Either::B((v, oedf, oepef)) => {
                on_client_terminates_connection = oedf;
                on_client_protocol_error = oepef;
                v
            }
        };
        match maybe_send_next_message_on_server::<S2CMC, S2CEC, FinalVal>(
            raw_to_send,
            writing_ref,
            version,
        )? {
            Some(v) => return Ok(v),
            None => continue,
        }
    }
    unreachable!("The loop either explicitly continues or it returns an Ok() or Err() ")
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
