//! HORRIBLE, HORRIBLE, EVIL HACKS FOR TTYs just to GET INLINE EDITORS FUCKING WORKING
//!
//! See [HERE](https://fasterthanli.me/articles/a-terminal-case-of-linux#oh-good-channels) on
//! FasterThanLime's website for what i'm doing with channels and threads and why it makes me want to
//! cry because JESUS FUCKING CHRIST TTYS!
//!
//! Slightly modified of course but just why
//!
//! As for licensing in this file, I'm not entirely sure since I've seen other blog posts that use
//! the stuff. But I will do my best to credit Faster Than Lime with the technical details, and a
//! lot of it is derived from GPL licensed corelib stuff. So I'm guessing probably GPLv3+... I
//! think.

use libc;
use std::{
    io::{stderr, stdin, stdout},
    os::{
        raw::c_int,
        unix::prelude::{AsRawFd, CommandExt},
    },
    process::{Command, ExitStatus, Stdio},
};

#[derive(Debug, thiserror::Error)]
pub enum PtyError {
    #[error("Error produced in final child.wait when waiting for process: {0}")]
    ProcessWaitingError(#[source] std::io::Error),
    #[error("Tried to start tty subprocess when we arent even in a tty!")]
    NotATTYErr,
}

/// Test if the file descriptor is both valid and referring to a tty
pub fn isatty(rawfd: &impl AsRawFd) -> bool { unsafe { libc::isatty(rawfd.as_raw_fd()) != 0 } }

/// Run a command while forwarding our tty-ness ;p
///
/// Note that if not being run from a tty this returns an error,
///
/// This is something that will hijack input if you aren't careful!
///
/// This assumes that if stdin/out/err are ttys, they are the *same* ttys ^.^
pub fn make_tty_child_and_forward(
    mut bare_command_object: Command,
) -> Result<std::process::ExitStatus, PtyError> {
    if !(isatty(&stdin()) && isatty(&stdout()) && isatty(&stderr())) {
        return Err(PtyError::NotATTYErr);
    }
    // Try piping io?
    let mut child = bare_command_object
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()
        .map_err(PtyError::ProcessWaitingError)?;
    child.wait().map_err(PtyError::ProcessWaitingError)
}
