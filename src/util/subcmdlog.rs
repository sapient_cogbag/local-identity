//! Module for logging subcommand output in a separate collection of threads ^.^

use crossbeam_utils::thread as cb_thread;

/// Defines how to log process output on a line by line basis for a subprocess.
pub struct ProcessLogMode {
    /// Process a line of stderr output. Argument is always one-line only.
    pub process_stderr_line: Box<dyn FnMut(&str) + 'static + Send>,
    /// Process a line of stdout output. Argument is always one-line only.
    pub process_stdout_line: Box<dyn FnMut(&str) + 'static + Send>,
    /// The child stderr for the process to forward logging for.
    pub child_stderr: Option<ChildStderr>,
    /// The child stdout for the process to forward logging to
    pub child_stdout: Option<ChildStdout>,
}

/// Receiving end of the logpool.
pub struct LogPoolRecv {
    inner_channel: std::sync::mpsc::Receiver<ProcessLogMode>,
}

impl LogPoolRecv {
    /// Run the receiving/logging threads in the given scope
    ///
    /// This consumes ourselves, as [LogPoolRecv] is based on [std::sync::mpsc::Receiver].
    ///
    /// It continues until no more sender channels exist and all the stdout/stderrs have been
    /// emptied, because it uses scoped threads.
    pub fn run_receive_threading<'env, 'scope>(
        self,
        s: &'scope cb_thread::Scope<'env>,
    ) -> cb_thread::ScopedJoinHandle<'scope, ()> {
        s.spawn(move |s| {
            while let Ok(mut next_subproc) = self.inner_channel.recv() {
                let bufstdout = next_subproc.child_stdout.take().map(BufReader::new);
                let bufstderr = next_subproc.child_stderr.take().map(BufReader::new);

                // Closure that produces another closure ;p nya
                fn fn_proc(
                    mut line_processor: Box<dyn FnMut(&str) + 'static + Send>,
                    mut buffered_reader: impl BufRead,
                ) -> impl FnOnce(&cb_thread::Scope<'_>) {
                    // Inner thread closure that reads lines and processes them.
                    move |_| {
                        // Read lines from stdout nya
                        let mut line_buf = String::with_capacity(500);
                        while let Ok(read_count) = buffered_reader.read_line(&mut line_buf) {
                            match read_count {
                                0 => break,
                                _ => {
                                    (line_processor)(&line_buf);
                                    line_buf.clear();
                                }
                            }
                        }
                    }
                }

                match bufstdout {
                    Some(has_stdout) => {
                        s.spawn(fn_proc(next_subproc.process_stdout_line, has_stdout));
                    }
                    None => {}
                };

                match bufstderr {
                    Some(has_stderr) => {
                        s.spawn(fn_proc(next_subproc.process_stderr_line, has_stderr));
                    }
                    None => {}
                };
            }
        })
    }
}

#[derive(Clone)]
/// Sender for child logging.
pub struct LogPoolSend {
    inner_channel: std::sync::mpsc::SyncSender<ProcessLogMode>,
}

impl LogPoolSend {
    /// Set up stdout and stderr for a [std::process::Command] to log to this, then attempt to
    /// start it with .spawn(), returning the child process or some error. Arguments are the
    /// logging pool send channel, the child command to then run, and stdout/stderr line-by-line
    /// processing.
    ///
    /// Returns either a successful handle to [std::process::Child] with stdout and stderr removed
    /// and processed by the logger thread pool, or an error of one kind or another.
    pub fn spawn_and_process_stdouterr_lines(
        &mut self,
        child_command: &mut std::process::Command,
        stdout_line_processor: impl FnMut(&str) + Send + 'static,
        stderr_line_processor: impl FnMut(&str) + Send + 'static,
    ) -> std::io::Result<std::process::Child> {
        let mut child_handle = child_command
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;
        let stdout = child_handle.stdout.take();
        let stderr = child_handle.stderr.take();
        self.inner_channel
            .send(ProcessLogMode {
                process_stderr_line: Box::new(stderr_line_processor),
                process_stdout_line: Box::new(stdout_line_processor),
                child_stderr: stderr,
                child_stdout: stdout,
            })
            .expect(
                "LogPoolRecv waits until all senders are dropped. In theory anyway. So this \
                 shouldn't crash because there should always be an endpount",
            );
        Ok(child_handle)
    }
}

/// Extension trait for [std::process::Command] for starting subcommands and then processing all
/// the output from them, line by line, through a pair of functions.
pub trait CommandLogPoolExt {
    /// Essentially a clone of [LogPoolSend::spawn_and_process_stdouterr_lines]
    fn spawn_and_line_process(
        &mut self,
        with_logsender: &mut LogPoolSend,
        stdout_line_processor: impl FnMut(&str) + Send + 'static,
        stderr_line_processor: impl FnMut(&str) + Send + 'static,
    ) -> std::io::Result<std::process::Child>;

    /// Like [spawn_and_line_process] but also waits for the process to terminate.
    fn spawn_and_wait(
        &mut self,
        with_logsender: &mut LogPoolSend,
        stdout_line_processor: impl FnMut(&str) + Send + 'static,
        stderr_line_processor: impl FnMut(&str) + Send + 'static,
    ) -> std::io::Result<std::process::ExitStatus> {
        self.spawn_and_line_process(with_logsender, stdout_line_processor, stderr_line_processor)?
            .wait()
    }
}

impl CommandLogPoolExt for Command {
    fn spawn_and_line_process(
        &mut self,
        with_logsender: &mut LogPoolSend,
        stdout_line_processor: impl FnMut(&str) + Send + 'static,
        stderr_line_processor: impl FnMut(&str) + Send + 'static,
    ) -> std::io::Result<std::process::Child> {
        with_logsender.spawn_and_process_stdouterr_lines(
            self,
            stdout_line_processor,
            stderr_line_processor,
        )
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    io::{BufRead, BufReader},
    process::{ChildStderr, ChildStdout, Command, Stdio},
};
