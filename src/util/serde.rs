//! [`serde`] utilities.

/// Macro for adding postvalidation for deserializable items, add `#[serde(remote = "Self")]`
///
/// See [here](https://stackoverflow.com/a/70293354) for an explanation on how this works.
///
/// `$for_type` is the type to do this for.
/// `$validator` for ($this, $D) is a block that must produce Option<D::Error> where:
///  * `$D` is the deserializer type param
///  * `$this` is the unvalidated result of the type
///  None means validation passed successfully
///  Some(err) means validation failed. Note that it is always possible to do
///  `$D::Error::custom("<some string here>")` for special error messages.
///
/// Note that without special handling, this would *break* a #[derive(Serialize)] because remotes
/// are more meant to be used for types that have no serde implementations in either serde or the
/// originating crate, so by default it eats both Serialize and Deserialize with #[remote].
///
/// We get around this by implementing Serialize ourselves with basic forwarding.
#[macro_export]
macro_rules! with_deserialization_validation {
    ($for_type:ident $(:<$fst_lt:lifetime $(, $lt:lifetime)*>)?, |$this:ident|<$D:ident> $validator:block) => {
        impl<'de $(:$fst_lt $(+ $lt)*)? $(, $fst_lt $(, $lt)*)?> ::serde::Deserialize<'de>
            for $for_type $(<$fst_lt $(, $lt)*>)?{
            fn deserialize<$D: ::serde::Deserializer<'de>>(
                deserializer: $D,
            ) -> Result<Self, $D::Error> {
                let $this = Self::deserialize(deserializer)?;
                use ::serde::de::Error;
                let validator_res = $validator;
                match validator_res {
                    None => Ok($this),
                    Some(e) => Err(e),
                }
            }
        }

        impl $(<$fst_lt $(, $lt)*>)? ::serde::Serialize for $for_type $(<$fst_lt $(, $lt)*>)? {
            fn serialize<S: ::serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
                Self::serialize(self, serializer)
            }
        }
    };
}

/// Literally just a method to ensure that we do not use inherent methods on a type, and instead
/// always use [`serde::Deserialize::deserialize`].
pub fn deserialize<'de, T: Deserialize<'de>, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<T, D::Error> {
    <T as Deserialize<'de>>::deserialize(deserializer)
}

use serde::{Deserialize, Deserializer};

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
