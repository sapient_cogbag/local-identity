//! Simple module for more precise and failure-tolerant/descriptive XDG stuff.
use std::{
    fmt::Display,
    path::{Path, PathBuf},
};

use log::warn;
use schemars::JsonSchema;
use serde::Serialize;
use thiserror;

/// Simple directory stuffs for the purposes of including $HOME
#[derive(Debug)]
pub struct XdgDirsWithHome {
    home: Option<PathBuf>,
    raw_dirs: Result<xdg::BaseDirectories, XdgError>,
}

#[derive(Clone, Serialize, JsonSchema)]
/// Printable information of found directory stuff.
#[serde(rename_all = "kebab-case")]
pub struct XdgPrintableInfo {
    #[serde(skip_serializing_if = "crate::util::is_default")]
    home: Option<PathBuf>,
    #[serde(skip_serializing_if = "crate::util::is_default")]
    xdg_config_home: Option<PathBuf>,
    #[serde(skip_serializing_if = "crate::util::is_default")]
    xdg_cache_home: Option<PathBuf>,
    #[serde(skip_serializing_if = "crate::util::is_default")]
    xdg_data_home: Option<PathBuf>,
    #[serde(skip_serializing_if = "crate::util::is_default")]
    xdg_state_home: Option<PathBuf>,
}

impl XdgPrintableInfo {
    pub fn new(homedirs: &XdgDirsWithHome) -> Self {
        Self {
            home: homedirs
                .home()
                .map_err(|e| warn!("Error when getting home: {}", e))
                .ok()
                .map(ToOwned::to_owned),
            xdg_config_home: homedirs
                .xdg_config_home()
                .map_err(|e| warn!("Error getting XDG config home: {}", e))
                .ok(),
            xdg_cache_home: homedirs
                .xdg_cache_home()
                .map_err(|e| warn!("Error getting XDG cache home: {}", e))
                .ok(),
            xdg_data_home: homedirs
                .xdg_data_home()
                .map_err(|e| warn!("Error getting XDG data home: {}", e))
                .ok(),
            xdg_state_home: homedirs
                .xdg_state_home()
                .map_err(|e| warn!("Error getting XDG state home: {}", e))
                .ok(),
        }
    }
}

impl Display for XdgPrintableInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut do_write = |name, maybe_data: &Option<PathBuf>| -> std::fmt::Result {
            write!(f, "{name}:")?;
            match maybe_data.as_ref() {
                Some(path) => writeln!(f, "{}", path.display()),
                None => f.write_str("[unobtained]\n"),
            }
        };

        do_write("HOME", &self.home)?;
        do_write("XDG_CONFIG_HOME", &self.xdg_config_home)?;
        do_write("XDG_CACHE_HOME", &self.xdg_cache_home)?;
        do_write("XDG_DATA_HOME", &self.xdg_data_home)?;
        do_write("XDG_STATE_HOME", &self.xdg_state_home)
    }
}

#[derive(Clone, Copy, Debug, thiserror::Error)]
#[error("No home directory")]
pub struct NoHomeError;

#[derive(Debug, thiserror::Error)]
pub enum XdgError {
    #[error("No home directory obtained")]
    NoHome(
        #[source]
        #[from]
        NoHomeError,
    ),
    #[error("Error with xdg directories, but not necessarily missing home")]
    XdgDirectoryError(
        #[source]
        #[from]
        xdg::BaseDirectoriesError,
    ),
}

/// Because of restrictions on [`xdg::BaseDirectoriesError`], we can't easily use getter
/// methods when holding a [`Result<xdg::BaseDirectoriesError, XdgError>`]. Therefore, we cheat
/// by rendering an xdgerror out into a string.
#[derive(Clone, Debug, thiserror::Error)]
pub enum XdgErrorFlat {
    #[error("No home directory obtained")]
    NoHome(
        #[source]
        #[from]
        NoHomeError,
    ),
    #[error("Error with xdg directories, not necessarily missing home, but flattened")]
    XdgDirectoryErrorFlat(String),
}

impl<'a> From<&'a XdgError> for XdgErrorFlat {
    fn from(e: &'a XdgError) -> Self {
        match e {
            XdgError::NoHome(v) => XdgErrorFlat::NoHome(*v),
            XdgError::XdgDirectoryError(e) => XdgErrorFlat::XdgDirectoryErrorFlat(e.to_string()),
        }
    }
}

impl Default for XdgDirsWithHome {
    fn default() -> Self { Self::new() }
}

impl XdgDirsWithHome {
    pub fn new() -> Self {
        let home = dirs::home_dir();
        let xdgs = xdg::BaseDirectories::new();

        Self {
            home,
            raw_dirs: xdgs.map_err(Into::into),
        }
    }

    /// Even if XDG fails for some reason we still have $HOME!
    pub fn home(&self) -> Result<&Path, NoHomeError> {
        self.home.as_ref().map(AsRef::as_ref).ok_or(NoHomeError)
    }

    /// Get the xdg standard config directory.
    pub fn xdg_config_home(&self) -> Result<PathBuf, XdgErrorFlat> {
        self.raw_dirs
            .as_ref()
            .map_err(Into::into)
            .map(|v| v.get_config_home())
    }

    /// Consuming version of [`Self::xdg_config_home`], gets the full error so if you are
    /// determined to fail you can get nicely typed errors... or something.
    pub fn xdg_config_home_consuming(self) -> Result<PathBuf, XdgError> {
        self.raw_dirs.map(|v| v.get_config_home())
    }

    /// Get the xdg standard cache directory.
    pub fn xdg_cache_home(&self) -> Result<PathBuf, XdgErrorFlat> {
        self.raw_dirs
            .as_ref()
            .map_err(Into::into)
            .map(|v| v.get_cache_home())
    }

    /// Consuming version of [`Self::xdg_cache_home`], gets the full error so if you are
    /// determined to fail you can get nicely typed errors... or something.
    pub fn xdg_cache_home_consuming(self) -> Result<PathBuf, XdgError> {
        self.raw_dirs.map(|v| v.get_cache_home())
    }

    /// Get the xdg standard data directory.
    pub fn xdg_data_home(&self) -> Result<PathBuf, XdgErrorFlat> {
        self.raw_dirs
            .as_ref()
            .map_err(Into::into)
            .map(|v| v.get_data_home())
    }

    /// Consuming version of [`Self::xdg_data_home`], gets the full error so if you are
    /// determined to fail you can get nicely typed errors... or something.
    pub fn xdg_data_home_consuming(self) -> Result<PathBuf, XdgError> {
        self.raw_dirs.map(|v| v.get_data_home())
    }

    /// Get the xdg standard state directory.
    pub fn xdg_state_home(&self) -> Result<PathBuf, XdgErrorFlat> {
        self.raw_dirs
            .as_ref()
            .map_err(Into::into)
            .map(|v| v.get_state_home())
    }

    /// Consuming version of [`Self::xdg_state_home`], gets the full error so if you are
    /// determined to fail you can get nicely typed errors... or something.
    pub fn xdg_state_home_consuming(self) -> Result<PathBuf, XdgError> {
        self.raw_dirs.map(|v| v.get_state_home())
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
