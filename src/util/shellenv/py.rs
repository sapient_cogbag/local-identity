//! Utilities for grabbing python environment information.
use std::{
    ffi::OsStr,
    os::unix::prelude::OsStrExt,
    path::{Path, PathBuf},
    process::Stdio,
};

/// The relevant python executable - we always want python3
pub const PYEXEC: &str = "python3";

/// Represents the search path of a python. Components may or may not exist.
#[derive(Clone, Debug)]
pub struct PythonPath {
    paths: Vec<PathBuf>,
}

impl PythonPath {
    /// Unconditionally pulls in a collection of paths as a pythonpath ^.^
    fn raw_make(pypath: Vec<PathBuf>) -> Self { Self { paths: pypath } }

    /// Attempt to find the given file/directory on pythonpath, returning the full path
    /// to the file/directory if it exists. ^.^. This is subject to race conditions with
    /// the filesystem if used to check a file before opening it, but it's good to just
    /// get a path to pass to python to run.
    ///
    /// Note that errors are treated as nonexistence.
    ///
    /// If the path is absolute, this is essentially a pointless method as this internally
    /// uses [std::path::PathBuf::join]
    pub fn find_in_pythonpath(&self, extra_path: &Path) -> Option<PathBuf> {
        for pythonpath_base in &self.paths {
            let full_path = pythonpath_base.join(extra_path);
            if full_path.exists() {
                return Some(full_path);
            }
        }
        None
    }
}

/// Get the python search path.
///
/// Python search paths are also filtered at time of creation for actually existing ^.^
/// (with errors being equated with non-existence).
///
/// Note that this does no caching or anything, so calling it twice **invokes python twice**
pub fn python_path() -> std::io::Result<PythonPath> {
    let python_path_lister_output = std::process::Command::new(PYEXEC)
        .arg("-c")
        .arg(
            r#"""
import sys
for p in sys.path:
    print(p)
"""#,
        )
        // Recreate the default ::piped() fn used by output() without config ^.^
        .stdout(Stdio::piped())
        // Default is piped() for output but we don't want stderr noise in output nya
        .stderr(Stdio::inherit())
        .output()?;
    Ok(PythonPath::raw_make(
        python_path_lister_output
            .stdout
            .split(|raw_u8_chr| *raw_u8_chr == b'\n' || *raw_u8_chr == b'\r')
            .filter(|v| !v.is_empty())
            .map(OsStr::from_bytes)
            .map(PathBuf::from)
            .collect::<Vec<_>>(),
    ))
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
