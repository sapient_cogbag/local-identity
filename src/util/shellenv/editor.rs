//! For opening editors with $EDITOR as an override and various default editors if present on the
//! system, in priority order.
//!
//! Note that this is based on https://docs.rs/editor-input/0.1.2/src/editor_input/lib.rs.html#1-65
//! with added pre-Write/BufWrite API.
//!
//! That code is all licensed under MIT or Apache-2.0
//!
//! Ours is of course under gplv3+

pub const DEFAULT_EDITOR_PROGRAM: &str = "vi";
pub const EDITOR_ENV_VAR: &str = "EDITOR";
use log::info;
use std::{
    fs::File,
    io::{self, Write},
    path::Path,
    process::{Command, ExitStatus},
};
use tempfile::{NamedTempFile, PathPersistError};

use crate::util::{tty_hacks::make_tty_child_and_forward, UniversalInspectMut, UniversalTransform};

#[derive(Debug, thiserror::Error)]
pub enum EditorInputError {
    #[error("Editor exited with unsuccessful exit status {0}")]
    ExitStatus(ExitStatus),
    #[error("Some io error when trying to write to stream")]
    IO(
        #[source]
        #[from]
        io::Error,
    ),
    #[error("PTY error when opening editor")]
    PtyEditorError(
        #[source]
        #[from]
        crate::util::tty_hacks::PtyError,
    ),
}

/// Get the editor command to use from the environment, as specified in [`DEFAULT_EDITOR_PROGRAM`]
/// and [`EDITOR_ENV_VAR`]
pub fn editor_cmd() -> String {
    std::env::var(EDITOR_ENV_VAR).unwrap_or_else(|_| DEFAULT_EDITOR_PROGRAM.to_owned())
}

/// Simply try to edit the path with the configured editor in the environment ^.^
pub fn simply_edit(filepath: &Path) -> Result<(), EditorInputError> {
    let editor = editor_cmd();
    Command::new(editor)
        .inspecting_process_mut(|cmd| {
            cmd.arg(filepath);
        })
        .apply_fn(make_tty_child_and_forward)?
        .apply_fn(|exit_status| {
            if exit_status.success() {
                Ok(())
            } else {
                Err(EditorInputError::ExitStatus(exit_status))
            }
        })
}

/// Opens the editor specified by the $EDITOR environment variable (fallback
/// `DEFAULT_EDITOR`, which is probably `vi`), and attempt to save to a target filepath.
///
/// Note that the last boolean can be false to prevent actually trying to edit, in case you dont
/// want to risk failing to find an exe, or otherwise having a fully interactionless workflow :)
/// ^.^
///
/// Args:
///   * `prewrite_function` - what to initially write to the file. In the case of no editor pass,
///     this is the only action performed
///   * `target_path` - where the file should persist to - note that in the case of no editor pass,
///      this is opened directly.
///   * `do_edit` - if true, actually try and edit the file after initially writing it. If not,
///   write directly.
///  
/// In the case of an error in an editing pass, the persistent file will simply not be written.
pub fn post_write_edited_file(
    prewrite_function: impl FnOnce(&mut File) -> std::io::Result<()>,
    target_path: &Path,
    do_edit: bool,
) -> Result<(), EditorInputError> {
    // In the case of an editor pass, we want to do temporary file creation.
    let maybe_named_file = if do_edit {
        Some(NamedTempFile::new()?)
    } else {
        None
    };

    // We split the temporary file into temppath (which controls the temporary file lifetime), and
    // file nya
    //
    // In the case where we don't have a temporary file, we produce a file that is directly the
    // final location.
    let (perhaps_temporary_location, mut always_file) = maybe_named_file
        .map(|v| {
            let (fl, tmppath) = v.into_parts();
            Ok((Some(tmppath), fl))
        })
        .unwrap_or_else(
            || -> Result<(Option<tempfile::TempPath>, File), std::io::Error> {
                // generate a file directly at the target location nya
                let fl_raw = File::create(target_path)?;
                Ok((None, fl_raw))
            },
        )?;

    prewrite_function(&mut always_file)?;

    // Perform editing here ^.^
    if let Some(temporary_file_path) = perhaps_temporary_location {
        simply_edit(&temporary_file_path)?;
        // Try flush the output file
        always_file.flush()?;

        // Now we want to persist the temporary path into the real file - if there *is* a temporary
        // path we didnt write to the real file, if there isn't, this step is unnecessary because
        // we wrote to the real file straight away nya

        // Note that we can't simply do .persist() or .persist_noclobber(), because that requires
        // being on the same filesystem. Instead, we try persisting first, and if impossible, we
        // see if we can copy instead ^.^
        if let Err(PathPersistError { error, path }) = temporary_file_path.persist(target_path) {
            match error.kind() {
                io::ErrorKind::CrossesDevices => {
                    // In this case we copy
                    info!(
                        "Copying from temporary {} to {} because cross-device filesystem move \
                         requires copy.",
                        path.display(),
                        target_path.display()
                    );
                    std::fs::copy(&path, target_path)?;
                }
                _ => Err(error)?,
            }
        }
    }

    always_file.flush().map_err(EditorInputError::IO)
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
