//! Argument file - contains utilities for specifying arguments.

use std::{
    ffi::OsStr,
    fmt::Display,
    fs::File,
    io::{BufReader, BufWriter},
    path::PathBuf,
};

use super::multi_io::{StdinOrFile, StdoutOrFile};

/// Input that can either be StdIn or a file path ^.^
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum StdinOrPath {
    Stdin,
    FilePath(PathBuf),
}

impl From<Option<PathBuf>> for StdinOrPath {
    fn from(v: Option<PathBuf>) -> Self {
        match v {
            Some(fp) => Self::FilePath(fp),
            None => Self::Stdin,
        }
    }
}

impl Display for StdinOrPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StdinOrPath::Stdin => write!(f, "stdin"),
            StdinOrPath::FilePath(fp) => write!(f, "filepath:{}", fp.display()),
        }
    }
}

impl StdinOrPath {
    /// If this is a filepath, attempt to open the file for reading.
    ///
    /// If not it naturally does nothing.
    pub fn open_filepath(&self) -> std::io::Result<StdinOrFile> {
        Ok(match self {
            StdinOrPath::Stdin => StdinOrFile::Stdin,
            StdinOrPath::FilePath(fpath) => StdinOrFile::File(BufReader::new(File::open(&fpath)?)),
        })
    }
}

/// Parse OSString into path - this is primarily for
/// https://github.com/clap-rs/clap/blob/v3.1.12/examples/derive_ref/README.md#arg-types
impl From<&OsStr> for StdinOrPath {
    fn from(path: &OsStr) -> Self { Self::FilePath(PathBuf::from(path)) }
}

impl Default for StdinOrPath {
    fn default() -> Self { StdinOrPath::Stdin }
}

/// Output that can either be StdOut or a file path ^.^
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum StdoutOrPath {
    Stdout,
    FilePath(PathBuf),
}

impl From<Option<PathBuf>> for StdoutOrPath {
    fn from(v: Option<PathBuf>) -> Self {
        match v {
            Some(fp) => Self::FilePath(fp),
            None => Self::Stdout,
        }
    }
}

impl Display for StdoutOrPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StdoutOrPath::Stdout => write!(f, "stdout"),
            StdoutOrPath::FilePath(fp) => write!(f, "filepath:{}", fp.display()),
        }
    }
}

impl StdoutOrPath {
    /// If this option isn't stdout, attempt to open the path as a file with [std::fs::File::create]
    /// to write to ^.^
    pub fn open_filepath_writeable(&self) -> std::io::Result<StdoutOrFile> {
        Ok(match self {
            StdoutOrPath::Stdout => StdoutOrFile::Stdout,
            StdoutOrPath::FilePath(fpath) => {
                StdoutOrFile::File(BufWriter::new(File::create(&fpath)?))
            }
        })
    }
}

/// Parse OSString into path - this is primarily for
/// https://github.com/clap-rs/clap/blob/v3.1.12/examples/derive_ref/README.md#arg-types
impl From<&OsStr> for StdoutOrPath {
    fn from(path: &OsStr) -> Self { Self::FilePath(PathBuf::from(path)) }
}

impl Default for StdoutOrPath {
    fn default() -> Self { StdoutOrPath::Stdout }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
