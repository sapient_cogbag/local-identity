//! Simple representation for cases where an input (Reader) or output (Writer) is
//! either stdio or a file.

use std::{
    fs::File,
    io::{stdin, stdout, BufRead, BufReader, BufWriter, Read, StdinLock, StdoutLock, Write},
};

/// Indicates whether or not to use stdin or a file, but does not lock stdin.
///
/// Useful for places where stdin needs special handling rather than being treated as a simple
/// reader.
pub enum StdinOrFile {
    Stdin,
    File(BufReader<File>),
}

impl StdinOrFile {
    /// Lock stdin for use as a [std::io::BufRead]. File has no change
    pub fn with_locked_stdin(self) -> StdinLockOrFile {
        match self {
            StdinOrFile::Stdin => StdinLockOrFile::Stdin(stdin().lock()),
            StdinOrFile::File(f) => StdinLockOrFile::File(f),
        }
    }
}

/// Indicates whether or not to use stdout or a file, but does not imply locked stdout.
///
/// Useful for places where stdout needs special handling, or where it may be interleaved ^.^
pub enum StdoutOrFile {
    Stdout,
    File(BufWriter<File>),
}

impl StdoutOrFile {
    /// Lock stdout for use as a [std::io::Write] in a uniform manner.
    pub fn with_locked_stdout(self) -> StdoutLockOrFile {
        match self {
            StdoutOrFile::Stdout => StdoutLockOrFile::Stdout(stdout().lock()),
            StdoutOrFile::File(f) => StdoutLockOrFile::File(f),
        }
    }

    /// If this writes to a file, try and flush it.
    ///
    /// The file is always in a [std::io::BufWriter], so you should *always* call this when
    /// done (either here or in [StdoutLockOrFile]). Here, it will only flush the file.
    pub fn flush(&mut self) -> std::io::Result<()> {
        match self {
            StdoutOrFile::Stdout => Ok(()),
            StdoutOrFile::File(f) => f.flush(),
        }
    }
}

/// Contains a locked stdin or a buffered file.
pub enum StdinLockOrFile {
    Stdin(StdinLock<'static>),
    File(BufReader<File>),
}

impl StdinLockOrFile {
    /// Perform a bulk read operation on whatever reader this enum contains.
    pub fn bulk_read_operation<T>(&mut self, mut op: impl FnMut(&mut dyn Read) -> T) -> T {
        match self {
            StdinLockOrFile::Stdin(stdin) => op(stdin),
            StdinLockOrFile::File(fl) => op(fl),
        }
    }

    /// Perform a bulk [BufRead] operation - needed because of the restriction on not
    /// having multiple non-auto traits in a dyn object.
    ///
    /// Use if you want lots of line-based operations.
    pub fn bulk_bufread_operation<T>(&mut self, mut op: impl FnMut(&mut dyn BufRead) -> T) -> T {
        match self {
            StdinLockOrFile::Stdin(stdin) => op(stdin),
            StdinLockOrFile::File(fl) => op(fl),
        }
    }

    /// Unlock stdin if we are stdin.
    pub fn with_unlocked_stdin(self) -> StdinOrFile {
        match self {
            StdinLockOrFile::Stdin(_) => StdinOrFile::Stdin,
            StdinLockOrFile::File(f) => StdinOrFile::File(f),
        }
    }
}

/// Contains a locked stdout or a buffered file
///
/// After finishing, you should call [StdoutLockOrFile::flush] to ensure that the buffer is
/// emptied, as with [std::io::BufWriter]
pub enum StdoutLockOrFile {
    Stdout(StdoutLock<'static>),
    File(BufWriter<File>),
}

impl StdoutLockOrFile {
    /// Perform a bulk write operation on whatever writer this enum contains
    pub fn bulk_write_operation<T>(&mut self, mut op: impl FnMut(&mut dyn Write) -> T) -> T {
        match self {
            StdoutLockOrFile::Stdout(stdout) => op(stdout),
            StdoutLockOrFile::File(fl) => op(fl),
        }
    }

    /// If this is stdout, unlock it. Note that it is recommended to use [StdoutLockOrFile::flush]
    /// if there is a risk of weird data loss stuff (i dont actually know).
    pub fn with_unlocked_stdout(self) -> StdoutOrFile {
        match self {
            StdoutLockOrFile::Stdout(_) => StdoutOrFile::Stdout,
            StdoutLockOrFile::File(f) => StdoutOrFile::File(f),
        }
    }

    /// Attempt to flush the writer, if necessary.
    pub fn flush(&mut self) -> std::io::Result<()> {
        match self {
            StdoutLockOrFile::Stdout(stdout) => stdout.flush(),
            StdoutLockOrFile::File(fl) => fl.flush(),
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
