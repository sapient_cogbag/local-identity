//! simple io utilities.

use std::{
    error::Error as StdError,
    ffi::OsStr,
    io::{Error as IoError, ErrorKind as IoErrorKind, Read, Result as IoResult, Stdin, Write},
    path::Path,
    process::ChildStdin,
};

use schemars::JsonSchema;
use serde::Serialize;

/// Convert any error into an IO error, with [`std::io::ErrorKind::Other`]
#[inline]
pub fn to_other_io_error<E: StdError + Send + Sync + 'static>(err: E) -> IoError {
    IoError::new(IoErrorKind::Other, err)
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, JsonSchema, thiserror::Error)]
/// Indicates an error in [`path_is_unicode_with_suffix_and_name`]
pub enum SuffixValidationError {
    #[error("Filename did not have suffix '{suffix}'")]
    DoesntMatchSuffix { suffix: &'static str },
    #[error("Filename was empty when stripped of suffix '{suffix}'")]
    TooShort { suffix: &'static str },
    #[error("Filename was not valid utf8")]
    InvalidUtf8,
    #[error("Path ended with .. (file_name() returned None)")]
    NoFilename,
}

/// Validate that a path's filename has the provided suffix, when stripped of the suffix is at
/// least one character long, and is valid utf8, and return the resulting filename of this
/// process if it succeeded.
///
/// Note that the suffix is checked for as is, rather than implicitly inserting a "." in front of
/// it or something similar.
pub fn path_is_unicode_with_suffix_and_name<'p>(
    path: &'p Path,
    suffix: &'static str,
) -> Result<&'p str, SuffixValidationError> {
    match path.file_name() {
        Some(raw_file_name) => is_unicode_with_suffix_and_name(raw_file_name, suffix),
        None => Err(SuffixValidationError::NoFilename),
    }
}

/// Like [path_is_unicode_with_suffix_and_name] but for OsStr (where OsStr is taken as the
/// filename).
pub fn is_unicode_with_suffix_and_name<'os>(
    osstr: &'os OsStr,
    suffix: &'static str,
) -> Result<&'os str, SuffixValidationError> {
    match osstr.to_str() {
        Some(utf8_file_name) => has_suffix_and_name(utf8_file_name, suffix),
        None => Err(SuffixValidationError::InvalidUtf8),
    }
}

/// Like [is_unicode_with_suffix_and_name] but for a filename string that is already known to be
/// unicode ^.^
pub fn has_suffix_and_name<'str>(
    string: &'str str,
    suffix: &'static str,
) -> Result<&'str str, SuffixValidationError> {
    match string.strip_suffix(suffix) {
        Some(valid_fname) if !valid_fname.is_empty() => Ok(valid_fname),
        Some(_v) => Err(SuffixValidationError::TooShort { suffix }),
        None => Err(SuffixValidationError::DoesntMatchSuffix { suffix }),
    }
}

/// Extension trait for readers
///
/// Primarily, this provides a version of [read] that automatically retries if the read was
/// interrupted nya
pub trait ReadExt: std::io::Read {
    /// Like [`std::io::Read::read`] but retries until it was not interrupted ^.^
    #[inline]
    fn read_interrupt_retry(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        loop {
            let result = self.read(buf);
            if let Err(e) = &result {
                if e.kind() == std::io::ErrorKind::Interrupted {
                    continue;
                }
            };
            break result;
        }
    }

    /// Like [`read_interrupt_retry`] but interpret certain errors as simply "run out of data"
    /// instead and return Ok(0)
    #[inline]
    fn read_interrupt_retry_with_finishing_errors(
        &mut self,
        buf: &mut [u8],
        finishing_ekinds: impl FnOnce(IoErrorKind) -> bool,
    ) -> IoResult<usize> {
        match self.read_interrupt_retry(buf) {
            Ok(n) => Ok(n),
            Err(e) if finishing_ekinds(e.kind()) => Ok(0),
            Err(e) => Err(e),
        }
    }

    /// Like [`read_interrupt_retry`] but returns Ok(0) with a broken pipe error as well (so that
    /// just gets interpreted as "no data left" ^.^
    ///
    /// Essentially specialised version of [read_interrupt_retry_with_finishing_errs]
    #[inline]
    fn read_interrupt_retry_broken_finisher(&mut self, buf: &mut [u8]) -> IoResult<usize> {
        self.read_interrupt_retry_with_finishing_errors(buf, |ek| ek == IoErrorKind::BrokenPipe)
    }
}

impl<T: Read + ?Sized> ReadExt for T {}

/// When streaming from input to a writable output, this encodes whether the stream can continue or
/// if it has stopped accepting any more input in the form of written output ^.^
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum WriterState {
    /// There has been no indication that the writer can no longer take input
    StillAcceptingInput,
    /// There has been some indication that the writer can't take any more bytes ^.^
    Broken,
}

/// Extension traits for writers
pub trait WriteExt: std::io::Write {
    /// Like [`std::io::Write::write_all`], except it allows designating certain errors as
    /// "finishing the stream", as opposed to "push this up this is going to kill everything" i.e,
    /// its simplified error handling.
    ///
    /// This function returns whether or not a writer is still accepting input, by returning a
    /// [WriterState]
    #[inline]
    fn write_all_with_finishing_errors(
        &mut self,
        next_data_chunk: &[u8],
        finishing_ekinds: impl FnOnce(IoErrorKind) -> bool,
    ) -> IoResult<WriterState> {
        match self.write_all(next_data_chunk) {
            Ok(()) => Ok(WriterState::StillAcceptingInput),
            Err(e) if finishing_ekinds(e.kind()) => Ok(WriterState::Broken),
            Err(e) => Err(e),
        }
    }

    /// Function specially designed for the case where you are streaming from one stdio stream or
    /// the other. Treats not writing anything, or broken pipes, as "Broken" writer state as
    /// opposed to an error state, but otherwise treats everything else as an error state.
    ///
    /// Is a convenience version of [`write_all_with_finishing_errors`], that classes a writer as
    /// broken in the case that write_all returns Broken Pipe or that a WriteZero error occurs.
    #[inline]
    fn write_all_streaming(&mut self, next_data_chunk: &[u8]) -> IoResult<WriterState> {
        self.write_all_with_finishing_errors(next_data_chunk, |ek| {
            ek == IoErrorKind::BrokenPipe || ek == IoErrorKind::WriteZero
        })
    }
}

impl<T: Write + ?Sized> WriteExt for T {}

/// Reads from the read and writes to the write while there are no errors.
///
/// Returns when there is no more data to read, *or* the write is a broken pipe or otherwise
/// blocked up and no more bytes could be written (see [WriteExt::write_all_streaming]).
pub fn streamforward(from: &mut dyn Read, to: &mut dyn Write) -> IoResult<()> {
    const BUF_SIZE: usize = 4096;
    let mut buf = [0u8; BUF_SIZE];
    loop {
        let bytes_read = from.read_interrupt_retry_broken_finisher(&mut buf)?;
        if bytes_read == 0 {
            return Ok(());
        }
        let read_into_buf = &buf[..bytes_read];
        let writer_state = to.write_all_streaming(read_into_buf)?;
        if writer_state == WriterState::Broken {
            return Ok(());
        }
    }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
