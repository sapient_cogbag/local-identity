use std::error::Error;

pub mod ioerr {
    /// Like the unstable std::io::Error::other function.
    ///
    /// TODO: Replace with the standard version when stabilised.
    ///
    /// See: https://github.com/rust-lang/rust/issues/91946
    pub fn other<E: Into<Box<dyn std::error::Error + Send + Sync>>>(err: E) -> std::io::Error {
        std::io::Error::new(std::io::ErrorKind::Other, err)
    }
}

/// Extension trait for [Result], to implement `flatten` on it.
///
/// TODO: Replace with https://github.com/rust-lang/rust/issues/70142 when its done and stable ^.^
pub trait ResultFlattenExt {
    type T;
    type E;
    fn flatten(self) -> Result<Self::T, Self::E>;
}

impl<T, E> ResultFlattenExt for Result<Result<T, E>, E> {
    type E = E;
    type T = T;

    fn flatten(self) -> Result<Self::T, Self::E> { self? }
}

/// Utility error for bundling data that would otherwise be destroyed into an existing error
/// transparently, both in terms of error functions as well as [`std::fmt::Display`] and
/// [`std::fmt::Debug`].
///
/// This is primarily designed for cases where you want to bundle e.g. an &self or &mut self, or
/// some other similar data that would otherwise be destroyed, back into the result even if there
/// is an error.
pub struct TransparentError<T, E: Error>(pub (T, E));

impl<T, E: Error> TransparentError<T, E> {
    /// Break this apart into the bundled type and error as a tuple [`(T, E)`]
    #[inline]
    pub fn split(self) -> (T, E) { self.0 }

    /// Extract the inner error, discarding the attached type
    #[inline]
    pub fn discard_data(self) -> E { self.0 .1 }
}

/// Special trait on any error that will convert it into a transparent error with the given data.
pub trait TransparentErrorExt: Error + Sized {
    #[inline]
    fn with_transparent_data<T>(self, data: T) -> TransparentError<T, Self> {
        TransparentError((data, self))
    }
}
impl<E: Error + Sized> TransparentErrorExt for E {}

/// Special trait on any result that wants transparent data added to it's error
pub trait TransparentResultExt {
    /// Error type
    type E: TransparentErrorExt;
    /// Ok type
    type O;
    fn with_transparent_err_data<T>(self, data: T)
        -> Result<Self::O, TransparentError<T, Self::E>>;
}

impl<O, E: TransparentErrorExt> TransparentResultExt for Result<O, E> {
    type E = E;
    type O = O;

    #[inline]
    fn with_transparent_err_data<T>(
        self,
        data: T,
    ) -> Result<Self::O, TransparentError<T, Self::E>> {
        match self {
            Ok(v) => Ok(v),
            Err(e) => Err(e.with_transparent_data(data)),
        }
    }
}

/// Special trait that allows discarding error data when a result has [TransparentError]
pub trait ResultTransparentExt {
    type O;
    type NewE;

    /// Discard transparent data attached to the error of this result to leave a plain error.
    fn discard_err_data(self) -> Result<Self::O, Self::NewE>;
}

impl<O, E: Error, T> ResultTransparentExt for Result<O, TransparentError<T, E>> {
    type NewE = E;
    type O = O;

    #[inline]
    fn discard_err_data(self) -> Result<Self::O, Self::NewE> {
        self.map_err(TransparentError::discard_data)
    }
}

impl<T, E: Error> Display for TransparentError<T, E> {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0 .1, f)
    }
}

impl<T, E: Error> Debug for TransparentError<T, E> {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { Debug::fmt(&self.0 .1, f) }
}

impl<T, E: Error> Error for TransparentError<T, E> {
    #[inline]
    fn source(&self) -> Option<&(dyn Error + 'static)> { self.0 .1.source() }

    /*
     * Unstable
    fn type_id(&self, _: private::Internal) -> std::any::TypeId
    where
        Self: 'static,
    {
        std::any::TypeId::of::<Self>()
    }

    fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
        None
    }
    */

    #[inline]
    // TODO: when description is finally removed, get rid of this.
    #[allow(deprecated)]
    fn description(&self) -> &str { self.0 .1.description() }

    #[inline]
    fn cause(&self) -> Option<&dyn Error> { self.0 .1.source() }
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::{Debug, Display};
