//! Some useful extra functions on types, or plain, or anything else really.

trait HashMapSealed {}

pub trait HashMapExt<K, V> {
    /// Attempt to insert a key/value into a HashMap.
    ///
    /// If it doesn't exist, just do the insert. If it does exist, however, perform a function that
    /// can modify the value in place, but that is also fallible.
    ///
    /// Returns either a mutable reference to the relevant key, or the error type returned by the
    /// function.
    // TODO: When [HashMap::try_insert] stabilises, it may be possible to implement this more
    // efficiently via that.
    fn try_insert_or_modify<
        'k,
        Q: ?Sized + Eq + Hash + ToOwned<Owned = K>,
        ErrorType,
        ValueModifyFn: FnOnce(&Q, &mut V, V) -> Result<(), ErrorType>,
    >(
        &mut self,
        key: &'k Q,
        new_value: V,
        conflict_resolution_function: ValueModifyFn,
    ) -> Result<&mut V, ErrorType>
    where
        K: Borrow<Q>;

    /// Like [try_insert_or_modify], but with an infallible function ^.^
    fn insert_or_modify<
        'k,
        Q: ?Sized + Eq + Hash + ToOwned<Owned = K>,
        ValueModifyFn: FnOnce(&Q, &mut V, V),
    >(
        &mut self,
        key: &'k Q,
        new_value: V,
        conflict_resolution_function: ValueModifyFn,
    ) -> &mut V
    where
        K: Borrow<Q>,
    {
        self.try_insert_or_modify(key, new_value, move |a, b, c| -> Result<(), ()> {
            conflict_resolution_function(a, b, c);
            Ok(())
        })
        .expect("Infallible conflict resolution fn")
    }
}

impl<K: Eq + Hash + Clone, V, S: BuildHasher> HashMapExt<K, V> for HashMap<K, V, S> {
    fn try_insert_or_modify<
        'k,
        Q: ?Sized + Eq + Hash + ToOwned<Owned = K>,
        ErrorType,
        ValueModifyFn: FnOnce(&Q, &mut V, V) -> Result<(), ErrorType>,
    >(
        &mut self,
        key: &'k Q,
        new_value: V,
        conflict_resolution_function: ValueModifyFn,
    ) -> Result<&mut V, ErrorType>
    where
        K: Borrow<Q>,
    {
        // See https://github.com/rust-lang/rust/issues/82766#issuecomment-1136245553 here for the
        // basic principle nya

        // First replace the value, getting the original.
        let original_value = self.insert(key.to_owned(), new_value);

        // If the original is None, we are all done nya
        match original_value {
            None => Ok(self.get_mut(key).expect("Just inserted")),
            Some(mut to_be_new_value) => {
                // Get hold of the new value
                let to_be_original_value = self.get_mut(key).expect("Just inserted");
                // Swap the values - now to_be_original_value is the original value (and the value
                // in the map), and to_be_new_value is the new value (owned).
                std::mem::swap(&mut to_be_new_value, to_be_original_value);
                let new_value = to_be_new_value;
                let original_value = to_be_original_value;
                conflict_resolution_function(key, original_value, new_value)?;
                Ok(original_value)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;

    use super::HashMapExt;

    #[test]
    pub fn test_hashmap_try_insert_and_modify() {
        let mut hashmap = HashMap::<u64, u64>::new();

        fn noncommutative_op(_: &u64, cv: &mut u64, nv: u64) -> Result<(), ()> {
            *cv -= nv;
            Ok(())
        }

        assert_eq!(
            *hashmap
                .try_insert_or_modify(&3u64, 12, noncommutative_op)
                .unwrap(),
            12
        );

        assert_eq!(
            *hashmap
                .try_insert_or_modify(&3u64, 8, noncommutative_op)
                .unwrap(),
            {
                let mut v = 12u64;
                noncommutative_op(&3, &mut v, 8).unwrap();
                v
            }
        );
    }
}

/// Function types but with the output as part of the trait rather than as an associated type. Good
/// for parameterisable-arguments. Default function traits have late-bound lifetimes in the output
/// which is unhelpful for constraining those lifetimes, and this provides alternatives ^.^
///
/// It also allows for use of GAT ctors easier even in the outputs :)
///
/// In different terms, it allows you to specify that you only care about one specific form of
/// output when provided the given arguments, even if the type abstractly implements multiple
/// possible output types for the same input arguments.
///
/// ([see here][latelt] for information on the very very obscure notions involved here)
///
/// [latelt](https://haibane-tenshi.github.io/rust-early-and-late-bound-generics/)
pub mod non_at_fns {
    pub trait NatFnOnce<ArgTuple, Output> {
        fn call_once(self, args: ArgTuple) -> Output;
    }

    pub trait NatFnMut<ArgTuple, Output> {
        fn call_mut(&mut self, args: ArgTuple) -> Output;
    }

    pub trait NatFn<ArgTuple, Output> {
        fn call(&self, args: ArgTuple) -> Output;
    }

    macro_rules! impl_with_n_tuples {
        ($F:ident, $O:ident, $($T:ident)*) => {
            // We use non-snake-case in instantiations because we unify type and variable
            // names to reduce the # of identifiers needed to be passed to the macro nya
            #[allow(non_snake_case)]
            impl <$O, $($T,)*  $F: FnOnce($($T),*) -> $O> NatFnOnce<($($T,)*), $O> for F {
                #[inline]
                fn call_once(self, ($($T,)*) : ($($T,)*)) -> $O {
                    (self)($($T),*)
                }
            }

            // We use non-snake-case in instantiations because we unify type and variable
            // names to reduce the # of identifiers needed to be passed to the macro nya
            #[allow(non_snake_case)]
            impl <$O, $($T,)*  $F: FnMut($($T),*) -> $O> NatFnMut<($($T,)*), $O> for F {
                #[inline]
                fn call_mut(&mut self, ($($T,)*) : ($($T,)*)) -> $O {
                    (self)($($T),*)
                }
            }

            // We use non-snake-case in instantiations because we unify type and variable
            // names to reduce the # of identifiers needed to be passed to the macro nya
            #[allow(non_snake_case)]
            impl <$O, $($T,)*  $F: Fn($($T),*) -> $O> NatFn<($($T,)*), $O> for F {
                #[inline]
                fn call(&self, ($($T,)*) : ($($T,)*)) -> $O {
                    (self)($($T),*)
                }
            }
        }
    }

    impl_with_n_tuples! {F, O,}
    impl_with_n_tuples! {F, O, T0}
    impl_with_n_tuples! {F, O, T0 T1}
    impl_with_n_tuples! {F, O, T0 T1 T2}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3 T4}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3 T4 T5}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3 T4 T5 T6}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3 T4 T5 T6 T7}
    impl_with_n_tuples! {F, O, T0 T1 T2 T3 T4 T5 T6 T7 T8}
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    borrow::Borrow,
    collections::HashMap,
    hash::{BuildHasher, Hash},
};
