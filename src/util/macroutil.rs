//! Module for useful type definition macros and similar things that don't fit cleanly into another
//! module.

/// Define a clean pair of string newtypes - one a dynamically-sized type, and one an owned type,
/// using `$inner_dst` and `$inner_owned` as the inner DST and owned types, respectively.
///
/// Note that reffn is required to get clean to_owned/borrow behaviour.
///
/// VALIDATION:
///  * This macro allows the injection of a validator into the type. If it exists, the raw
///  conversion functions are made unsafe and TryFrom converters are made available. Furthermore,
///  serde deserialization gains an extra validation step.
///     ```
///     /// Some... documentation here
///     #[inline] // function attributes are allowed
///     <visibility> validator_name = |this: &'lt INNERDST /*this is currently simply str*/| ->
///       Option<ErrorType>{
///         ...validation code
///     }
///     ```
///  * This can't work without a reffn to convert from &str to &DST
///  * This also uses the same intrinsic mechanism as [`crate::utils::serde::Deserialize`], but
///    without the `#[serde(remote = Self)]` trick as it manually implements the deserialization
///    instead of autoderiving it and then using remote to overlay that implementation with
///    validation.
///  * The error type must implement [`alloc::borrow::ToOwned`] - If your type implements [`Clone`]
///    you should be fine.
///
/// SERDE:
///  * This macro is capable of implementing serde ::Serialize and ::Deserialize traits.
///  * It takes a series of options - "serialize" or "deserialize" or both (maybe more in the
///  future).
///
/// JSON SCHEMA:
///  * This macro can also generate json schema by implementing [`schemars::JsonSchema`]
///  * Add `json_schema yes;` to the properties to generate json schema definitions.
///
///  DISPLAY FORWARDING:
///  * This macro can forward a [`std::fmt::Display`] implementation from the outer DST to the
///  inner DST, optionally providing a transformation (where the inner DST is provided as a
///  parameter to a function along with the format writer).
#[macro_export]
macro_rules! dst_newtype {
    {
        $(#[$metao:meta])*
        $viso:vis $owned:ident$(($visinnero:vis))? from $inner_owned:ty;
        $(#[$metad:meta])*
        $visd:vis $dst:ident$(($visinnerd:vis))? from $inner_dst:ty;
        $(
            reffn
            $(#[$meta_reffn:meta])*
            $visfrom:vis $raw_from_fn_name:ident;
        )?
        $(
            mutfn
            $(#[$meta_mutfn:meta])*
            $visfrom_mut:vis $raw_from_fn_name_mut:ident;
        )?
        $(extra_shared_derives $($shared_derives:ident),*;)?
        $(owned_only_derives $($owned_only_derives:ident),*;)?
        $(ref_only_derives $($ref_only_derives:ident),*;)?
        $(serde $($serde_options:ident)*;)?
        $(json_schema $($json_schema_enable:ident)?;)?
        $(display $($display_enable:ident $(
            $(#[$display_meta:meta])*
            |$display_inner_this:ident: &$display_lt:lifetime DST,
            $display_formatter:ident: &mut Formatter<$display_formatter_lt:lifetime>|
            $display_blk:block
        )?)?;)?
        $(validator
          $(#[$validator_meta:meta])*
          $validator_vis:vis $validator_name:ident =
            |$validator_this:ident: &$validator_lt:lifetime DST| -> Option<$validator_err_ty:ty>
            $validator_blk:block;
        )?
    } => {
        #[derive(
            Debug, PartialEq, Eq, PartialOrd, Ord, Hash,
            $($($shared_derives,)*)?
            $($($owned_only_derives,)*)?
        )]
        $(#[$metao])*
        #[repr(transparent)]
        $viso struct $owned($($visinnero)? $inner_owned);

        #[derive(
            Debug, PartialEq, Eq, PartialOrd, Ord, Hash,
            $($($shared_derives,)*)?
            $($($ref_only_derives,)*)?
        )]
        $(#[$metad])*
        #[repr(transparent)]
        $visd struct $dst($($visinnerd)? $inner_dst);

        impl $dst {
            dst_newtype!{@validated_raw_conv
                $dst from $inner_dst;
                $( $(#[$meta_reffn])* $visfrom $raw_from_fn_name)?;
                $(|$validator_this|)?
            }

            dst_newtype!{@validated_raw_conv_mut
                $dst from $inner_dst;
                $( $(#[$meta_mutfn])* $visfrom_mut $raw_from_fn_name_mut )?;
                $(|$validator_this|)?
            }

            $(
                $(#[$validator_meta])*
                $validator_vis fn $validator_name<$validator_lt>(
                    $validator_this: &$validator_lt $inner_dst
                ) -> ::core::option::Option::<$validator_err_ty> {
                    $validator_blk
                }
            )?
        }

        dst_newtype!{@from_impl <$inner_dst, $inner_owned> for <$dst, $owned>;
            $($validator_name ($validator_lt) -> Option<$validator_err_ty>)?;
            $(reffn $raw_from_fn_name)?;
            $(mutfn $raw_from_fn_name_mut)?;
        }

        dst_newtype!{@serde
            from <$inner_dst, $inner_owned> as <$dst, $owned>;
            validator $($validator_name)?;
            $($($serde_options)*)?
        }

        $(dst_newtype!{@json_schema $($json_schema_enable)?;
            <$inner_dst, $inner_owned> => <$dst, $owned>
        })?

        dst_newtype!{@display $($($display_enable)?)? $($($(
            $(#[$display_meta])*
            |$display_inner_this: $display_lt, $display_formatter: $display_formatter_lt| $display_blk
        )?)?)?;<$inner_dst, $inner_owned> => <$dst, $owned> $(reffn $raw_from_fn_name)?}

        $(
        impl ::std::borrow::ToOwned for $dst {
            type Owned = $owned;

            #[inline]
            fn to_owned(&self) -> Self::Owned {
                $owned(self.0.to_owned())
            }
        }

        impl ::std::borrow::Borrow<$dst> for $owned {
            #[inline]
            fn borrow(&self) -> &$dst {
                self.as_ref()
            }
        }

        // Conditional on const ref function
        impl ::core::convert::AsRef<$dst> for $owned {
            #[inline]
            // Sometimes the raw function is marked as safe, sometimes not
            #[allow(unused_unsafe)]
            fn as_ref(&self) -> &$dst {
                // safety: validation function is applied to construction of all components nya
                unsafe { <$dst>::$raw_from_fn_name(&self.0) }
            }
        })?

        // Conditional on mut ref function.
        $(impl ::core::convert::AsMut<$dst> for $owned {
            #[inline]
            // Sometimes the raw function is marked as safe, sometimes not
            #[allow(unused_unsafe)]
            fn as_mut(&mut self) -> &mut $dst {
                // safety: validation function is applied to construction of all components nya
                unsafe { <$dst>::$raw_from_fn_name_mut(&mut self.0) }
            }
        })?

        // AsRefs for string references nya
        impl ::core::convert::AsRef<$inner_dst> for $dst {
            #[inline]
            fn as_ref(&self) -> &$inner_dst {
                &self.0
            }
        }

        dst_newtype!{@novalidated_as_mut_inner $($validator_name)?; <$dst as $inner_dst>}
    };
    // When there is a validator, we do not implement because the inner mutable reference may bork
    // up the validation invariants
    {@novalidated_as_mut_inner $validator_name:ident; <$dst:ident as $inner_dst:ty>} => {};
    {@novalidated_as_mut_inner; <$dst:ident as $inner_dst:ty>} => {
        /// Note that we do not allow this if there is validation because the inner mutable
        /// reference could remove the invariants enforced by the validator.
        impl ::core::convert::AsMut<$inner_dst> for $dst {
            #[inline]
            fn as_mut(&mut self) -> &mut $inner_dst {
                &mut self.0
            }
        }
    };
    // Raw conversion with a validator
    {@validated_raw_conv $dst:ident from $inner_dst:ty;
        $(#[$meta:meta])* $visfrom:vis $reffnname:ident;
        |$this:ident|
    } => {
        $(#[$meta])*
        /// # Safety
        /// Ensure the inner type meets all the preconditions set up by the outer
        /// type when calling this function
        $visfrom unsafe fn $reffnname(inner: &$inner_dst) -> &$dst {
            // See [`std::path::Path`] and u8_slice_as_os_str nya
            // It shows how to "properly" do this cast safely.
            unsafe { &*(inner as *const $inner_dst as *const $dst)}
        }
    };
    // Raw conversion without a validator (safe) nyaa
    {@validated_raw_conv
        $dst:ident from $inner_dst:ty;
        $(#[$meta:meta])* $visfrom:vis $reffnname:ident;
    } => {
        $(#[$meta])*
        $visfrom fn $reffnname(inner: &$inner_dst) -> &$dst {
            // See [`std::path::Path`] and u8_slice_as_os_str nya
            // It shows how to "properly" do this cast safely.
            unsafe { &*(inner as *const $inner_dst as *const $dst)}
        }
    };
    // No reffn at all - create no function
    {@validated_raw_conv $dst:ident from $inner_dst:ty;; $(|$this:ident|)?} => {};


    {@validated_raw_conv_mut
        $dst:ident from $inner_dst:ty;
        $(#[$meta:meta])* $visfrom:vis $reffnname:ident;
        |$this:ident|
    } => {
        $(#[$meta])*
        $visfrom unsafe fn $reffnname(inner: &mut $inner_dst) -> &mut $dst {
            // See [`std::path::Path`] and u8_slice_as_os_str nya
            // It shows how to "properly" do this cast safely.
            unsafe { &mut *(inner as *mut $inner_dst as *mut $dst)}
        }
    };
    {@validated_raw_conv_mut
        $dst:ident from $inner_dst:ty;
        $(#[$meta:meta])* $visfrom:vis $reffnname:ident;
    } => {
        $(#[$meta])*
        $visfrom fn $reffnname(inner: &mut $inner_dst) -> &mut $dst {
            // See [`std::path::Path`] and u8_slice_as_os_str nya
            // It shows how to "properly" do this cast safely.
            unsafe { &mut *(inner as *mut $inner_dst as *mut $dst)}
        }
    };
    // No mutfn at all - create no function
    {@validated_raw_conv_mut $dst:ident from $inner_dst:ty;; $(|$this:ident|)?} => {};


    // From or TryFrom with the validator nya
    //
    // Implements the traits for both nomut and mut (as well as by value) with validation
    //
    // reffn is necessary to implement the owned variant as well.
    {@from_impl <$inner_dst:ty, $inner_owned:ty> for <$dst:ident, $owned:ident>;
        $validator_fn_name:ident ($validator_lt:lifetime) -> Option<$validator_err_ty:ty>;
        $(reffn $reffn_name:ident)?;
        $(mutfn $mutfn_name:ident)?;
    } => {
        $(
        impl<$validator_lt> ::core::convert::TryFrom::<&$validator_lt $inner_dst>
            for &$validator_lt $dst {
            type Error = $validator_err_ty;
            fn try_from(value: &$validator_lt $inner_dst)
                -> ::core::result::Result::<Self, Self::Error> {
                // Safety: validator_fn validates the value and only if there are no errors do we
                // pass it through nya.
                $dst::$validator_fn_name(value)
                    .map(::core::result::Result::Err)
                    .unwrap_or(Ok(unsafe {$dst::$reffn_name(value)}))
            }
        }

        impl ::core::convert::TryFrom::<$inner_owned> for $owned {
            type Error = <$validator_err_ty as ::std::borrow::ToOwned>::Owned;
            fn try_from(value: $inner_owned) -> ::core::result::Result<$owned, Self::Error> {
                use ::core::convert::AsRef;
                use ::std::borrow::ToOwned;
                let vref = &value;
                let validation_err = $dst::$validator_fn_name(
                    AsRef::<$inner_dst>::as_ref(vref)
                ).map(|a| ToOwned::to_owned(&a));
                match validation_err {
                    ::core::option::Option::Some(e) => ::core::result::Result::Err(e),
                    // Safety: we checked! nya
                    ::core::option::Option::None => ::core::result::Result::Ok($owned(value))
                }

            }
        }
        )?

        $(
        impl <$validator_lt> ::core::convert::TryFrom::<&$validator_lt mut $inner_dst> for &$validator_lt mut $dst {
            type Error = $validator_err_ty;
            fn try_from(value: &$validator_lt mut $inner_dst) -> ::core::result::Result<&$validator_lt mut $dst, Self::Error> {
                // Safety - we just validated it nyaaa
                $dst::$validator_fn_name(value)
                    .map(::core::result::Result::Err)
                    .unwrap_or(Ok(unsafe {$dst::$mutfn_name(value)}))
            }

        }
        )?
    };

    {@from_impl <$inner_dst:ty, $inner_owned:ty> for <$dst:ident, $owned:ident>;
        ;
        $(reffn $reffn_name:ident)?;
        $(mutfn $mutfn_name:ident)?;
    } => {
        $(
        impl<'a> ::core::convert::From<&'a $inner_dst> for &'a $dst {
            #[inline]
            fn from(v: &'a $inner_dst) -> &'a $dst {
                $dst::$reffn_name(v)
            }
        })?

        impl ::core::convert::From<$inner_owned> for $owned {
            #[inline]
            fn from(v: $inner_owned) -> $owned {
                $owned(v)
            }
        }

        $(
        impl<'a> ::core::convert::From<&'a mut $inner_dst> for &'a mut $dst {
            #[inline]
            fn from(v: &'a mut $inner_dst) -> &'a mut $dst {
                $dst::$mutfn_name(v)
            }
        }
        )?
    };


    // More than one option - provide a single option.
    {@serde
        from <$inner_dst:ty, $inner_owned:ty> as <$dst:ident, $owned:ident>;
        validator $($validator_fn_name:ident)?;
        $current_option:ident $($rest_of_options:ident)+
    } => {
        dst_newtype!{@serde
            from <$inner_dst, $inner_owned> as <$dst, $owned>;
            validator $($validator_fn_name)?;
            $current_option
        }

        dst_newtype!{@serde
            from <$inner_dst, $inner_owned> as <$dst, $owned>;
            validator $($validator_fn_name)?;
            $($rest_of_options)+
        }
    };

    // Serialization impl - note that we don't care about validator functionality here, so we don't
    // conditionally implement on that nya
    {@serde
        from <$inner_dst:ty, $inner_owned:ty> as <$dst:ident, $owned:ident>;
        validator $($validator_fn_name:ident)?;
        serialize
    } => {
        impl ::serde::Serialize for $owned {
            #[inline]
            fn serialize<S: ::serde::ser::Serializer>(&self, serializer: S)
                -> ::core::result::Result<S::Ok, S::Error> {
                ::serde::Serialize::serialize(&self.0, serializer)
            }
        }

        impl ::serde::Serialize for $dst {
            #[inline]
            fn serialize<S: ::serde::ser::Serializer>(&self, serializer: S)
                -> ::core::result::Result<S::Ok, S::Error> {
                    ::serde::Serialize::serialize(&self.0, serializer)
                }
        }
    };

    // Deserialization impl - this is much more complex and requires a reffn name - it also has
    // both unvalidated and validated variations
    //
    // Take a look at [this internal peice of
    // code](https://docs.rs/serde/latest/src/serde/de/impls.rs.html#733-748)
    // for forwarding to see roughly what we're gonna do ^.^ nya
    {@serde
        from <$inner_dst:ty, $inner_owned:ty> as <$dst:ident, $owned:ident>;
        validator $validator_fn_name:ident;
        deserialize
    } => {
        impl<'de> ::serde::Deserialize<'de> for $owned {
            #[inline]
            fn deserialize<D: ::serde::de::Deserializer<'de>>(deserializer: D)
                -> ::core::result::Result<$owned, <D as ::serde::de::Deserializer<'de>>::Error> {
                use ::serde::{Deserialize, de::{Error, Deserializer}};
                use ::core::convert::TryFrom;
                // Get the inner chunk
                let inner_deserialized = <$inner_owned as Deserialize>::deserialize(deserializer)?;
                // use TryFrom and map the error to a custom
                TryFrom::try_from(inner_deserialized)
                    .map_err(<D as Deserializer>::Error::custom)
            }
        }

        impl <'a, 'de: 'a> ::serde::Deserialize<'de> for &'a $dst {
            #[inline]
            fn deserialize<D: ::serde::de::Deserializer<'de>>(deserializer: D)
                -> ::core::result::Result<&'a $dst, <D as ::serde::de::Deserializer<'de>>::Error> {
                use ::serde::{Deserialize, de::{Error, Deserializer}};
                use ::core::convert::TryFrom;
                // Get the inner chunk
                let inner_deserialized = <&'a $inner_dst as Deserialize>::deserialize(deserializer)?;
                // use TryFrom and map the error to a custom
                TryFrom::try_from(inner_deserialized)
                    .map_err(<D as Deserializer>::Error::custom)
            }
        }
    };

    // Deserializer impl without need for extra validation.
    {@serde
        from <$inner_dst:ty, $inner_owned:ty> as <$dst:ident, $owned:ident>;
        validator;
        deserialize
    } => {
        impl<'de> ::serde::Deserialize<'de> for $owned {
            #[inline]
            fn deserialize<D: ::serde::de::Deserializer<'de>>(deserializer: D)
                -> ::core::result::Result<$owned, <D as ::serde::de::Deserializer<'de>>::Error> {
                use ::serde::Deserialize;
                use ::core::convert::Into;
                // Get the inner chunk
                <$inner_owned as Deserialize>::deserialize(deserializer).map(Into::into)
            }
        }

        impl <'a, 'de: 'a> ::serde::Deserialize<'de> for &'a $dst {
            #[inline]
            fn deserialize<D: ::serde::de::Deserializer<'de>>(deserializer: D)
                -> ::core::result::Result<&'a $dst, <D as ::serde::de::Deserializer<'de>>::Error> {
                use ::serde::Deserialize;
                use ::core::convert::Into;
                // Get the inner chunk
                <&'a $inner_dst as Deserialize>::deserialize(deserializer).map(Into::into)
            }
        }
    };

    // No more serde options nya
    {@serde
        from <$inner_dst:ty, $inner_owned:ty> as <$dst:ident, $owned:ident>;
        validator $($validator_fn_name:ident)?;
    } => {};

    // Json schema forwarding impl
    //
    // This is a more sophisticated version of forward_impl! in
    // https://github.com/GREsau/schemars/blob/master/schemars/src/json_schema_impls/mod.rs
    // that allows a rename
    {@json_schema_forward_impl_rename $name:ident ($($impl:tt)+) from $inner:ty} => {
        impl $($impl)+ {
            fn is_referenceable() -> bool {
                <$inner as ::schemars::JsonSchema>::is_referenceable()
            }

            fn schema_name() -> String {
                <str as ::std::borrow::ToOwned>::to_owned(stringify!($name))
            }

            fn json_schema(gen: &mut ::schemars::gen::SchemaGenerator) -> ::schemars::schema::Schema {
                <$inner as ::schemars::JsonSchema>::json_schema(gen)
            }

            fn _schemars_private_non_optional_json_schema(gen: &mut ::schemars::gen::SchemaGenerator) -> ::schemars::schema::Schema {
                <$inner as ::schemars::JsonSchema>::_schemars_private_non_optional_json_schema(gen)
            }

            fn _schemars_private_is_option() -> bool {
                <$inner as ::schemars::JsonSchema>::_schemars_private_is_option()
            }
        }
    };
    {@json_schema_forward_impl_rename $name:ident $inner:ty => $outer:ty} => {
        dst_newtype!{@json_schema_forward_impl_rename $name  (::schemars::JsonSchema for $outer) from $inner}
    };
    {@json_schema yes; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident>} => {
        dst_newtype!{@json_schema_forward_impl_rename $dst $inner_dst => $dst}
        dst_newtype!{@json_schema_forward_impl_rename $owned $inner_owned => $owned}
    };
    {@json_schema; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident>} => {};
    // Creating display implementations nya
    {@display; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident> $(reffn $rf:ident)?} => {};
    {@display enable; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident> $(reffn $reffn_name:ident)?} => {
        dst_newtype!{@display enable
            /// Forward to inner type implementation directly
            #[inline]
            |v: 'lt, fmt: '_| {
                use ::core::{convert::AsRef, fmt::Display};
                Display::fmt(AsRef::<$inner_dst>::as_ref(v), fmt)
            }; <$inner_dst, $inner_owned> => <$dst, $owned> $(reffn $reffn_name)?}
    };
    {@display enable
        $(#[$meta:meta])*
        |$inner_this:ident: $this_lt:lifetime, $fmt:ident: $fmt_lt:lifetime| $impl_block:block;
        <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident> $(reffn $reffn_name:ident)?
    } => {
        dst_newtype!{@owned_display_impl $($reffn_name)?;
            <$inner_dst, $inner_owned> => <$dst, $owned>
        }

        impl ::core::fmt::Display for $dst {
            fn fmt<$this_lt>(&$this_lt self, $fmt: &mut ::core::fmt::Formatter<$fmt_lt>) -> ::core::fmt::Result {
                let $inner_this = ::core::convert::AsRef::<$inner_dst>::as_ref(self);
                $impl_block
            }
        }
    };
    // Implement owned display if we have reffn, since that implies AsRef meow
    {@owned_display_impl; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident>} => {};
    {@owned_display_impl $reffn_name:ident; <$inner_dst:ty, $inner_owned:ty> => <$dst:ident, $owned:ident>} => {
        impl ::core::fmt::Display for $owned {
            #[inline]
            // It says to always use full paths in macros so idk nya
            #[allow(unused_imports)]
            fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> ::core::fmt::Result {
                use ::core::{convert::AsRef, fmt::Display};
                let selfref = AsRef::<$dst>::as_ref(self);
                Display::fmt(selfref, f)
            }
        }
    };
}

#[macro_export]
/// Essentially allows dropping the input and producing nothing,
///
/// This is used to repeat an identical expression over a repeated variable, even if that
/// variable is not used in an expression
macro_rules! macro_drop {
    ($($tts:tt)*) => {};
}

/// Like [`macro_drop`] but spits out everything after the first tt collection (in {})
///
/// Used in places where you cant just glob [`macro_drop`] on the end.
#[macro_export]
macro_rules! macro_drop_rest_tts {
    ({$($actual_tts:tt)*} $($rest:tt)*) => { $($actual_tts)* }
}

/// A pile of semi-internal where-clause utilities for providing and merging where
/// clauses ^.^
///
/// The where clause syntax is special to allow easier parsing - take a look at [crate::]
/// ```
/// {
///   @lt {some_lifetime_constraint}
///   ... repeated
///   @ty {some_type_constraint}
///   ... repeated
///   @lt_univ {some for<'lt> style constraint}
///   ... repeated
/// }
/// ```
///
/// It is designed for easy bundling and unbundling into TTs
#[macro_export]
macro_rules! where_builder {
    // Essentially the identity function ^.^
    {@validate_and_rebundle {
        $(@lt {$($lt_constraint:tt)*})*
        $(@ty {$($ty_constraint:tt)*})*
        $(@lt_univ {$($lt_univ_constraint:tt)*})*
    }} => {{
        $(@lt {$($lt_constraint)*})*
        $(@ty {$($ty_constraint)*})*
        $(@lt_univ {$($lt_univ_constraint)*})*
    }};
    /*
     *
     * We can't do plain @realise because macros can't produce *just* where clauses.
     *
     * So we fill it in properly nya
    // Simple alias nya
    {@realize $p:tt} => {$crate::where_bundler!{@realise $p}};
    // Turn a set of easy-parse constraints into an actual where clause - not including
    // the `where` keyword though.
    {@realise {
        $(@lt {$($lt_constraint:tt)*})*
        $(@ty {$($ty_constraint:tt)*})*
        $(@lt_univ {$($lt_univ_constraint:tt)*})*
    }} => {
        $($($lt_constraint)*,)*
        $($($ty_constraint)*,)*
        $($($lt_univ_constraint)*,)*
    };
    */
    // Merge two where clauses and produce another! nya
    {@merge {
        $(@lt {$($lt_constraint0:tt)*})*
        $(@ty {$($ty_constraint0:tt)*})*
        $(@lt_univ {$($lt_univ_constraint0:tt)*})*
    } {
        $(@lt {$($lt_constraint1:tt)*})*
        $(@ty {$($ty_constraint1:tt)*})*
        $(@lt_univ {$($lt_univ_constraint1:tt)*})*
    }} => {
        $crate::where_builder!{@validate_and_rebundle {
            $(@lt {$($lt_constraint0)*})*
            $(@lt {$($lt_constraint1)*})*
            $(@ty {$($ty_constraint0)*})*
            $(@ty {$($ty_constraint1)*})*
            $(@lt_univ {$($lt_univ_constraint0)*})*
            $(@lt_univ {$($lt_univ_constraint1)*})*
        }}
    };

    // Merge multiple where bundles
    {@merge_multiple $({
        $(@lt {$($lt_constraint:tt)*})*
        $(@ty {$($ty_constraint:tt)*})*
        $(@lt_univ {$($lt_univ_constraint:tt)*})*
    })*} => {
        $crate::where_builder!{@validate_and_rebundle {
            $($(@lt {$($lt_constraint)*})*)*
            $($(@ty {$($ty_constraint)*})*)*
            $($(@lt_univ {$($lt_univ_constraint)*})*)*
        }}
    };

    // Realise a where clause after some other collection of clauses, and filling in the innards
    {
        @realise_where $block_contents:tt $(where {
            $(@lt {$($lt_constraint:tt)*})*
            $(@ty {$($ty_constraint:tt)*})*
            $(@lt_univ {$($lt_univ_constraint:tt)*})*
        })? as
        $($block_is_this:tt)*
    } => {
        $($block_is_this)* $(where
            $($($lt_constraint)*,)*
            $($($ty_constraint)*,)*
            $($($lt_univ_constraint)*,)*
        )? $block_contents
    };

    // Realise a where, with multiple where bundles merged together. This
    // avoids some issues with macro-in-macro stuff nya
    {@realise_multi_where $block_contents:tt where $({
        $(@lt {$($lt_constraint:tt)*})*
        $(@ty {$($ty_constraint:tt)*})*
        $(@lt_univ {$($lt_univ_constraint:tt)*})*
    })* as $block_is_this:tt} => {
        $($block_is_this)* where
            $($($($lt_constraint)*,)*)*
            $($($($ty_constraint)*,)*)*
            $($($($lt_univ_constraint)*,)*)*
        $block_contents
    };
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
