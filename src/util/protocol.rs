//! Module for defining protocols *in general* on a "per packet" basis, providing
//! some combiners as well.
//!
//! Note that while I would prefer session types, I am not currently able to rehack the
//! `session_types` crate to go over serialisation stuff and do that >.<

use super::unix_sockets::Ctr;

pub type WithLt<'a, T> = <T as Ctr>::Ty<'a>;

/// Packet received from the server on the client.
pub trait S2CPacketRecvCtr: Ctr {
    /// The packet to send to the server as a result (constructed from input lifetime)
    type ResultingC2SPacket<'a>: Sized;
    /// The error produced (constructed from input lifetime)
    type ResultingError<'a>: Sized;

    /// Return either nothing - terminating the connection - or a result when given a type
    /// constructor for a general packet type buildable from the resulting
    fn on_received<'a, FinalC2SPacket: Ctr, FinalC2SErr: Ctr>(
        recved: Self::Ty<'a>,
    ) -> Option<Result<FinalC2SPacket::Ty<'a>, FinalC2SErr::Ty<'a>>>
    where
        WithLt<'a, FinalC2SPacket>: From<Self::ResultingC2SPacket<'a>>,
        WithLt<'a, FinalC2SErr>: From<Self::ResultingError<'a>>;
}

/// Packet received from the client on the server.
pub trait C2SPacketRecvCtr: Ctr {
    /// The packet to send to the client as a result (constructed from input lifetime)
    type ResultingS2CPacket<'a>: Sized;
    /// The error produced (constructed from input lifetime)
    type ResultingError<'a>: Sized;

    /// Return either nothing - terminating the connection - or a result when given a type
    /// constructor for a general packet type buildable from the resulting
    fn on_received<'a, FinalS2CPacket: Ctr, FinalS2CErr: Ctr>(
        recved: Self::Ty<'a>,
    ) -> Option<Result<FinalS2CPacket::Ty<'a>, FinalS2CErr::Ty<'a>>>
    where
        WithLt<'a, FinalS2CPacket>: From<Self::ResultingS2CPacket<'a>>,
        WithLt<'a, FinalS2CErr>: From<Self::ResultingError<'a>>;
}


#[macro_export]
/// Useful macro that contains utility sub-macros for creating various bits and pieces of protocol
/// modules. 
macro_rules! priv_module_builder_internal {
    // Create a type, and an associated constructor type with it. 
    //
    // This is a little chunk that can take one lifetime, and basic generics, and a standard where
    // bundle for Ctor impls (using where_builder! macro nya), and generate a struct as well as
    // a lifetime constructor struct too.
    //
    // This takes generics as part of some weird custom declaration stuff (in particular TTs, which
    // allow construction of generics). 
    //
    // The last bit (the as part) allows specifying how to fill in the generics on the concrete
    // type to match it to the constructor lifetime.
    {@with_ctor
        $(#[$applied_meta:meta])*
        $vis:vis $ctor_name:ident $(<$($fixed_generics_lts:lifetime),* $($fixed_generics_tys:ident),*>)? $(where $where_bundle:tt)?
            = $inner_item_name:ident @ $inner_item_def:item for <$lt:lifetime> $(= $($generics_with_lts_filled_in:tt)*)?
    } => {

        $crate::build_generic_lt_and_type_holder! { 
            #[doc = concat!("Generated lifetime constructor for item ", stringify!{$inner_item_name})]
            $vis $ctor_name $(<$($fixed_generics_lts),* $($fixed_generics_tys),*>)?
            $(where $where_bundle)?
        }

        $(#[$applied_meta])* 
        $inner_item_def

        $crate::where_builder!{@realise_where {
            type Ty<$lt> = $inner_item_name$(::$($generics_with_lts_filled_in)*)?;
        } $(where $where_bundle)? as 
            impl $(<$($fixed_generics_lts,)* $($fixed_generics_tys),*>)? $crate::util::unix_sockets::Ctr 
            for $ctor_name$(::<$($fixed_generics_lts,)* $($fixed_generics_tys),*>)?
        }
    };


    // Generate the real return type - and its constructor - from the constructors of all
    // the types and errors - this has collected generic parameters for *everything*, which means users should
    // not be gluing horrible generics in their things.
    //
    // It does provide a rewrite option, where it will rewrite the generic parameters for each
    // type. Very unpleasant business, though that is the internal thing (we just plug in all the
    // lt and type generics and where clauses all together as appropriate). 
    //
    // This result type doesn't include stuff like transitions, that needs to be added *later*. 
    {@make_flat_result_type
        $(#[$result_type_meta:meta])*
        $vis:vis for<$lt:lifetime> $ctor_name:ident $(<$($source_generics_lts:lifetime),* $($source_generics_tys:ident),*>)? ($ok_name:ident, $err_name:ident) = 
            Ok(Ctr) | 
    } => {


    }
}


/*
#[macro_export]
/// Macro to define a single protocol module. 
///
/// Protocol modules are rust modules. They contain an optional "state" that must be constructible
/// from and deconstructible to the state of the parent module, whatever that may be. If not
/// present, it will be the parent state (which gets passed through), and if *that* isn't present,
/// it will be [`()`] - i.e. the unit type.
macro_rules! protocol_module_builder {{
    user {
        $(state = 
    },
    parent {
        $($parent_state:ty)? 
    }} => {

    }

}


/// Macro for defining a protocol in one direction or the other.
///
macro_rules! full_protocol_definition {
    {

    }
}
*/

#[cfg(test)]
mod protocol_macro_testing_chamber {
    use crate::{priv_module_builder_internal, util::UniversalInspectMut};
    use crate::util::unix_sockets::Ctr;

    use super::WithLt;

    priv_module_builder_internal!{
        @with_ctor 
        /// Testing a nice basic constructible stringref
        pub BestStringRefCtor = BestStringRef @ pub struct BestStringRef<'a> {
            first_block_of_strings: &'a str,
            second_block_of_strings: &'a str
        } for<'a> = <'a>
    }

    #[allow(dead_code)]
    fn test_string_ref_combine(v: WithLt<'_, BestStringRefCtor>) -> String {
        v.first_block_of_strings.to_owned().inspecting_process_mut(|s| s.push_str(v.second_block_of_strings))
    }

    priv_module_builder_internal!{
        @with_ctor
        /// Some complex sized generics :)
        pub BestSliceRefCtor<T> where {@ty {T: 'static}} 
            = BestSliceRef @ pub struct BestSliceRef<'a, T: 'static>(&'a [T]); for<'a> = <'a, T>
    }

    priv_module_builder_internal! {
        @with_ctor
        /// Real DST hours here nya
        #[allow(dead_code)]
        pub BestRecursiveCtor<InnerCtor> where {
                @ty {InnerCtor: Ctr} 
                @lt_univ {for<'v> WithLt<'v, InnerCtor>: Sized} 
        } = BestRecursive @ pub enum BestRecursive<'a, InnerCtor> 
            where 
                InnerCtor: Ctr, 
                for <'v> WithLt<'v, InnerCtor>: Sized 
        {
            Just(WithLt<'a, InnerCtor>),
            Nothing
        }  for<'a> = <'a, InnerCtor>
    }

}


// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
