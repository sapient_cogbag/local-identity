//! Module containing conversion utilities.

pub use count_macro::count;
pub use count_tts::count_tts;
pub use seq_macro::seq;

/// Generates a multi-enum from a set of *unique* types, with *from* implementations.
///
/// This will naturally produce errors in the case of duplicate types. It also provides for
/// optional parameterisation (for e.g. lifetimes or generic types). If you must specify
/// constraints on the parameters, you *must* do it in a *where clause*, because these are all
/// glued into certain trait implementations. Take a look at [crate::where_builder] for how to
/// format it.
///
/// Note that because of the blanket implementation of [Into], we can't implement a *unifying into*
/// where if each parameter is into we can ourselves convert.
///
/// Implementing it as a new trait also doesn't work because blanket impls cause conflicting impls
/// in this case, so instead we do it as an internal method.
///
/// Each type path can also have associated #[attrs] put on the enum variant as well as on the
/// inner component
#[macro_export]
macro_rules! multi_enum_unique {
    (
        $(#[$meta:meta])*
        $vis:vis enum $name:ident $(<$($lt_ps:lifetime),* $(,)? $($ty_ps:ident),*>)?
        for [
            $($Ts:ty
                $([outer $(#[$out_attr:meta])*])?
                $([inner $(#[$in_attr:meta])*])?
            )|*
        ] $(where $where_bundle:tt)?;
    ) => {
    $crate::util::conv::count!{
        $crate::where_builder!{@realise_where {$(
            $($(#[$out_attr])*)?
            E_int_enumvariant_(
                $($(#[$in_attr])*)?
                $Ts
            ),
        )*} $(where $where_bundle)? as
            $(#[$meta])*
            $vis enum $name $(<$($lt_ps,)* $($ty_ps),*>)?
        }
        $crate::multi_enum_unique!{
            @ftp ftpm impl_from impl $(<{$($lt_ps,)* $($ty_ps,)*}>)? for $name $(where $where_bundle)?[$($Ts)|*]
        }
        $crate::multi_enum_unique!{
            @ftp ftps impl_shared_into impl $(<{$($lt_ps,)* $($ty_ps,)*}>)? for $name $(where $where_bundle)? [$($Ts)|*]
        }
    }};

    // Fill in default parameters to forward to the given impl generator
    // Inner functions need to have {} to contain empty <> as well as < $params >, because
    // the innards cannot be empty otherwise - <> doesn't a token tree make :( nya
    //
    // This ends up also filling out the where bundle to a default as well.
    //
    // The first argument to this is the mode:
    //  m: multi - mode where one call is made for each type with the extra magic parameters.
    //  s: single - mode where the inner macro is called once for the whole enum without enabling
    //     easy type iter - good for cases where it doesn't make sense to generate definitions for
    //     each internal type nya
    //     This has the exact same call configuration as multi except without the "with [...]"
    //     clause for per-type/variant information ^.^
    (@ftpm $subgen:ident impl <{$($params:tt)*}> for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $($tt:tt)*) => {
        $crate::multi_enum_unique! {
            @for_types {<{$($params)*}>} call @$subgen for $name where $where_bundle [$($Ts)|*] {$($tt)*}
        }
    };

    (@ftps $subgen:ident impl <{$($params:tt)*}> for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $($extras:tt)*) => {
          $crate::multi_enum_unique!{@$subgen
                {<{$($params)*}>} for $name where $where_bundle [$($Ts)|*]
                {$($extras)*}
          }
    };

    // Forward inward, filling in empty params
    //
    // We need to put where before the [] in all cases otherwise where's get matched as tts on the
    // end when passed through nya.
    (@ftp $m:ident $subgen:ident impl for $name:ident $(where $where_bundle:tt)? [$($Ts:ty)|*] $($tt:tt)*) => {
        $crate::multi_enum_unique! {
            @ftp $m $subgen impl <{}> for $name $(where $where_bundle)? [$($Ts)|*]  $($tt)*
        }
    };

    // Forward inward, filling in empty where bundle nya
    (@ftp $m:ident $subgen:ident impl <{$($params:tt)*}> for $name:ident [$($Ts:ty)|*] $($tt:tt)*)  => {
        $crate::multi_enum_unique! {
            @$m $subgen impl <{$($params)*}> for $name where {} [$($Ts)|*] $($tt)*
        }
    };

    // Finalised forwarder to submethods
    (@ftp $m:ident $subgen:ident impl <{$($params:tt)*}> for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $($tt:tt)*)  => {
        $crate::multi_enum_unique! {@$m
            $subgen impl <{$($params)*}> for $name where $where_bundle [$($Ts)|*] $($tt)*
        }
    };
    // Wrapper for_types that duplicates and bundles the type list for the inner call nya
    //
    // See that one for main documentation ^.^
    (@for_types
        $param_bundle:tt call @$implementation_fn:ident
        for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $extra:tt
     ) => {
        $crate::multi_enum_unique!{@for_types_bundled_types
            $param_bundle call @$implementation_fn
                for $name where $where_bundle [$($Ts)|*] [$($Ts)|*] $extra
        }
    };


    // This is to get around the issue where we need to expand a list of
    // generic params, but because they need to be bundled as a *list* of tts,
    // expanding them cannot be done when expanding the types.
    //
    // This allows bundling the { $($params:tt)* } as a single outer tt, iterating over
    // all of the types, and calling back in with a { $($params:tt)* } block that can
    // be un-bundled nya
    //
    // It also in the process provides paths to the enum variant corresponding to the type,
    // and a raw integer literal (there is both a self-relative path and a full path).
    //
    // Note that for the same reason we have to bundle parameters, we also bundle extra args in a
    // single {} $tt:tt nya
    //
    // You also need to bundle the types a second time :(
    (@for_types_bundled_types
     $param_bundle:tt call @$implementation_fn:ident
     for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $typepath_bundle:tt $extra:tt
     ) => {
        $crate::util::conv::count!{$(
              $crate::multi_enum_unique!{@$implementation_fn
                    $param_bundle for $name where $where_bundle $typepath_bundle with [$Ts | Self::E_int_enumvarself_ | $name::E_int_enumvarglobal_ | _int_raw_]
                    $extra
              }
        )*}
    };

    // From-Impls from each type.
    //
    // This effectively enforces the uniqueness rule nya
    (@impl_from {<{$($params:tt)*}>} for $name:ident where $where_bundle:tt [$($Ts:ty)|*] with [$CurrT:ty | $selfrel:path | $globalrel:path | $raw_n:literal] {}) => {
        $crate::where_builder!{@realise_where {
                #[inline]
                fn from(v: $CurrT) -> Self {
                    $selfrel(v)
                }
            } where $where_bundle as impl <$($params)*> From<$CurrT> for $name::<$($params)*>
        }
    };

    (@impl_shared_into {<{$($params:tt)*}>} for $name:ident where $where_bundle:tt [$($Ts:ty)|*] $($unused:tt)*) => {
        $crate::util::conv::count!{
            $crate::where_builder!{@realise_where {
                #[inline]
                // Macro so its fine nya
                #[allow(dead_code)]
                pub fn unifying_into<Q>(self) -> Q where $($Ts : ::core::convert::Into::<Q>),* {
                    match self {$(
                        Self::E_int_varenumctr_(v) => ::core::convert::Into::<Q>::into($crate::macro_drop_rest_tts!({v} $Ts)),
                    )*}
                }
            } where $where_bundle as impl <$($params)*> $name::<$($params)*>
        }}
    };
}

#[cfg(test)]
mod test_multi_enum {
    use crate::multi_enum_unique;
    multi_enum_unique! {
        /// Lil docs
        #[derive(serde::Deserialize, serde::Serialize, schemars::JsonSchema)]
        pub enum AllOf for [u32 | u64 | i8];
    }

    multi_enum_unique! {
        /// Testy packet
        pub enum MaybeStringOrPath <'a> for [&'a str | &'a std::path::Path];
    }

    multi_enum_unique! {
        /// Budget Vec Cow nya
        #[derive(serde::Serialize)]
        pub enum VecOrSliceRef <'a, T> for [
            &'a [T] [outer #[serde(rename="was_borrowed")]] |
            Vec<T> [outer #[serde(rename="owned")]]
        ] where {@ty {T: Sized}};
    }
}

#[macro_export]
/// Create a type that can act to "hold" the given type and lifetime parameters along with
/// a where clause, with the given name.
macro_rules! build_generic_lt_and_type_holder {
    {
        $(#[$meta:meta])*
        $vis:vis $name:ident 
        $(<$($fixed_generics_lts:lifetime),* $($fixed_generics_tys:ident),*>)?
        $(where $where_bundle:tt)?
    } => {
        $crate::where_builder!{@realise_where ; $(where $where_bundle)? as
        $vis struct $name $(<$($fixed_generics_lts),* $($fixed_generics_tys),*>)? $((
            ::core::marker::PhantomData::<
                ($(&$fixed_generics_lts ()),* 
                 $(Box::<$fixed_generics_tys>),*,)
            >))?
        }
    }
}


#[macro_export]
/// This macro is category E Evil.
///
/// What it does is take a set of [`super::unix_socket::Ctr`] types, that themselves are generic 
/// over lists of types.
///
/// It then creates a new constructor, that is itself generic over some collection of lifetimes and
/// types, and *maps* that constructor to each individual type in the generated multi-enum. 
///
/// I know this is horrible, trust me...
///
/// You occasionally have to provide a new where bundle. Consider that when partially reifying
/// certain type parameters, you can't do a simple substitution in all cases because there is no
/// way to pull out the internal structure of where clauses and eliminate the ones with
/// all-concrete types.
///
/// Ergo, you have to provide a new one using the unspecified types.
macro_rules! multi_unique_enum_from_generic_constructed_types {
    {$(#[$multi_meta:meta])*
    $ctor_vis:vis $ctor_name:ident $(<$($source_generics_lts:lifetime),* $(,)? $($source_generics_tys:ident),*>)? = 
        $enum_vis:vis enum $enum_name:ident $(<$($target_generics_lts:lifetime),* $(,)? $($target_generics_tys:ident),*>)? for [
            $(@$per_ctr_lt:lifetime $CtorTs:ty
                $([outer $(#[$out_attr:meta])*])?
                $([inner $(#[$in_attr:meta])*])?
            )|*
        ] $(where $where_bundle:tt)? for <$lt:lifetime> $(= $($generics_when_ctr_constructs:tt)*)?
    } => {
        $crate::build_generic_lt_and_type_holder!{
            #[doc = concat!("Generated lifetime-parameterised constructor for multi-enum", stringify!{$ctor_name})]
            $ctor_vis $ctor_name $(<$($source_generics_lts),* $($source_generics_tys),*>)?
            $(where $where_bundle)?
        }
        
        $crate::multi_enum_unique!{
            $(#[$multi_meta])*
            $enum_vis enum $enum_name $(<$($target_generics_lts),*, $($target_generics_tys),*>)? for [$(
                <$CtorTs as $crate::util::unix_sockets::Ctr>::Ty::<$per_ctr_lt>
                $([outer $(#[$out_attr])*])?
                $([inner $(#[$in_attr])*])?
            )|*] $(where $where_bundle)?;
        }

        // Actually implement the constructor, feeding the enum the constructor-translated
        // generics when present ^.^ nya
        $crate::where_builder!{@realise_where {
            type Ty<$lt> = $enum_name$(::$($generics_when_ctr_constructs)*)?;
        } $(where $where_bundle)? as   
            impl $(<$($source_generics_lts),* $($source_generics_tys),*>)? $crate::util::unix_sockets::Ctr
            for $ctor_name$(::<$($source_generics_lts),* $($source_generics_tys),*>)?
        }
    }
}

#[cfg(test)]
mod multi_unique_gen_enum_testing_chamber {
    use crate::{multi_unique_enum_from_generic_constructed_types, util::{protocol::WithLt, UniversalInspectMut}};
    use crate::priv_module_builder_internal;

    priv_module_builder_internal!{@with_ctor 
        pub CoolStrRefCtor <T> where {@ty {T: 'static + ?Sized}}
            = CoolStrRef @ pub struct CoolStrRef<'v, T: ?Sized>(pub &'v T); for<'a> = <'a, T> 
    }

    priv_module_builder_internal!{@with_ctor
        #[derive(serde::Serialize, serde::Deserialize)]
        pub SomeNeatPacketCtor = SomeNeatPacket @ pub struct SomeNeatPacket<'v> {
            pub name: &'v str,
            pub dob: &'v str
        } for <'a> = <'a>
    }

    multi_unique_enum_from_generic_constructed_types! {
        /// Innards
        pub CoolComboCtor<T> = pub enum CoolCombo<'v, T> for [
            @'v CoolStrRefCtor<u8> 
            | @'v CoolStrRefCtor<char>
            | @'v CoolStrRefCtor<[T]>
            | @'v SomeNeatPacketCtor
        ] where {@ty {T: 'static + Sized + Clone}} for <'a> = <'a, T>
    }

    #[allow(dead_code)]
    fn cool_string_combination<T: Into<char> + Clone>(v: WithLt<'_, CoolComboCtor<T>>) -> String {
        match v {
            CoolCombo::E0(u_8) => String::from_utf8([*u_8.0].to_vec()).unwrap(),
            CoolCombo::E1(chr) => String::new().inspecting_process_mut(|s| s.push(*chr.0)),
            CoolCombo::E2(CoolStrRef(inner_ref)) => {
                let mut s = String::new();
                for value in inner_ref.iter() {
                    s.push(value.clone().into());
                }
                s
            },
            CoolCombo::E3(SomeNeatPacket { name, dob }) => {
                format_args!{"{name} was born on {dob}"}.to_string()
            }
        }
    }
}



// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
