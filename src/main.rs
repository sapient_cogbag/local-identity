#![feature(exit_status_error)]
#![feature(io_safety)]
#![feature(io_error_more)]
#![feature(io_error_other)]
#![feature(trace_macros)]
#![feature(generic_associated_types)]
#![deny(unsafe_op_in_unsafe_fn)]
pub mod cmd;
pub mod identity;
pub mod state_modules;
pub mod util;

use clap::Parser;

use crate::cmd::CmdExecWithPassthrough;

fn main() -> Result<(), anyhow::Error> {
    // Note here we use parse() rather than try_parse() because it doesn't
    // require extra work to display the error properly nya
    let args: cmd::LocalIdentityCmd = cmd::LocalIdentityCmd::parse();

    args.exec_passthrough(module_path!().to_string())
}

// local-identity
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
